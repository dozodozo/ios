//
//  Messages.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 8/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import Foundation

class Messages {
    static let ForgottenPasswordTitle: String = "Forgot Password"
    static let ForgottenPasswordText: String = "Enter the e-mail you used to register with JopaJopa"
    static let INPUT_INVALID_EMAIL = "E-mail is not valid"
}
