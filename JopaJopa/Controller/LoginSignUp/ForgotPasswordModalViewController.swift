//
//  ForgotPasswordModalViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 8/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

//MARK: ForgoPaswordModalViewController

import UIKit

class ForgotPasswordModalViewController: UIViewController {
    
    // MARK: Properties
    
    //message label
    let lblMessage: UILabel = {
        let lbl = UILabel()
        lbl.text = NSLocalizedString("Enter the e-mail you used to register with JopaJopa", comment: "")
        lbl.lineBreakMode = .byWordWrapping
        lbl.numberOfLines = 3
        lbl.textAlignment = NSTextAlignment.justified
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    //text fields
    let txtEmail: InputTextField = {
        let txtField = InputTextField()
        txtField.placeholder = NSLocalizedString("E-mail", comment:"")
        return txtField
    }()
    
    //login button
    let btnSendEmail: NormalButton = {
        let btn = NormalButton()
        btn.setTitle(NSLocalizedString("Send E-mail", comment:""), for: UIControlState.normal)
        btn.alpha = 0.5
        btn.isEnabled = false
        btn.addTarget(self, action: #selector(sendEmail), for: UIControlEvents.touchUpInside)
        return btn
    }()
    
    // activity indicator
    let activityIndicator: UIActivityIndicatorView = {
        let actInd = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        actInd.translatesAutoresizingMaskIntoConstraints = false
        return actInd
    }()
    
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Reset Password", comment:"")
        
        //add views
        self.view.addSubview(lblMessage)
        self.view.addSubview(txtEmail)
        self.view.addSubview(btnSendEmail)
        self.view.addSubview(activityIndicator)
        
        
        //delegate
        txtEmail.delegate = self
        
        //setup
        setupLayout()

    }
    
    override func viewDidLayoutSubviews() {
        //add bottom borders, done after adding views
        txtEmail.addBottomBorder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    * Layout views
    */
    
    func setupLayout(){
        
        lblMessage.topAnchor.constraint(equalTo: view.topAnchor, constant:80).isActive = true
        lblMessage.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        lblMessage.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        txtEmail.topAnchor.constraint(equalTo: lblMessage.bottomAnchor, constant:20).isActive = true
        txtEmail.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        txtEmail.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        btnSendEmail.topAnchor.constraint(equalTo: txtEmail.bottomAnchor, constant:40).isActive = true
        btnSendEmail.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        btnSendEmail.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        activityIndicator.topAnchor.constraint(equalTo: btnSendEmail.centerYAnchor).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: btnSendEmail.centerXAnchor).isActive = true
    }
    
    // MARK: Helpers
    
    func updateApperanceSendingEmail(_ sender: Any){
        //show activtity indicator
        activityIndicator.startAnimating()
    }
    
    func updateApperanceSentEmail(_ sender: Any){
        //stop activity indicator
        self.activityIndicator.stopAnimating()
    }
    
     // MARK: Action
    
    //send reset e-mail
    @objc func sendEmail(_ sender: UIButton){
        
        //update appearance
        updateApperanceSendingEmail(sender)
        
        //get email and password
        let email = txtEmail.text!
        let jsonBody = "{\"email\":\"\(email)\"}"
        
        JopaJopaClient.sharedInstance().sendResetPasswordEmail(jsonBody: jsonBody){error in
            
            //update appearance
            performUIUpdatesOnMain {
                 self.updateApperanceSentEmail(sender)
            }
           
            /* Display Success Message: */
            func displaySuccessMessage(){
                let alertView: UIAlertController = {
                    let myAlertView = UIAlertController(title: "", message: "", preferredStyle: UIAlertControllerStyle.alert)
                    myAlertView.title = NSLocalizedString("E-mail Sent", comment:"")
                    myAlertView.message =  NSLocalizedString("A password reset link has been sent to your e-mail", comment: "")
                    let action = UIAlertAction(title: NSLocalizedString("Close", comment:""), style: UIAlertActionStyle.default){handler in
                        self.navigationController?.popViewController(animated: true)
                    }
                    myAlertView.addAction(action)
                    return myAlertView
                }()
                
                performUIUpdatesOnMain {
                    self.present(alertView, animated: true, completion: nil)
                }
                
            }
            
            //check if login was successful
            if let error = error {
                let alertController = JopaJopaClient.sharedInstance().getAlertControllerForErrors(error, JopaJopaClient.JopaJopaTask.Reset_Password)
                performUIUpdatesOnMain {
                    self.present(alertController, animated: true, completion: nil)
                }
            } else {
                //if password reset email sent, show homeview with tabs
                displaySuccessMessage()
            }
        }
    }

}

// MARK: LoginViewController Extension (UITextFieldDelegate)

extension ForgotPasswordModalViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // get email and password from text
        var updatedEmail = txtEmail.text!
        
        if let email = txtEmail.text,
            let textRange = Range(range, in: email) {
            if (textField == txtEmail){
                updatedEmail = email.replacingCharacters(in: textRange,
                                                         with: string)
            }
        }
        
        //enable login button only if email is valid and password not empty
        //small bug: if email being edited and password edited again with backspace
        
        if (InputValidator.isValidEmail(updatedEmail)){
            btnSendEmail.isEnabled = true
            btnSendEmail.alpha = 1.0
        } else {
            btnSendEmail.isEnabled = false
            btnSendEmail.alpha = 0.5
        }
        
        return true
    }
}
