//
//  LoginViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 7/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

// MARK: Login

import UIKit

class LoginViewController: UIViewController {
    
    // MARK: Properties
   
    let txtEmail: InputTextField = {
        let txtField = InputTextField()
        txtField.placeholder = NSLocalizedString("E-mail", comment:"")
        return txtField
    }()
    
    let txtPassword: InputTextField = {
        let txtField = InputTextField()
        txtField.placeholder = NSLocalizedString("Password", comment:"")
        txtField.isSecureTextEntry = true
        return txtField
    }()
    
    //login button
    let btnLogin: NormalButton = {
        let btn = NormalButton()
        btn.setTitle(NSLocalizedString("Login", comment:""), for: UIControlState.normal)
        btn.alpha = 0.5
        btn.isEnabled = false
        btn.addTarget(self, action: #selector(login), for: UIControlEvents.touchUpInside)
        return btn
    }()
    
    //password reset label
    let btnForgotPassword: UIButton = {
        let btn = UIButton()
        btn.setTitle(NSLocalizedString("Forgot Password", comment:""), for: UIControlState.normal)
        btn.setTitleColor(ViewConstants.BUTTON_COLOR_DEFAULT, for: UIControlState.normal)
        btn.titleLabel?.font = btn.titleLabel?.font.withSize(ViewConstants.SMALL_FONT)
        btn.addTarget(self, action: #selector(showEmailForm), for: UIControlEvents.touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    //activity Indicator
    let activityIndicator: UIActivityIndicatorView = {
        let actInd = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        actInd.translatesAutoresizingMaskIntoConstraints = false
        return actInd
    }()

    
    // MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //delegates
        txtEmail.delegate = self
        txtPassword.delegate = self
        
        //add UIViews
        self.view.addSubview(txtEmail)
        self.view.addSubview(txtPassword)
        self.view.addSubview(btnLogin)
        self.view.addSubview(btnForgotPassword)
        self.view.addSubview(activityIndicator)
        
        //tap gesture - hide keyboard if screen clicked
        let tapGestureRecoginizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tapGestureRecoginizer)
        self.view.isUserInteractionEnabled = true
        
        //setup layout
        setupLayout()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //set title - title must be set here for tabs
        self.tabBarController?.navigationItem.title = NSLocalizedString("Login", comment: "")
    }
    
    override func viewDidLayoutSubviews() {
        
        //add bottom borders, done after adding views
        self.txtEmail.addBottomBorder()
        self.txtPassword.addBottomBorder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UI Layout
    
    func setupLayout(){

        txtEmail.topAnchor.constraint(equalTo: view.topAnchor, constant:100).isActive = true
        txtEmail.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        txtEmail.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        txtPassword.topAnchor.constraint(equalTo: txtEmail.bottomAnchor, constant:10).isActive = true
        txtPassword.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        txtPassword.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        btnLogin.topAnchor.constraint(equalTo: txtPassword.bottomAnchor, constant:40).isActive = true
        btnLogin.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        btnLogin.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        btnForgotPassword.topAnchor.constraint(equalTo: btnLogin.bottomAnchor, constant:10).isActive = true
        btnForgotPassword.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        btnForgotPassword.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        activityIndicator.topAnchor.constraint(equalTo: btnLogin.centerYAnchor).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: btnLogin.centerXAnchor).isActive = true
    }
    
    // MARK: Helpers
    
    // appearance while logging  in
    private func updateAppearanceLoggingIn(_ sender: Any){
        //disable login button
        btnLogin.isEnabled = false
        btnLogin.alpha = 0.5
        
        //show activtity indicator
        activityIndicator.startAnimating()
        
        //hide keyboard
        hideKeyboard(sender)
    }
    
    // appearance afer login completes - this could also be a login faiure
    private func updateAppearanceLogInComplete(_ sender: Any){
        //stop activity indicator
        self.activityIndicator.stopAnimating()
        
    }
    
    // MARK: Actions
    
    //hide keyboard when user taps outside input
    @objc private func hideKeyboard(_ sender: Any){
        self.view.endEditing(true)
    }
    
    //login user
    @objc private func login(_ sender: UIButton){
        
        //appearance for logging in
        updateAppearanceLoggingIn(sender)
        
        //get email and password
        let email = txtEmail.text!
        let password = txtPassword.text!
        let jsonBody = "{\"email\":\"\(email)\",\"password\":\"\(password)\"}"
        
        //call login convenience method
        JopaJopaClient.sharedInstance().login(jsonBody: jsonBody){ error in
            
            //update appearance
            performUIUpdatesOnMainWithDelay {
                self.updateAppearanceLogInComplete(sender)
            }
            
        
            //check if login was successful
            if let error = error {
                
                //displayError()
                let alertController = JopaJopaClient.sharedInstance().getAlertControllerForErrors(error, JopaJopaClient.JopaJopaTask.Log_In)
                
                performUIUpdatesOnMain {
                    //enable login button
                    self.btnLogin.isEnabled = true
                    self.btnLogin.alpha = 1
                    //alert controller
                    self.present(alertController, animated: true, completion: nil)
                }
                
            } else {
                //if login successful, show homeview with tabs
                performUIUpdatesOnMain {
                    self.navigationController?.popViewController(animated: true)
                }
            }
            
        }
        
    }
    
    //show forgot password form
    @objc private func showEmailForm(_sender: UIButton){
        let forgotPasswordModalViewController = ForgotPasswordModalViewController()
        self.navigationController?.pushViewController(forgotPasswordModalViewController, animated: true)
    }

}

// MARK: LoginViewController Extension (UITextFieldDelegate)

extension LoginViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        // get email and password from text
        var updatedEmail = txtEmail.text!
        var updatedPassword = txtPassword.text!
        
        if let email = txtEmail.text,
            let textRange = Range(range, in: email) {
            if (textField == txtEmail){
                updatedEmail = email.replacingCharacters(in: textRange,
                                                         with: string)
            }
        }
        
        if let password = txtPassword.text,
            let textRange = Range(range, in: password) {
            if (textField == txtPassword){
                updatedPassword = password.replacingCharacters(in: textRange,
                                                               with: string)
            }
        }
        
        //enable login button only if email is valid and password not empty
        //small bug: if email being edited and password edited again with backspace
        
        if (InputValidator.isValidEmail(updatedEmail) && !updatedPassword.isEmpty){
            btnLogin.isEnabled = true
            btnLogin.alpha = 1.0
        } else {
            btnLogin.isEnabled = false
            btnLogin.alpha = 0.5
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
