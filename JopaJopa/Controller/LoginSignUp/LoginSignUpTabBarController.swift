//
//  LoginSignUpViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 7/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit

class LoginSignUpTabBarController: UITabBarController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //tab items and view controllers
        let loginViewController = LoginViewController()
        let signUpViewController = SignUpViewController()
        
        loginViewController.tabBarItem = UITabBarItem(title: NSLocalizedString("Login", comment:""), image: #imageLiteral(resourceName: "login"), tag: 0)
        signUpViewController.tabBarItem = UITabBarItem(title: NSLocalizedString("Sign Up", comment:""), image: #imageLiteral(resourceName: "signup"), tag: 1)
        
        self.viewControllers = [loginViewController, signUpViewController]
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
