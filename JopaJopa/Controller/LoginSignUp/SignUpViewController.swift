//
//  SignUpViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 7/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit

// MARK: Sign Up View Controller

class SignUpViewController: UIViewController {
    
    // MARK: Properties
    
    //keep track of active text field for adjusting screen when keyboard displayed
    var activeTextField: InputTextField!
    
    //booleans for checking if sign up button should be enabled
    var isFirstNameSet = false
    var isLastNameSet = false
    var isEmailSet = false
    var isPasswordSet = false
    var isConfirmPasswordSet = false
    
    let txtFirstName: InputTextField = {
        let txtField = InputTextField()
        txtField.placeholder = NSLocalizedString("First Name", comment:"")
        return txtField
    }()
    
    let txtLastName: InputTextField = {
        let txtField = InputTextField()
        txtField.placeholder = NSLocalizedString("Last Name", comment:"")
        return txtField
    }()
    
    let txtEmail: InputTextField = {
        let txtField = InputTextField()
        txtField.placeholder = NSLocalizedString("E-mail", comment:"")
        return txtField
    }()
    
    let txtPassword: InputTextField = {
        let txtField = InputTextField()
        txtField.isSecureTextEntry = true
        txtField.placeholder = NSLocalizedString("Password (min. 8 chararcters)", comment:"")
        return txtField
    }()
    
    let txtPasswordConfirmation: InputTextField = {
        let txtField = InputTextField()
        txtField.isSecureTextEntry = true
        txtField.placeholder = NSLocalizedString("Re-enter Password", comment:"")
        return txtField
    }()
    
    let btnSignUp: NormalButton = {
        let btn = NormalButton()
        btn.setTitle(NSLocalizedString("Sign Up", comment:""), for: UIControlState.normal)
        btn.addTarget(self, action: #selector(signup), for: .touchUpInside)
        btn.alpha = 0.5
        btn.isEnabled = false
        return btn
    }()
    
    // activity Indicator
    let activityIndicator: UIActivityIndicatorView = {
        let actInd = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        actInd.translatesAutoresizingMaskIntoConstraints = false
        return actInd
    }()
    
    // MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //delegates
        txtFirstName.delegate = self
        txtLastName.delegate = self
        txtEmail.delegate = self
        txtPassword.delegate = self
        txtPasswordConfirmation.delegate = self
        
        //add textFields
        self.view.addSubview(txtFirstName)
        self.view.addSubview(txtLastName)
        self.view.addSubview(txtEmail)
        self.view.addSubview(txtPassword)
        self.view.addSubview(txtPasswordConfirmation)
        self.view.addSubview(btnSignUp)
        self.view.addSubview(activityIndicator)
        
        //tap gesture
        let tapGestureRecoginizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tapGestureRecoginizer)
        self.view.isUserInteractionEnabled = true
        
        //setup layout
        setupLayout()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //set title of page
        self.tabBarController?.navigationItem.title = NSLocalizedString("Sign Up", comment: "")
        
        //keyboard notification
        subscribeToKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        
        //add bottom borders, done after adding views
        txtFirstName.addBottomBorder()
        txtLastName.addBottomBorder()
        txtEmail.addBottomBorder()
        txtPassword.addBottomBorder()
        txtPasswordConfirmation.addBottomBorder()
    }

    
    // MARK: UI Layout
    
    func setupLayout(){
        
        txtFirstName.topAnchor.constraint(equalTo: view.topAnchor, constant:100).isActive = true
        txtFirstName.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        txtFirstName.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        txtLastName.topAnchor.constraint(equalTo: txtFirstName.bottomAnchor, constant:10).isActive = true
        txtLastName.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        txtLastName.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        txtEmail.topAnchor.constraint(equalTo: txtLastName.bottomAnchor, constant:10).isActive = true
        txtEmail.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        txtEmail.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        txtPassword.topAnchor.constraint(equalTo: txtEmail.bottomAnchor, constant:10).isActive = true
        txtPassword.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        txtPassword.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        txtPasswordConfirmation.topAnchor.constraint(equalTo: txtPassword.bottomAnchor, constant:10).isActive = true
        txtPasswordConfirmation.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        txtPasswordConfirmation.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        btnSignUp.topAnchor.constraint(equalTo: txtPasswordConfirmation.bottomAnchor, constant:40).isActive = true
        btnSignUp.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        btnSignUp.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        activityIndicator.topAnchor.constraint(equalTo: btnSignUp.centerYAnchor).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: btnSignUp.centerXAnchor).isActive = true
    }
    
    
    // MARK : Helpers
    
    //keyboard notifications
    func subscribeToKeyboardNotifications(){
        //notification when showin kb
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        //notification hiding kb
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification:Notification) {
        let kbpoints = getKeyboardHeight(notification)
        if (kbpoints.0 - (activeTextField.frame.origin.y + activeTextField.frame.height)) < activeTextField.frame.size.height {
            view.frame.origin.y = -100
        } else {
            view.frame.origin.y = 0
        }
    }
    
    @objc func keyboardWillHide(_ notification:Notification) {
        view.frame.origin.y = 0
    }
    
    func getKeyboardHeight(_ notification: Notification) -> (CGFloat, CGFloat){
        let userInfo = notification.userInfo
        let keyboardSize = userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue
        print("keyboard \(keyboardSize.cgRectValue.origin.y)")
        return (keyboardSize.cgRectValue.origin.y, keyboardSize.cgRectValue.height)
    }
    
    //enable sign up btn
    func enableSignUpBtn(){
        if (isFirstNameSet && isLastNameSet && isEmailSet && isPasswordSet && isConfirmPasswordSet){
            self.btnSignUp.isEnabled = true
            self.btnSignUp.alpha = 1
        } else {
            self.btnSignUp.isEnabled = false
            self.btnSignUp.alpha = 0.5
        }
    }
    
    //update appearance signing up
    func updateAppearanceSigningUp(_ sender: Any){
        
        //show activtity indicator
        activityIndicator.startAnimating()
        
        //hide keyboard
        hideKeyboard(sender)
        
    }
    
    //update appearance signup finished
    func updateAppearanceSignUpFinished(_ sender: Any){
        
        // stop activity indicator
        self.activityIndicator.stopAnimating()
        
    }
    
    //clear text fields and disable button
    private func clearFields(){
        self.txtFirstName.text = ""
        self.txtLastName.text = ""
        self.txtEmail.text = ""
        self.txtPassword.text = ""
        self.txtPasswordConfirmation.text = ""
        self.btnSignUp.isEnabled = false
        self.btnSignUp.alpha = 0.5
    }
    
    // MARK: Action
    
    //hide keyboard
    @objc private func hideKeyboard(_ sender: Any){
        self.view.endEditing(true)
    }
    
    //signup
    @objc func signup(_ sender: UIButton){
        
        //check passwords match
        if (txtPassword.text != txtPasswordConfirmation.text){
            
            //show messaage that passwords don't match
            let alertView: UIAlertController = {
                let title = NSLocalizedString("Sign Up Failure", comment:"")
                let message =  NSLocalizedString("Passwords don't match", comment: "")
                let myAlertView = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
                //action
                let action = UIAlertAction(title: NSLocalizedString("Close", comment:""), style: UIAlertActionStyle.default, handler: nil)
                myAlertView.addAction(action)
                return myAlertView
            }()
        
            self.present(alertView, animated: true, completion: nil)
        }
        else { //passwords match create user
            
            //appearance for signing up
            updateAppearanceSigningUp(sender)
            
            //create user
            let user = JopaJopaUser(firstName: txtFirstName.text!, lastName: txtLastName.text!, email: txtEmail.text!, password: txtPassword.text!)
            
            // encode json
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            let data = try! encoder.encode(user)
            
            //json to string
            let userString = String(data: data, encoding: .utf8)!
            
            //call signup convenience method
            JopaJopaClient.sharedInstance().signup(jsonBody: userString){error in
                
                //updated appearnce
                performUIUpdatesOnMain {
                    self.updateAppearanceSignUpFinished(sender)
                }
                
                /* Display Success Message: */
                func displaySuccessMessage(){
                    //create alert  view
                    let alertView: UIAlertController = {
                        let title = NSLocalizedString("Account Created", comment: "")
                        let message =  NSLocalizedString("Before you can login, you must activate your account. Please check your e-mail", comment: "")
                        let myAlertView = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
                        //action
                        let action = UIAlertAction(title: NSLocalizedString("Close", comment:""), style: UIAlertActionStyle.default){handler in
                            self.clearFields()
                            self.tabBarController?.selectedIndex = 0
                        }
                        myAlertView.addAction(action)
                        return myAlertView
                    }()
                    
                    //ui update
                    performUIUpdatesOnMain {
                        self.present(alertView, animated: true, completion: nil)
                    }
                    
                }
                
                //check if login was successful
                if let error = error {
                    //displayError()
                    let alertController = JopaJopaClient.sharedInstance().getAlertControllerForErrors(error, JopaJopaClient.JopaJopaTask.Sign_Up)
                    performUIUpdatesOnMain {
                        self.present(alertController, animated: true, completion: nil)
                    }
                } else {
                    //if signup successful, show message
                    displaySuccessMessage()
                }
                
            }
            
        }
    }
    
}

// MARK : SignUpViewController Extension (TextField Delegate)

extension SignUpViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // get email and password from text
        var updatedFirstName = txtFirstName.text!
        var updatedLastName = txtLastName.text!
        var updatedEmail = txtEmail.text!
        var updatedPassword = txtPassword.text!
        var updatedPasswordConfirmation = txtPasswordConfirmation.text!
        
        if let firstName = txtFirstName.text,
            let textRange = Range(range, in: firstName) {
            if (textField == txtFirstName){
                updatedFirstName = firstName.replacingCharacters(in: textRange,
                                                         with: string)
            }
        }
        
        if let lastName = txtLastName.text,
            let textRange = Range(range, in: lastName) {
            if (textField == txtLastName){
                updatedLastName = lastName.replacingCharacters(in: textRange,
                                                         with: string)
            }
        }
        
        if let email = txtEmail.text,
            let textRange = Range(range, in: email) {
            if (textField == txtEmail){
                updatedEmail = email.replacingCharacters(in: textRange,
                                                         with: string)
            }
        }
        
        if let password = txtPassword.text,
            let textRange = Range(range, in: password) {
            if (textField == txtPassword){
                updatedPassword = password.replacingCharacters(in: textRange,
                                                               with: string)
            }
        }
        
        if let passwordConfirmation = txtPasswordConfirmation.text,
            let textRange = Range(range, in: passwordConfirmation) {
            if (textField == txtPasswordConfirmation){
                updatedPasswordConfirmation = passwordConfirmation.replacingCharacters(in: textRange, with: string)
            }
        }
        
        //enable signup button only if email is valid and password not empty
        //small bug: if email being edited and password edited again with backspace
        
        if (!updatedFirstName.isEmpty){
            self.isFirstNameSet = true
        } else {
            self.isFirstNameSet = false
        }
        
        if (!updatedLastName.isEmpty){
            self.isLastNameSet = true
        } else {
            self.isLastNameSet = false
        }
        
        if (!updatedEmail.isEmpty){
            self.isEmailSet = true
        } else {
            self.isEmailSet = false
        }
        
        if (!updatedPassword.isEmpty){
            self.isPasswordSet = true
        } else {
            self.isPasswordSet = false
        }
        
        if (!updatedPasswordConfirmation.isEmpty){
            self.isConfirmPasswordSet = true
        } else {
            self.isConfirmPasswordSet = false
        }
        
        //check enable sigup btn
        enableSignUpBtn()
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField as! InputTextField
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
