//
//  PlacesViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 20/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit
import PromiseKit

class PostsViewController: UIViewController {
    
    // MARK: Properties
    
    //store list of posts returned from db
    var posts: [JopaJopaPost] = [JopaJopaPost]()
    
    //refresh control boolean
    var isRefreshControlActive = false
    
    let nothingView: UIView = {
        let tempView = UIView()
        tempView.translatesAutoresizingMaskIntoConstraints = false
        return tempView
    }()
    
    let label: UILabel = {
        let lbl = UILabel()
        lbl.text = NSLocalizedString("You have no posts!", comment: "")
        lbl.textAlignment =  NSTextAlignment.center
        lbl.font = UIFont.preferredFont(forTextStyle: .body)
        lbl.numberOfLines = 0
        lbl.textColor = ViewConstants.LIGHT_TEXT_COLOR
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    //image view if posts is empty
    let imageView: UIImageView = {
        let imgView = UIImageView(image: #imageLiteral(resourceName: "poster"))
        imgView.translatesAutoresizingMaskIntoConstraints = false
        return imgView
    }()
    
    let tableView: UITableView = {
        let tblView = UITableView()
        tblView.translatesAutoresizingMaskIntoConstraints = false
        tblView.register(PostsTableViewCell.self, forCellReuseIdentifier: "cell")
        tblView.separatorStyle = UITableViewCellSeparatorStyle.none
        return tblView
    }()
    
    let dimView : UIView = {
        let view = UIView()
        view.backgroundColor = ViewConstants.BACKGROUND_COLOR_DIM
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let activityIndicator: UIActivityIndicatorView = {
        let actInd = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        actInd.translatesAutoresizingMaskIntoConstraints = false
        return actInd
    }()
    
    //refresh control
    let refreshControl: UIRefreshControl = {
        let rc = UIRefreshControl()
        rc.addTarget(self, action: #selector(refreshTest), for: .valueChanged)
        return rc
    }()
    
    //message box when place deleted or updated successfully
    let msgLabelSucccess : UILabel = {
        let lbl = UILabel()
        lbl.text = "Hello"
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textAlignment = NSTextAlignment.center
        lbl.backgroundColor = UIColor.black
        lbl.textColor = UIColor.white
        lbl.alpha = 0
        return lbl
    }()
    
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //title
        self.title = "Posts"
        
        //self.tabBarItem.isEnabled = false
 
        //data source and delegate
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        //add button to add place
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(showAddPost))
        
        //add sub views
        self.view.addSubview(nothingView)
        self.nothingView.addSubview(label)
        self.nothingView.addSubview(imageView)
        
        self.view.addSubview(tableView)
        
        if #available(iOS 10.0, *) {
            self.tableView.refreshControl = refreshControl
        } else {
            self.tableView.addSubview(refreshControl)
        }
        
        self.view.addSubview(dimView)
        self.dimView.addSubview(activityIndicator)
        
        //msg label
        self.view.addSubview(msgLabelSucccess)
        
        //setup
        setupLayout()
        
        //get posts
        getPosts()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //show tabbar if hidden
        self.tabBarController?.tabBar.isHidden = false
        
        //get posts
        if (!self.nothingView.isHidden){
            getPosts()
        }
        
    }
    
    // MARK: UI Layout
    
    func setupLayout(){
        
        //nothing view
        self.nothingView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.nothingView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        self.nothingView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.nothingView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        
        //imgView
        self.imageView.centerYAnchor.constraint(equalTo: self.nothingView.centerYAnchor, constant: 10).isActive = true
        self.imageView.centerXAnchor.constraint(equalTo: self.nothingView.centerXAnchor).isActive = true
        
        //lblMessage
        self.label.leadingAnchor.constraint(equalTo: self.nothingView.leadingAnchor, constant: 10).isActive = true
        self.label.trailingAnchor.constraint(equalTo: self.nothingView.trailingAnchor, constant: -10).isActive = true
        self.label.topAnchor.constraint(equalTo: self.imageView.bottomAnchor, constant: 5).isActive = true
        
        //table view
        self.tableView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        //dim view
        dimView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        dimView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        dimView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        activityIndicator.centerYAnchor.constraint(equalTo: self.dimView.centerYAnchor, constant:40).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: self.dimView.centerXAnchor).isActive = true
        
        msgLabelSucccess.heightAnchor.constraint(equalToConstant: (self.tabBarController?.tabBar.frame.height)!).isActive = true
        msgLabelSucccess.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -(self.tabBarController?.tabBar.frame.height)!).isActive = true
        msgLabelSucccess.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        msgLabelSucccess.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
    }
    
    // MARK : Helpers
    
    //update appearane getting posts
    func updateAppearanceGettingPosts(){
        
        //show dim view
        self.dimView.isHidden = false
        self.activityIndicator.startAnimating()
    }
    
    //update appearance got posts
    func updateAppearanceGotPosts(){
        
        //hide dim view
        self.activityIndicator.stopAnimating()
        self.dimView.isHidden = true
    }
    
    //update appearance post published
    func updateAppearancePostPublishedUnpublished(){
        
        //hide dim view
        self.activityIndicator.stopAnimating()
        self.dimView.isHidden = true
    }
    
    // MARK: Actions
    
    //refresh control table view
    @objc func refreshTest(_ sender: UIRefreshControl){
        
        self.isRefreshControlActive = true
        
        //get posts
        getPosts()
        
        self.isRefreshControlActive = false
        sender.endRefreshing()
    }
    
    
    // get posts
    func getPosts() {
        
        //update appearance
        if (!self.isRefreshControlActive){
            self.updateAppearanceGettingPosts()
        }
        
        //get posts list
        JopaJopaClient.sharedInstance().getPosts {posts, error in
            
            //update appearance
            performUIUpdatesOnMain {
                self.updateAppearanceGotPosts()
            }
            
            //check if request was successful
            if let error = error {
                //alert controller
                let alertController = JopaJopaClient.sharedInstance().getAlertControllerForErrors(error, JopaJopaClient.JopaJopaTask.Get_Places)
                
                // if error is authentication failure, log user out
                if (error.userInfo["JopaJopaErrorType"] as! String == JopaJopaClient.JopaJopaErrorType.AUTHENTICATION_FAILURE){
                    JopaJopaClient.sharedInstance().logoutUser()
                    let logoutAction = UIAlertAction(title: "Close", style: .default){ handler in
                        self.present(MainTabViewController(), animated: true, completion: nil)
                    }
                    alertController.addAction(logoutAction)
                }
                performUIUpdatesOnMain {
                    self.present(alertController, animated: true, completion: nil)
                }
            } else {
                //save posts
                self.posts = posts!
                
                if (posts!.isEmpty){
                    performUIUpdatesOnMain {
                        self.tableView.reloadData()
                        self.tableView.isHidden = true
                        self.nothingView.isHidden = false
                    }
                } else {
                    performUIUpdatesOnMain {
                        self.tableView.reloadData()
                        self.tableView.isHidden = false
                        self.nothingView.isHidden = true
                    }
                }
            }
            
        }
    }
    
    //add post button
    @objc func showAddPost(_sender: UIBarButtonItem){
        let addPostViewController = AddPostViewController()
        addPostViewController.delegate = self
        self.navigationController?.pushViewController(addPostViewController, animated: true)
    }
    
    
    
}

// MARK: TableView extension

extension PostsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return !(self.posts.isEmpty) ? self.posts.count : 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PostsTableViewCell
        let post = self.posts[indexPath.row]
        
        /* Set cell defaults */
        cell.postTitle.text = post.postTitle
        cell.postText.text = post.postText
        cell.lblPostCategory.text = post.postCategory
        cell.lblPostLocation.text = "\((post.placeIDs?.count)!) places"
        
        //calculate time reamining if post is still published
        if (post.postIsPublished!){
            cell.lblTimeRemaining.text = "Time Remaining: \(TimeDistanceHelper.secondsToHoursMinutesSecondsColonFormatted(seconds: post.postTimeRemaining!))"
        } else {
            cell.lblTimeRemaining.text = "Time Remaining: 00:00:00"
        }
        
        //add publish unpublish action
        cell.btnPublishUnpublish.addTarget(self, action: #selector(publishUnpublishPost), for: .touchUpInside)
        cell.btnPublishUnpublish.tag = indexPath.row
        
        //check if post is a published post
        if (post.postIsPublished)!{
            cell.btnPublishUnpublish.setTitle(NSLocalizedString("Unpublish", comment: ""), for: .normal)
            cell.btnPublishUnpublish.setTitleColor(UIColor.red, for: .normal)
        } else {
            cell.btnPublishUnpublish.setTitle(NSLocalizedString("Publish", comment: ""), for: .normal)
            cell.btnPublishUnpublish.setTitleColor(UIColor.black, for: .normal)
        }
        
        // get post image
        let url = URL(string: post.postImageURL!)
        JopaJopaClient.sharedInstance().taskForGETImageFromSpaces(url: url!, useCachedImage: true){ imageData, error in
            
            if (error != nil){
                performUIUpdatesOnMain {
                    cell.cellImageView.image = UIImage(named: "defaultImage")
                }
                
            } else
            {
                if let imageData = imageData {
                    performUIUpdatesOnMain {
                        cell.cellImageView.image = UIImage(data: imageData)
                    }
                } 
            }
        }
        
        //return cell
        return cell
    }
    
    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let post = self.posts[indexPath.row]

        let postDetailViewController = PostDetailViewController()
        postDetailViewController.delegate = self
        postDetailViewController.indexPath = indexPath
        
        postDetailViewController.post = post
        postDetailViewController.txtPostTitle.text = post.postTitle
        postDetailViewController.txtPostText.text = post.postText
        postDetailViewController.selectedPlacesId = Set((post.placeIDs)!)
        
        postDetailViewController.btnPostLocation.setTitle("\((post.placeIDs?.count)!) places", for: .normal)
        postDetailViewController.btnPostLocation.setTitleColor(UIColor.black, for: .normal)
        
        postDetailViewController.btnPostCategory.setTitle(post.postCategory, for: .normal)
        postDetailViewController.btnPostCategory.setTitleColor(UIColor.black, for: .normal)
        
        let cell = tableView.cellForRow(at: indexPath) as! PostsTableViewCell
        postDetailViewController.imgViewPost.image = cell.cellImageView.image
        
        postDetailViewController.imgViewAddButton.isHidden = true
        postDetailViewController.lblPlaceHolder.isHidden = true
        postDetailViewController.txtPostText.textColor = UIColor.black
        
        postDetailViewController.isPostTextSet = true
        postDetailViewController.isPostTitleSet = true
        postDetailViewController.isPostImageSelected = true
        postDetailViewController.isPostPlacesSelected = true
        postDetailViewController.isPostCategorySelected = true
                
        self.navigationController?.pushViewController(postDetailViewController, animated: true)
    }
    
    //insert / delete row
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        //delete
        if editingStyle == UITableViewCellEditingStyle.delete {
            self.posts.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        }
        
        //insert
        if editingStyle == UITableViewCellEditingStyle.insert{
            tableView.insertRows(at: [indexPath], with: UITableViewRowAnimation.bottom)
        }
    }
    
}

// MARK : Extension AddPostViewControllerDelegate

extension PostsViewController : AddPostViewControllerDelegate {
   
    func didFinishAddingPost(_ post: JopaJopaPost) {
        
        //insert new place at index 0
        self.posts.insert(post, at: 0)
        
        let indexPath = IndexPath(row: 0, section: 0)
        self.tableView(tableView, commit: .insert, forRowAt: indexPath)
        
        //scroll to top
        self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: true)
        
        //show message
        self.msgLabelSucccess.alpha = 1
        self.msgLabelSucccess.text = NSLocalizedString("Post Added", comment: "")
        UIView.animate(withDuration: 3){
            self.msgLabelSucccess.alpha = 0
        }
    }
    
    
}

// MARK : Extension PostDetailViewControllerDelegate

extension PostsViewController :  PostDetailViewControllerDelegate {
    func didFinishUpdatingPost(_ postDetailViewController: PostDetailViewController) {
        
        //update cell
        let indexPath = postDetailViewController.indexPath
        
        let cell = self.tableView.cellForRow(at: indexPath!) as! PostsTableViewCell
        cell.cellImageView.image = postDetailViewController.imgViewPost.image
        cell.postTitle.text = postDetailViewController.txtPostTitle.text
        cell.postText.text = postDetailViewController.txtPostText.text
        cell.lblPostLocation.text = postDetailViewController.btnPostLocation.title(for: .normal)
        cell.lblPostCategory.text = postDetailViewController.btnPostCategory.title(for: .normal)
        
        //update post
        let index = indexPath!.row
        posts[index].placeIDs = postDetailViewController.post.placeIDs
        posts[index].postCategory = postDetailViewController.post.postCategory
        posts[index].postTitle = postDetailViewController.post.postTitle
        posts[index].postText = postDetailViewController.post.postText
        
        //show message
        self.msgLabelSucccess.alpha = 1
        self.msgLabelSucccess.text = NSLocalizedString("Post Updated", comment: "")
        UIView.animate(withDuration: 3){
            self.msgLabelSucccess.alpha = 0
        }
    }
    
    func didFinishDeletingPost(_ indexPath: IndexPath) {
        self.tableView(tableView, commit: .delete, forRowAt: indexPath)
        
        //show message
        self.msgLabelSucccess.alpha = 1
        self.msgLabelSucccess.text = NSLocalizedString("Post Deleted", comment: "")
        UIView.animate(withDuration: 3){
            self.msgLabelSucccess.alpha = 0
        }
    }
}

//extension PostsViewController : CustomModalViewControllerDelegate {
//
//    func didCancelSelecting(viewController: UIViewController) {
//
//    }
//
//    func didFinishSelecting(viewController: UIViewController) {
////        print("value selected:" + (viewController as! CustomModalViewController).selectedValue!)
//    }
//}



// MARK : Extension contains functions for publishing / unpublishing posts
extension PostsViewController {
    
    // publish unpublish post
    @objc func publishUnpublishPost(_ sender: UIButton){
        
        // post to be acted on
        let post = self.posts[sender.tag]
        
        //the duration selected if post is to be published
        var postDuration: Int?
        
        //controller confirm unpublish post
        let confirmUnpublishAlertController : UIAlertController = {
            let ac = UIAlertController(title: NSLocalizedString("Unpublish Post", comment: ""), message: NSLocalizedString("Your post will be unpublished", comment: ""), preferredStyle: .alert)
            let a1 = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            let a2 = UIAlertAction(title: "OK", style: .default){ handler in
                self.proceedPublishUnpublishPost(tag: sender.tag, post: post, postDuration: postDuration)
            }
            ac.addAction(a1)
            ac.addAction(a2)
            return ac
        }()
        
        //controller confirm unpublish post
        let confirmPublishAlertController : UIAlertController = {
            let ac = UIAlertController(title: NSLocalizedString("Publish Post", comment: ""), message: NSLocalizedString("Your post will be published. You will not be able to update your post until you unpublish it first", comment: ""), preferredStyle: .alert)
            let a1 = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            let a2 = UIAlertAction(title: "OK", style: .default){ handler in
                self.proceedPublishUnpublishPost(tag: sender.tag, post: post, postDuration: postDuration)
            }
            ac.addAction(a1)
            ac.addAction(a2)
            return ac
        }()
        
        //controller post duration
        let postDurationAlertController : UIAlertController = {
            let ac = UIAlertController(title: NSLocalizedString("Post Duration", comment: ""), message: NSLocalizedString("How long would you like the post to be published for?", comment: ""), preferredStyle: .actionSheet)
            let a1 = UIAlertAction(title: JopaJopaClient.PostDurationKeys.TwoHours, style: .default) { handler in
                postDuration = JopaJopaClient.PostDurationValues.TwoHours
                self.present(confirmPublishAlertController, animated: true, completion: nil)
            }
            let a2 = UIAlertAction(title: JopaJopaClient.PostDurationKeys.FourHours, style: .default) { handler in
                postDuration = JopaJopaClient.PostDurationValues.FourHours
                self.present(confirmPublishAlertController, animated: true, completion: nil)
            }
            let a3 = UIAlertAction(title: JopaJopaClient.PostDurationKeys.EightHours, style: .default) { handler in
                postDuration = JopaJopaClient.PostDurationValues.EightHours
                self.present(confirmPublishAlertController, animated: true, completion: nil)
            }
            let a4 = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
            ac.addAction(a1)
            ac.addAction(a2)
            ac.addAction(a3)
            ac.addAction(a4)
            return ac
        }()
        
        
        //check if post is a published post or not
        if post.postIsPublished! {
            self.present(confirmUnpublishAlertController, animated: true, completion: nil)
        } else {
            self.present(postDurationAlertController, animated: true, completion: nil)
        }
        
    }
    
    
    //proceed publish post
    func proceedPublishUnpublishPost(tag: Int, post: JopaJopaPost, postDuration: Int?) {
        
        //update appearance
        updateAppearanceGettingPosts()
        
        //publish post if unpublished and vice versa
        let jsonString: String!
        let messageAfterAction: String!
        
        //form jsonString according to action to be taken
        if post.postIsPublished! {
            //post to be removed, set fields for HTTP Patch
            jsonString = "{\"operation\":\"unpublish\"}"
            //jsonString = "{\"path\":\"\(JopaJopaClient.JSONReponseKeys.PostIsPublished)\", \"value\":\"false\", \"op\": \"replace\"}"
            messageAfterAction = NSLocalizedString("Post was unpublished", comment: "")
        } else {
            //post to be published, set fields for HTTP Patch, note "postDuration"
            jsonString = "{\"operation\":\"publish\", \"postDuration\":\(postDuration!)}"
            //jsonString = "{\"path\":\"\(JopaJopaClient.JSONReponseKeys.PostIsPublished)\", \"value\":\"true\", \"op\": \"replace\", \"postDuration\":\(postDuration!)}"
            messageAfterAction = NSLocalizedString("Post was published", comment: "")
        }
        
        // call convenience method to publish unpublish post
        firstly {
            JopaJopaClient.sharedInstance().postPublishUnpublish(jsonBody: jsonString, postID: post.postID!)
            }
            .done {result in
                
                let indexPath = IndexPath(row: tag, section: 0)
                let cell = self.tableView.cellForRow(at: indexPath) as! PostsTableViewCell
                
                post.postIsPublished = !post.postIsPublished!
                
                if post.postIsPublished! {
                    //update post
                    
                    performUIUpdatesOnMain {
                        self.updateAppearancePostPublishedUnpublished()
                        cell.btnPublishUnpublish.setTitle("Unpublish", for: .normal)
                        cell.btnPublishUnpublish.setTitleColor(UIColor.red, for: .normal)
                        cell.lblTimeRemaining.text = "Time Remaining: \(postDuration!):00:00"
                        
                        //show message
                        self.msgLabelSucccess.alpha = 1
                        self.msgLabelSucccess.text = NSLocalizedString("Post Published", comment: "")
                        UIView.animate(withDuration: 3){
                            self.msgLabelSucccess.alpha = 0
                        }
                    }
                    
                } else {
                    performUIUpdatesOnMain {
                        self.updateAppearancePostPublishedUnpublished()
                        cell.btnPublishUnpublish.setTitle("Publish", for: .normal)
                        cell.btnPublishUnpublish.setTitleColor(UIColor.black, for: .normal)
                        cell.lblTimeRemaining.text = "Time Remaining: 00:00:00"
                        //show message
                        self.msgLabelSucccess.alpha = 1
                        self.msgLabelSucccess.text = NSLocalizedString("Post Unpublished", comment: "")
                        UIView.animate(withDuration: 3){
                            self.msgLabelSucccess.alpha = 0
                        }
                    }
                    
                }
                
            }
            .catch {error in
                
                //create alert view
                let alertView: UIAlertController = {
                    let title = NSLocalizedString("Problem Publishing Post", comment:"")
                    let message =  NSLocalizedString("Post could not be published", comment:"")
                    let myAlertView = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
                    //action
                    let action = UIAlertAction(title: NSLocalizedString("Close", comment:""), style: UIAlertActionStyle.default, handler: nil)
                    myAlertView.addAction(action)
                    return myAlertView
                }()
                
                performUIUpdatesOnMain {
                    self.updateAppearancePostPublishedUnpublished()
                    self.present(alertView, animated: true, completion: nil)
                }
                
        }
    }
    
}


