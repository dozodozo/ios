//
//  AddPostViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 2/7/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit
import PromiseKit

// MARK : AddPostViewControllerDelegate

protocol  AddPostViewControllerDelegate {
    func didFinishAddingPost(_ post : JopaJopaPost)
}

class AddPostViewController: UIViewController {
    
    // MARK: Properties
    
    var selectedPlacesId : Set<String> = Set<String>()
    
    let noCharsPostTitle = 30
    let noCharsPostText = 150
    
    var isPostImageSelected = false
    var isPostPlacesSelected = false
    var isPostCategorySelected = false
    var isPostTitleSet = false
    var isPostTextSet = false
    
    //keep tracks of whether scrollView was adjusted when keyboard shows
    var scrollViewAdjusted = false
    
    //delegate
    var delegate : AddPostViewControllerDelegate?
    
    // scrollView
    let scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    // container for subviews
    let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // post image
    let imgViewPost: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.gray
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        return imageView
    }()
    
    let imgViewAddButton: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "addImageGray"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let imgViewLocation: UIImageView = {
        let tintableImage = UIImage(named: "locationPin")?.withRenderingMode(.alwaysTemplate)
        let imageView = UIImageView()
        imageView.image = tintableImage
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let btnPostLocation: UIButton = {
        let btn = UIButton()
        btn.setTitle("Add Place", for: .normal)
        btn.setTitleColor(UIColor.black, for: .normal)
        btn.layer.borderColor = ViewConstants.LIGHT_TEXT_COLOR.cgColor
        btn.layer.borderWidth = 1.0
        btn.layer.cornerRadius = 20
        btn.addTarget(self, action: #selector(addPlace), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitleColor(ViewConstants.LIGHT_TEXT_COLOR, for: .normal)
        btn.titleLabel?.font = UIFont.preferredFont(forTextStyle: .footnote)
        return btn
    }()
    
    let btnPostCategory: UIButton = {
        let btn = UIButton()
        btn.setTitle("Add Category", for: .normal)
        btn.setTitleColor(ViewConstants.LIGHT_TEXT_COLOR, for: .normal)
        btn.titleLabel?.font = UIFont.preferredFont(forTextStyle: .footnote)
        btn.layer.borderColor = ViewConstants.LIGHT_TEXT_COLOR.cgColor
        btn.layer.borderWidth = 1.0
        btn.layer.cornerRadius = 20
        btn.addTarget(self, action: #selector(addCategory), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let imgViewPostCategory: UIImageView = {
        let tintableImage = UIImage(named: "category")?.withRenderingMode(.alwaysTemplate)
        let imageView = UIImageView()
        imageView.image = tintableImage
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let txtPostTitle: InputTextField = {
        let txtField = InputTextField()
        txtField.font = UIFont.preferredFont(forTextStyle: .subheadline).bold()
        txtField.placeholder = "Post Title"
        txtField.translatesAutoresizingMaskIntoConstraints = false
        return txtField
    }()
    
    let lblNoCharsRemainingPostTitle: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = ViewConstants.LIGHT_TEXT_COLOR
        lbl.font = UIFont.preferredFont(forTextStyle: .footnote)
        return lbl
    }()

    let txtPostText: UITextView = {
        let txtView = UITextView()
        txtView.text = ""
        txtView.font = UIFont.preferredFont(forTextStyle: .subheadline)
        txtView.textColor = ViewConstants.LIGHT_TEXT_COLOR
        txtView.translatesAutoresizingMaskIntoConstraints = false
        txtView.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).cgColor
        txtView.layer.borderWidth = 1.0
        txtView.layer.cornerRadius = 1
        return txtView
    }()
    
    let lblNoCharsRemainingPostText: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = ViewConstants.LIGHT_TEXT_COLOR
        lbl.font = UIFont.preferredFont(forTextStyle: .footnote)
        return lbl
    }()
    
    let lblPlaceHolder: UILabel = {
        let lbl = UILabel()
        lbl.text = NSLocalizedString("Post Caption", comment: "")
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = ViewConstants.LIGHT_TEXT_COLOR
        lbl.font = UIFont.preferredFont(forTextStyle: .footnote)
        return lbl
    }()
    
    let btnSave: UIBarButtonItem = {
        let btn = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.plain, target: self, action: #selector(savePost))
        btn.isEnabled = false
        return btn
    }()
    
    // MARK: Activity Indicator
    
    let dimView : UIView = {
        let view = UIView()
        view.backgroundColor = ViewConstants.BACKGROUND_COLOR_DIM
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let activityIndicator : UIActivityIndicatorView = {
        let ai = UIActivityIndicatorView()
        ai.color = ViewConstants.BUTTON_COLOR_DEFAULT
        ai.translatesAutoresizingMaskIntoConstraints = false
        return ai
    }()
   
   
    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //view title
        self.title = NSLocalizedString("Add Post", comment: "")
        
        //hide tab bar
        self.tabBarController?.tabBar.isHidden = true
        
        //right bar button item
        self.navigationItem.rightBarButtonItem = btnSave
       
        //start activity indicator - it is only visible when user clicks save
        activityIndicator.startAnimating()
        
        //tap gesture for selecting places
        let tapGestureRecoginizer = UITapGestureRecognizer(target: self, action: #selector(selectImage))
        imgViewPost.addGestureRecognizer(tapGestureRecoginizer)
        imgViewPost.isUserInteractionEnabled = true
        
        // keyboard notfications for adjusting scrollview
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardWillHide, object: nil)

        //assign delegates
        txtPostTitle.delegate = self
        txtPostText.delegate = self
        
        //assing values to labels
        lblNoCharsRemainingPostTitle.text = "\(noCharsPostTitle)"
        lblNoCharsRemainingPostText.text = "\(noCharsPostText)"
        
        self.view.addSubview(scrollView)
        self.scrollView.addSubview(containerView)
        self.containerView.addSubview(imgViewPost)
        self.containerView.addSubview(imgViewAddButton)
        self.containerView.addSubview(imgViewLocation)
        self.containerView.addSubview(btnPostLocation)
        self.containerView.addSubview(btnPostCategory)
        self.containerView.addSubview(imgViewPostCategory)
        self.containerView.addSubview(txtPostTitle)
        self.containerView.addSubview(lblNoCharsRemainingPostTitle)
        self.containerView.addSubview(txtPostText)
        self.containerView.addSubview(lblNoCharsRemainingPostText)
        self.containerView.addSubview(lblPlaceHolder)
        
        self.view.addSubview(dimView)
        
        //setup layout
        setupLayout()
        
    }

    
    // MARK: Layout
    
    func setupLayout(){
        scrollView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
        /*
         important: container must be anchored to scroll view in Left-Right, Top-Bottom order
         bottom anchor must be anchored to scroll view bottom anchor
         size cannot depend on scroll view
         */
        containerView.leadingAnchor.constraint(equalTo: self.scrollView.leadingAnchor).isActive = true
        containerView.topAnchor.constraint(equalTo: self.scrollView.topAnchor).isActive = true
        containerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        containerView.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor).isActive = true
        //containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        
        
        imgViewPost.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        imgViewPost.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        imgViewPost.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        imgViewPost.heightAnchor.constraint(equalTo: imgViewPost.widthAnchor).isActive = true
        
        imgViewAddButton.centerXAnchor.constraint(equalTo: imgViewPost.centerXAnchor).isActive = true
        imgViewAddButton.centerYAnchor.constraint(equalTo:imgViewPost.centerYAnchor).isActive = true
        imgViewAddButton.widthAnchor.constraint(equalToConstant: view.frame.width/4).isActive = true
        imgViewAddButton.heightAnchor.constraint(equalTo: imgViewAddButton.widthAnchor).isActive = true
        
        btnPostLocation.topAnchor.constraint(equalTo: imgViewPost.bottomAnchor, constant:20).isActive = true
        btnPostLocation.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant:10).isActive = true
        btnPostLocation.trailingAnchor.constraint(equalTo: containerView.centerXAnchor, constant: -2.5).isActive = true
        //        btnPostLocation.widthAnchor.constraint(equalToConstant: view.frame.size.width/2 - 5).isActive = true
        btnPostLocation.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        imgViewLocation.topAnchor.constraint(equalTo: btnPostLocation.topAnchor, constant:12.5 ).isActive = true
        imgViewLocation.leadingAnchor.constraint(equalTo: btnPostLocation.leadingAnchor, constant:10).isActive = true
        imgViewLocation.widthAnchor.constraint(equalToConstant: 15).isActive = true
        imgViewLocation.heightAnchor.constraint(equalTo: imgViewLocation.widthAnchor).isActive = true
        
        
        btnPostCategory.topAnchor.constraint(equalTo: imgViewPost.bottomAnchor, constant:20).isActive = true
        btnPostCategory.leadingAnchor.constraint(equalTo: containerView.centerXAnchor, constant: 2.5).isActive = true
        btnPostCategory.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10).isActive = true
        btnPostCategory.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        
        imgViewPostCategory.topAnchor.constraint(equalTo: btnPostCategory.topAnchor, constant:12.5 ).isActive = true
        imgViewPostCategory.leadingAnchor.constraint(equalTo: btnPostCategory.leadingAnchor, constant:10).isActive = true
        imgViewPostCategory.widthAnchor.constraint(equalToConstant: 15).isActive = true
        imgViewPostCategory.heightAnchor.constraint(equalTo: imgViewPostCategory.widthAnchor).isActive = true
        
        txtPostTitle.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant:5).isActive = true
        txtPostTitle.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant:-25).isActive = true
        txtPostTitle.topAnchor.constraint(equalTo: btnPostCategory.bottomAnchor, constant:20).isActive = true
        
        lblNoCharsRemainingPostTitle.leadingAnchor.constraint(equalTo: txtPostTitle.trailingAnchor, constant: 0).isActive = true
        lblNoCharsRemainingPostTitle.centerYAnchor.constraint(equalTo: txtPostTitle.centerYAnchor, constant: 0).isActive = true
        
        txtPostText.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant:10).isActive = true
        txtPostText
            .topAnchor.constraint(equalTo: txtPostTitle.bottomAnchor, constant:-5).isActive = true
        txtPostText.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant:-10).isActive = true
        txtPostText.heightAnchor.constraint(equalToConstant: 80).isActive = true
        txtPostText.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        
        lblNoCharsRemainingPostText.leadingAnchor.constraint(equalTo: txtPostText.safeAreaLayoutGuide.trailingAnchor, constant: -25).isActive = true
        lblNoCharsRemainingPostText.bottomAnchor.constraint(equalTo: txtPostText.bottomAnchor, constant: 0).isActive = true
        
        lblPlaceHolder.topAnchor.constraint(equalTo: txtPostText.topAnchor, constant:8).isActive = true
        lblPlaceHolder.leadingAnchor.constraint(equalTo: txtPostText.leadingAnchor, constant: 8).isActive = true
        
        dimView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        dimView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        dimView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

    }
    
    // MARK: Helpers
    
    // keyboard show/hide adjust scrollview
    @objc func keyboardWillShow(notification: NSNotification) {
        
        //adjust contentInset of scroll view
        let keyboardInfo = (notification.userInfo![UIKeyboardFrameEndUserInfoKey]) as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        
        //if contentInset has NOT already been set to keyboard height, set it to keyboard height
        if (!scrollViewAdjusted) {
            scrollViewAdjusted = true
            let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height + txtPostText.frame.size.height, right: 0)
            self.scrollView.contentInset = contentInset
            
            //move scrollview up
            self.scrollView.bounds.origin.y += keyboardSize.height + txtPostText.frame.size.height
        }
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        scrollViewAdjusted = false
        
        //set contentInset to original position
        self.scrollView.contentInset.bottom = txtPostText.frame.size.height
        
        //move scrollview back to original position
        self.scrollView.bounds.origin.y = 0
    }
    
    //enable save button
    func enableSaveButton(){
        if (isPostImageSelected && isPostCategorySelected && isPostPlacesSelected &&  isPostTextSet && isPostTitleSet){
            self.btnSave.isEnabled = true
        } else {
            self.btnSave.isEnabled = false
        }
        
    }
    
    //update appearanec savign post
    func updateAppearanceSavingPost(_ sender: Any?){
        //hide keboard
        hideKeyboard(sender!)
        //show dim view
        self.dimView.isHidden = false
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activityIndicator)
    }
    
    //update appearance savingPostDone
    func updateAppearanceSavingPostDone(_ sender: Any?){
        //hide dim view
        self.dimView.isHidden = true
        self.navigationItem.rightBarButtonItem =  UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.savePost))
    }
    
    // MARK: Actions
    
    //hide keyboard
    @objc private func hideKeyboard(_ sender: Any){
        self.view.endEditing(true)
    }
    
    // launch image picker
    @objc func selectImage(_sender: UIImageView){
        let imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        let alertControllerTitle = NSLocalizedString("Select Source", comment: "")
        let cameraTitle = NSLocalizedString("Camera", comment: "")
        let libraryTitle = NSLocalizedString("Photo Library", comment: "")
        
        //show options for camera or library
        let alertController = UIAlertController(title: alertControllerTitle, message: "", preferredStyle: .actionSheet)
        
        //camera
        let camera = UIAlertAction(title: cameraTitle, style: .default){handler in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                imagePicker.sourceType = .camera
                imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
                self.present(imagePicker, animated: true, completion: nil)
            } else {
                let error = NSError(domain: "TaskForSelectImage", code: 1, userInfo : ["JopaJopaErrorType" : JopaJopaClient.JopaJopaErrorType.MEDIA_SOURCE_NOT_AVAILABLE])
                self.present(JopaJopaClient.sharedInstance().getAlertControllerForErrors(error, ""), animated: true, completion: nil)
            }
        }
        
        //photo library
        let library = UIAlertAction(title: libraryTitle, style: .default){ handler in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                imagePicker.sourceType = .photoLibrary
                imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
                self.present(imagePicker, animated: true, completion: nil)
            } else {
                let error = NSError(domain: "TaskForSelect", code: 1, userInfo : ["JopaJopaErrorType" : JopaJopaClient.JopaJopaErrorType.MEDIA_SOURCE_NOT_AVAILABLE])
                self.present(JopaJopaClient.sharedInstance().getAlertControllerForErrors(error, ""), animated: true, completion: nil)
            }
        }
        
        //cancel
        let cancel = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        
        //add actions
        alertController.addAction(camera)
        alertController.addAction(library)
        alertController.addAction(cancel)
        
        //show imagepicker option
        self.present(alertController, animated: true, completion: nil)
    }
    
    // select place
    @objc func addPlace(_ sender: UIButton){
        let selectPlacesViewController = SelectPlacesViewController()
        selectPlacesViewController.delegate = self
        selectPlacesViewController.selectedPlacesId = self.selectedPlacesId
        let nc = UINavigationController(rootViewController: selectPlacesViewController)
        self.navigationController?.present(nc, animated: true, completion: nil)
    }
    
    // select category
    @objc func addCategory(_ sender: UIButton){
        
        //show activity view
       // self.dimView.isHidden = false
        
        //controller post category
        let postCategoryAlertController : UIAlertController = {
            let ac = UIAlertController(title: NSLocalizedString("Post Category", comment: ""), message: NSLocalizedString("Select a post cateogory", comment: ""), preferredStyle: .actionSheet)
            let a1 = UIAlertAction(title: JopaJopaClient.PostCategories.eatDrink, style: .default) { handler in
                self.btnPostCategory.setTitle(JopaJopaClient.PostCategories.eatDrink, for: .normal)
                self.btnPostCategory.setTitleColor(UIColor.black, for: .normal)
                self.isPostCategorySelected = true
            }
            let a2 = UIAlertAction(title: JopaJopaClient.PostCategories.shop, style: .default) { handler in
                self.btnPostCategory.setTitle(JopaJopaClient.PostCategories.shop, for: .normal)
                self.btnPostCategory.setTitleColor(UIColor.black, for: .normal)
                self.isPostCategorySelected = true
            }
            let a3 = UIAlertAction(title: JopaJopaClient.PostCategories.whatsOn, style: .default) { handler in
                self.btnPostCategory.setTitle(JopaJopaClient.PostCategories.whatsOn, for: .normal)
                self.btnPostCategory.setTitleColor(UIColor.black, for: .normal)
                self.isPostCategorySelected = true
            }
            let a4 = UIAlertAction(title: JopaJopaClient.PostCategories.healthBeauty, style: .default) { handler in
                self.btnPostCategory.setTitle(JopaJopaClient.PostCategories.healthBeauty, for: .normal)
                self.btnPostCategory.setTitleColor(UIColor.black, for: .normal)
                self.isPostCategorySelected = true
            }
            let a5 = UIAlertAction(title: JopaJopaClient.PostCategories.community, style: .default) { handler in
                self.btnPostCategory.setTitle(JopaJopaClient.PostCategories.community, for: .normal)
                self.btnPostCategory.setTitleColor(UIColor.black, for: .normal)
                self.isPostCategorySelected = true
            }
            let a6 = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
            
            ac.addAction(a1)
            ac.addAction(a2)
            ac.addAction(a3)
            ac.addAction(a4)
            ac.addAction(a5)
            ac.addAction(a6)
            return ac
        }()
        
        //show category selector
        self.present(postCategoryAlertController, animated: true, completion: nil)
        
    }
    
    // save post
    @objc func savePost(_ sender: UIBarButtonItem){
        
        //update appearance
        updateAppearanceSavingPost(sender)
        
        //convert selectedPlacesId to an array
        let placeIDs = Array(selectedPlacesId)
        
        //create post - note timeRemaining is 0
        let post = JopaJopaPost(userID: JopaJopaClient.sharedInstance().getUserID(), postTitle: txtPostTitle.text, postText: txtPostText.text, postIsPublished: false, postCategory: btnPostCategory.titleLabel?.text, placeIDs: placeIDs, postTimeRemaining: 0)
        
        
        // encode json
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let data = try! encoder.encode(post)
        
        //json to string
        let postString = String(data: data, encoding: .utf8)!
        
        // add place with expiry (ttl) and get signed url
        var savedPostID : String! //id of place returned by db
        var postImageURL: String?
        
        //call conveniece method to save
        firstly {
            JopaJopaClient.sharedInstance().addPost(jsonBody: postString)
        }
        // upload image to digital ocean spaces
        .then {(postID, url) -> Promise<Bool> in
            //image data
            savedPostID = postID
            
            //extract image url from spaces image authorized url
            let urlString = "\(url)"
            let indexParams = urlString.index(of: "?")
            postImageURL = String(urlString[..<indexParams!])
            
            let img = self.imgViewPost.image?.resized(toWidth: 400)
            let image_data = UIImageJPEGRepresentation(img!, 0.5)!
            return JopaJopaClient.sharedInstance().uploadImageToSpaces(signedURL: url, data: image_data)
        }
        //if image upload succsseful, remove data expiry from place
        .then { (uploadSucceded) -> Promise<Bool> in
            let jsonString = "{\"path\":\"\(JopaJopaClient.JSONReponseKeys.PostDateExpiry)\", \"value\":\"\", \"op\": \"remove\"}"
            return JopaJopaClient.sharedInstance().postRemoveDateExpiry(jsonBody: jsonString, postID: savedPostID)
        }
        .done { result in
            
            //update appearance
            performUIUpdatesOnMain {
                self.updateAppearanceSavingPostDone(sender)
            }
            
            //update post to send back to delegate
            post.postID = savedPostID
            post.postImageURL = postImageURL
            
            self.navigationController?.popViewController(animated: true)
            self.delegate?.didFinishAddingPost(post)
            
        }
        .catch{ error in
            
            //update appearance
            performUIUpdatesOnMain {
                self.updateAppearanceSavingPostDone(sender)
            }
            
            let nsError = error as NSError
            
            //alert controller
            let alertController = JopaJopaClient.sharedInstance().getAlertControllerForErrors(nsError, JopaJopaClient.JopaJopaTask.Add_Post)
            
            // if error is authentication failure, log user out
            if (nsError.userInfo["JopaJopaErrorType"] as! String == JopaJopaClient.JopaJopaErrorType.AUTHENTICATION_FAILURE){
                JopaJopaClient.sharedInstance().logoutUser()
                let logoutAction = UIAlertAction(title: "Close", style: .default){ handler in
                    self.present(MainTabViewController(), animated: true, completion: nil)
                }
                alertController.addAction(logoutAction)
            }
            performUIUpdatesOnMain {
                self.present(alertController, animated: true, completion: nil)
            }
            
        }
        
    }

}


// MARK: Text Field Delegates

extension AddPostViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // get email and password from text
        var updatedPostTitle = txtPostTitle.text!
        
        if let postTitle = txtPostTitle.text,
            let textRange = Range(range, in: postTitle) {
            updatedPostTitle = postTitle.replacingCharacters(in: textRange,
                                                         with: string)
            //check to enable save button
            if (updatedPostTitle != ""){
                isPostTitleSet  = true
            } else {
                isPostTitleSet  = false
            }
            enableSaveButton()
            
            //update character count label
            let n = noCharsPostTitle - updatedPostTitle.count
            lblNoCharsRemainingPostTitle.text = n < 0 ? "\(0)" : "\(n)"
            if (updatedPostTitle.count <= noCharsPostTitle) {
                return true
            }
        }
        
        return false
    }
    

}


// MARK: Extension UITextViewDelegate


extension AddPostViewController: UITextViewDelegate {
    
    //hide textview placeholder
    func textViewDidChange(_ textView: UITextView) {
        if(textView.text != ""){
            lblPlaceHolder.isHidden = true
            textView.textColor = UIColor.black
        } else {
            lblPlaceHolder.isHidden = false
            textView.textColor = ViewConstants.LIGHT_TEXT_COLOR
        }
    }
        
    //
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        //ignore return by resigning first responder
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        
        // get email and password from text
        var updatedPostText = txtPostText.text!
        
        if let postText = txtPostText.text,
            let textRange = Range(range, in: postText) {
            updatedPostText = postText.replacingCharacters(in: textRange,
                                                             with: text)
            
             //check to enable save button
            if (updatedPostText != ""){
                isPostTextSet  = true
            } else {
                isPostTextSet  = false
            }
            
            //enable save btn
            enableSaveButton()
            
            //update character count label
            let n = noCharsPostText - updatedPostText.count
            lblNoCharsRemainingPostText.text = n < 0 ? "\(0)" : "\(n)"
            if (updatedPostText.count <= noCharsPostText) {
                return true
            }
        }
        
        return false
    }
    
}

// MARK: ImagePickerControllerDelegate

extension AddPostViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //image picked
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        //get chosen image
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        //dismiss picker
        dismiss(animated: true, completion: nil)
        
        //instantiate ImageCropperViewController
        let imageCropperViewController = ImageCropperViewController(image: chosenImage)
        
        //make this view controller the delegate
        imageCropperViewController.delegate = self
        
        //presnet ImageCropperViewController
        self.present(imageCropperViewController, animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}

// MARK: Extension ImageCropperViewControllerDelegate

extension AddPostViewController: ImageCropperViewControllerDelegate {
    
    func imageDidFinishCropping(imageCropperViewController: ImageCropperViewController) {
        self.imgViewPost.image = imageCropperViewController.croppedImage
        self.imgViewAddButton.isHidden = true
        
        self.isPostImageSelected = true
        
        //check to enable save button
        enableSaveButton()
    }
}

// MARK: Extension SelectPlacesViewControllerDelegate

extension AddPostViewController: SelectPlacesViewControllerDelegate {
    
    func didFinishSelectingPlaces(selectPlacesViewController: SelectPlacesViewController) {
        //self.txtPostLocation.text = "\(selectPlacesViewController.noPlacesSelected) selected"
       
        self.selectedPlacesId = selectPlacesViewController.selectedPlacesId
        
        btnPostLocation.setTitle("\(self.selectedPlacesId.count) selected", for: .normal)
        btnPostLocation.setTitleColor(UIColor.black, for: .normal)
        
        if (self.selectedPlacesId.count > 0) {
            self.isPostPlacesSelected = true
        } else {
            self.isPostPlacesSelected = false
        }
        
        //check to enable save button
        enableSaveButton()
    }
}


// MARK: Extension SelectPostCategoryViewControllerDelegate


extension AddPostViewController: SelectPostCategoryViewControllerDelegate {
    
    func postCategoryDidFinishSelecting(selectPostCategoryViewController: SelectPostCategoryViewController) {
        
        //stop activity view
        self.dimView.isHidden = true
        
        btnPostCategory.setTitle(selectPostCategoryViewController.postCategory!, for: .normal)
        btnPostCategory.setTitleColor(UIColor.black, for: .normal)
        
        self.isPostCategorySelected = true
        
        //check to enable save button
        enableSaveButton()
        
    }
    
    func postCategoryDidCancelSelecting(selectPostCategoryViewController: SelectPostCategoryViewController) {
        //update appearance
        self.dimView.isHidden = true
    }
    
    
}
