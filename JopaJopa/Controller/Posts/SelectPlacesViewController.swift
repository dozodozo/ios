    //
//  SelectPlacesViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 20/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit

protocol  SelectPlacesViewControllerDelegate {
    func didFinishSelectingPlaces(selectPlacesViewController: SelectPlacesViewController)
}

class SelectPlacesViewController: UIViewController {
    
    // MARK: Properties
    
    //list of places returned from db
    var places: [JopaJopaPlace] = [JopaJopaPlace]()
    
    //delegate
    var delegate: SelectPlacesViewControllerDelegate?
    
    //places selected
    var selectedPlacesId : Set<String> = Set<String>()
    
    //view to show if places is empty, consists of label and image view
    let nothingView: UIView = {
        let tempView = UIView()
        tempView.translatesAutoresizingMaskIntoConstraints = false
        return tempView
    }()
    
    let label: UILabel = {
        let lbl = UILabel()
        lbl.text = NSLocalizedString("You have no places!", comment: "")
        lbl.textColor = ViewConstants.LIGHT_TEXT_COLOR
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let imageView: UIImageView = {
        let imgView = UIImageView(image: #imageLiteral(resourceName: "places"))
        imgView.translatesAutoresizingMaskIntoConstraints = false
        return imgView
    }()
    
    //table view for places
    let tableView: UITableView = {
        let tblView = UITableView()
        tblView.translatesAutoresizingMaskIntoConstraints = false
        tblView.register(PlacesTableViewCell.self, forCellReuseIdentifier: "cell")
        return tblView
    }()
    
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //title
        self.title = NSLocalizedString("Select Places", comment: "")
        
        //data source and delegate
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        //add button to add place and cancel
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelAddPlace))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(finishedSelecting))
        
        //add sub views
        self.view.addSubview(nothingView)
        self.nothingView.addSubview(label)
        self.nothingView.addSubview(imageView)
        
        self.view.addSubview(tableView)
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension //for dynamic height
        
        //setup
        setupLayout()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //get places
        getPlaces()
        
    }
    
    
    // MARK: UI Layout
    
    func setupLayout(){
        
        
        //imgView
        self.imageView.centerYAnchor.constraint(equalTo: self.nothingView.centerYAnchor, constant: 10).isActive = true
        self.imageView.centerXAnchor.constraint(equalTo: self.nothingView.centerXAnchor).isActive = true
        
        //lblMessage
        self.label.centerXAnchor.constraint(equalTo: self.nothingView.centerXAnchor).isActive = true
        self.label.topAnchor.constraint(equalTo: self.imageView.bottomAnchor, constant: 5).isActive = true
        
        //table view
        self.tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        self.tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        //nothing view
        self.nothingView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        self.nothingView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        self.nothingView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.nothingView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
    }
    
    
    // MARK: Actions

    //get places
    
    func getPlaces() -> Void {
        
        //get places list
        JopaJopaClient.sharedInstance().getPlacesList {places, error in
            
            //check if request was successful
            //check if request was successful
            if let error = error {
                
                //alert controller
                let alertController = JopaJopaClient.sharedInstance().getAlertControllerForErrors(error, JopaJopaClient.JopaJopaTask.Get_Places)
                
                // if error is authentication failure, log user out
                if (error.userInfo["JopaJopaErrorType"] as! String == JopaJopaClient.JopaJopaErrorType.AUTHENTICATION_FAILURE){
                    JopaJopaClient.sharedInstance().logoutUser()
                    let logoutAction = UIAlertAction(title: "Close", style: .default){ handler in
                        self.present(MainTabViewController(), animated: true, completion: nil)
                    }
                    alertController.addAction(logoutAction)
                }
                performUIUpdatesOnMain {
                    self.present(alertController, animated: true, completion: nil)
                }
                
            } else {
                //save places
                self.places = places!
                
                if (places!.isEmpty){
                    performUIUpdatesOnMain {
                        self.navigationItem.rightBarButtonItem?.isEnabled = false
                        self.tableView.reloadData()
                        self.tableView.isHidden = true
                        self.nothingView.isHidden = false
                    }
                } else {
                    performUIUpdatesOnMain {
                        self.tableView.reloadData()
                        self.tableView.isHidden = false
                        self.nothingView.isHidden = true
                    }
                }
            }
            
        }
    }
    
    //finished selecting places
    @objc func finishedSelecting(_ sender: UIBarButtonItem){
        delegate?.didFinishSelectingPlaces(selectPlacesViewController: self)
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func cancelAddPlace(_ sender: UIBarButtonItem){
        self.dismiss(animated: true, completion: nil)
    }
    
}

// MARK: TableView extension

extension SelectPlacesViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return !(self.places.isEmpty) ? self.places.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PlacesTableViewCell
        let place = self.places[indexPath.row]
        /* Set cell defaults */
        cell.titleLabel.text = place.placeName
        cell.detailLabel.text = place.placeLocation!.address
        
        let isCellSelected = (selectedPlacesId.contains(place.placeID!))
        
        if (isCellSelected){
            cell.imageViewCell.alpha = 0.5
            cell.imageViewCell.layer.borderColor = ViewConstants.BUTTON_COLOR_DEFAULT.cgColor
            cell.imageViewCell.layer.borderWidth = 3
        }
        
        let url = URL(string: place.placeImageURL!)
        JopaJopaClient.sharedInstance().taskForGETImageFromSpaces(url: url!){ imageData, error in
            if (error != nil) {
                performUIUpdatesOnMain {
                    cell.imageViewCell.image = UIImage(named: "defaultImage")
                }
                
            } else
            {
                if let imageData = imageData {
                    performUIUpdatesOnMain {
                        cell.imageViewCell.image = UIImage(data: imageData)
                    }
                }
            }
        }
        
        return cell
    }
    
    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! PlacesTableViewCell
        let place = self.places[indexPath.row]
        let isCellSelected = (selectedPlacesId.contains(place.placeID!))
        
        if (!isCellSelected){
            //insert place to selectedPlacesId
            selectedPlacesId.insert(place.placeID!)
            
            UIView.transition(with: cell.imageViewCell, duration: 0.5, options: .transitionFlipFromLeft, animations: {
                cell.imageViewCell.alpha = 0.5
                cell.imageViewCell.layer.borderColor = ViewConstants.BUTTON_COLOR_DEFAULT.cgColor
                cell.imageViewCell.layer.borderWidth = 3
                self.view.layoutIfNeeded()
            }, completion: nil)

        } else {
            selectedPlacesId.remove(place.placeID!)
            
            UIView.transition(with: cell.imageViewCell, duration: 0.5, options: .transitionFlipFromLeft, animations: {
                cell.imageViewCell.alpha = 1
                cell.imageViewCell.layer.borderWidth = 0
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
            
        //update lable for no of places selected
        self.title = "Select Places (\(selectedPlacesId.count))"
    }
   
}


