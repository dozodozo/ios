//
//  PostDetailViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 2/7/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit
import PromiseKit

protocol  PostDetailViewControllerDelegate {
    func didFinishUpdatingPost(_ postDetailViewController: PostDetailViewController)
    
    func didFinishDeletingPost(_ indexPath: IndexPath)
}

class PostDetailViewController: UIViewController {
    
    // MARK: Properties
    
    //JopaJopaPlace
    var post: JopaJopaPost!
    
    var indexPath: IndexPath?
    
    var delegate: PostDetailViewControllerDelegate?
    
    var selectedPlacesId : Set<String> = Set<String>()
    
    let noCharsPostTitle = 30
    let noCharsPostText = 150
    
    var isPostImageSelected = false
    var isPostPlacesSelected = false
    var isPostCategorySelected = false
    var isPostTitleSet = false
    var isPostTextSet = false
    
    //keep tracks of whether scrollView was adjusted when keyboard shows
    var scrollViewAdjusted = false
    
    // scrollView
    let scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    // container for subviews inside scrollview
    let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // post image
    let imgViewPost: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.gray
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        return imageView
    }()
    
    let imgViewAddButton: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "addImageGray"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let imgViewLocation: UIImageView = {
        let tintableImage = UIImage(named: "locationPin")?.withRenderingMode(.alwaysTemplate)
        let imageView = UIImageView()
        imageView.image = tintableImage
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let btnPostLocation: UIButton = {
        let btn = UIButton()
        btn.setTitle("Add Place", for: .normal)
        btn.setTitleColor(UIColor.black, for: .normal)
        btn.layer.borderColor = ViewConstants.LIGHT_TEXT_COLOR.cgColor
        btn.layer.borderWidth = 1.0
        btn.layer.cornerRadius = 20
        btn.addTarget(self, action: #selector(addPlace), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitleColor(ViewConstants.LIGHT_TEXT_COLOR, for: .normal)
        btn.titleLabel?.font = UIFont.preferredFont(forTextStyle: .footnote)
        return btn
    }()
    
    let btnPostCategory: UIButton = {
        let btn = UIButton()
        btn.setTitle("Add Category", for: .normal)
        btn.setTitleColor(ViewConstants.LIGHT_TEXT_COLOR, for: .normal)
        btn.titleLabel?.font = UIFont.preferredFont(forTextStyle: .footnote)
        btn.layer.borderColor = ViewConstants.LIGHT_TEXT_COLOR.cgColor
        btn.layer.borderWidth = 1.0
        btn.layer.cornerRadius = 20
        btn.addTarget(self, action: #selector(addCategory), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let imgViewPostCategory: UIImageView = {
        let tintableImage = UIImage(named: "category")?.withRenderingMode(.alwaysTemplate)
        let imageView = UIImageView()
        imageView.image = tintableImage
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let txtPostTitle: InputTextField = {
        let txtField = InputTextField()
        txtField.font = UIFont.preferredFont(forTextStyle: .subheadline).bold()
        txtField.placeholder = "Post Title"
        txtField.translatesAutoresizingMaskIntoConstraints = false
        return txtField
    }()
    
    let lblNoCharsRemainingPostTitle: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = ViewConstants.LIGHT_TEXT_COLOR
        lbl.font = UIFont.preferredFont(forTextStyle: .footnote)
        return lbl
    }()
    
    let txtPostText: UITextView = {
        let txtView = UITextView()
        txtView.text = ""
        txtView.textColor = ViewConstants.LIGHT_TEXT_COLOR
        txtView.font = UIFont.preferredFont(forTextStyle: .footnote)
        txtView.translatesAutoresizingMaskIntoConstraints = false
        txtView.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).cgColor
        txtView.layer.borderWidth = 1.0
        txtView.layer.cornerRadius = 1
        return txtView
    }()
    
    let lblNoCharsRemainingPostText: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = ViewConstants.LIGHT_TEXT_COLOR
        lbl.font = UIFont.preferredFont(forTextStyle: .footnote)
        return lbl
    }()
    
    let lblPlaceHolder: UILabel = {
        let lbl = UILabel()
        lbl.text = NSLocalizedString("Post Caption", comment: "")
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = ViewConstants.LIGHT_TEXT_COLOR
        lbl.font = UIFont.preferredFont(forTextStyle: .caption1)
        return lbl
    }()
    
    let btnSave: UIBarButtonItem = {
        let btn = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.plain, target: self, action: #selector(savePost))
        btn.isEnabled = false
        return btn
    }()
    
    let btnDeletePost: UIButton = {
        let btn = UIButton()
        btn.setTitle(NSLocalizedString("Delete", comment:""), for: .normal)
        btn.setTitleColor(UIColor.red, for: .normal)
        btn.backgroundColor = ViewConstants.TAB_BAR_BACKGROUND_COLOR
        btn.addTarget(self, action: #selector(deletePost), for: UIControlEvents.touchUpInside)
        btn.layer.borderWidth = 0.5
        btn.layer.borderColor = UIColor.lightGray.cgColor
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    //activity indicator centred on delete button
    let activityIndicatorDeleteButton : UIActivityIndicatorView = {
        let ai = UIActivityIndicatorView()
        ai.color = ViewConstants.BUTTON_COLOR_DEFAULT
        ai.translatesAutoresizingMaskIntoConstraints = false
        return ai
    }()
    
    let dimView : UIView = {
        let view = UIView()
        view.backgroundColor = ViewConstants.BACKGROUND_COLOR_DIM
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let activityIndicator: UIActivityIndicatorView = {
        let actInd = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        actInd.color = ViewConstants.BUTTON_COLOR_DEFAULT
        actInd.translatesAutoresizingMaskIntoConstraints = false
        return actInd
    }()
    
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //view title
        //self.title = NSLocalizedString("Add Post", comment: "")
        
        //hide tab bar
        self.tabBarController?.tabBar.isHidden = true
        
        //right bar button item
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.plain, target: self, action: #selector(savePost))
         self.navigationItem.rightBarButtonItem?.isEnabled = false
        
        //tap gesture for selecting places
        let tapGestureRecoginizer = UITapGestureRecognizer(target: self, action: #selector(selectImage))
        imgViewPost.addGestureRecognizer(tapGestureRecoginizer)
        imgViewPost.isUserInteractionEnabled = true
        
        // keyboard notfications for adjusting scrollview
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
        
        //assign delegates
        txtPostTitle.delegate = self
        txtPostText.delegate = self
        
        //assing values to labels
        lblNoCharsRemainingPostTitle.text = "\(noCharsPostTitle)"
        lblNoCharsRemainingPostText.text = "\(noCharsPostText)"
        
        self.view.addSubview(scrollView)
        self.scrollView.addSubview(containerView)
        self.containerView.addSubview(imgViewPost)
        self.containerView.addSubview(imgViewAddButton)
        self.containerView.addSubview(imgViewLocation)
        self.containerView.addSubview(btnPostLocation)
        self.containerView.addSubview(btnPostCategory)
        self.containerView.addSubview(imgViewPostCategory)
        self.containerView.addSubview(txtPostTitle)
        self.containerView.addSubview(lblNoCharsRemainingPostTitle)
        self.containerView.addSubview(txtPostText)
        self.containerView.addSubview(lblNoCharsRemainingPostText)
        self.containerView.addSubview(lblPlaceHolder)
        self.view.addSubview(btnDeletePost)
        self.btnDeletePost.addSubview(activityIndicatorDeleteButton)
        
        self.view.addSubview(dimView)
        
        //setup layout
        setupLayout()
        
    }
    

    
    // MARK: Layout
    
    func setupLayout(){
        scrollView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
        /*
         important: container must be anchored to scroll view in Left-Right, Top-Bottom order
         bottom anchor must be anchored to scroll view bottom anchor
         size cannot depend on scroll view
         */
        containerView.leadingAnchor.constraint(equalTo: self.scrollView.leadingAnchor).isActive = true
        containerView.topAnchor.constraint(equalTo: self.scrollView.topAnchor).isActive = true
        containerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        containerView.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor).isActive = true
        //containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        
        
        imgViewPost.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        imgViewPost.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        imgViewPost.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        imgViewPost.heightAnchor.constraint(equalTo: imgViewPost.widthAnchor).isActive = true
        
        imgViewAddButton.centerXAnchor.constraint(equalTo: imgViewPost.centerXAnchor).isActive = true
        imgViewAddButton.centerYAnchor.constraint(equalTo:imgViewPost.centerYAnchor).isActive = true
        imgViewAddButton.widthAnchor.constraint(equalToConstant: view.frame.width/4).isActive = true
        imgViewAddButton.heightAnchor.constraint(equalTo: imgViewAddButton.widthAnchor).isActive = true
        
        btnPostLocation.topAnchor.constraint(equalTo: imgViewPost.bottomAnchor, constant:20).isActive = true
        btnPostLocation.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant:10).isActive = true
        btnPostLocation.trailingAnchor.constraint(equalTo: containerView.centerXAnchor, constant: -2.5).isActive = true
        btnPostLocation.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        imgViewLocation.topAnchor.constraint(equalTo: btnPostLocation.topAnchor, constant:12.5 ).isActive = true
        imgViewLocation.leadingAnchor.constraint(equalTo: btnPostLocation.leadingAnchor, constant:10).isActive = true
        imgViewLocation.widthAnchor.constraint(equalToConstant: 15).isActive = true
        imgViewLocation.heightAnchor.constraint(equalTo: imgViewLocation.widthAnchor).isActive = true
        
        
        btnPostCategory.topAnchor.constraint(equalTo: imgViewPost.bottomAnchor, constant:20).isActive = true
        btnPostCategory.leadingAnchor.constraint(equalTo: containerView.centerXAnchor, constant: 2.5).isActive = true
        btnPostCategory.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10).isActive = true
        btnPostCategory.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        
        imgViewPostCategory.topAnchor.constraint(equalTo: btnPostCategory.topAnchor, constant:12.5 ).isActive = true
        imgViewPostCategory.leadingAnchor.constraint(equalTo: btnPostCategory.leadingAnchor, constant:10).isActive = true
        imgViewPostCategory.widthAnchor.constraint(equalToConstant: 15).isActive = true
        imgViewPostCategory.heightAnchor.constraint(equalTo: imgViewPostCategory.widthAnchor).isActive = true
        
        txtPostTitle.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant:5).isActive = true
        txtPostTitle.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant:-25).isActive = true
        txtPostTitle.topAnchor.constraint(equalTo: btnPostCategory.bottomAnchor, constant:20).isActive = true
        
        lblNoCharsRemainingPostTitle.leadingAnchor.constraint(equalTo: txtPostTitle.trailingAnchor, constant: 0).isActive = true
        lblNoCharsRemainingPostTitle.centerYAnchor.constraint(equalTo: txtPostTitle.centerYAnchor, constant: 0).isActive = true
        
        txtPostText.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant:10).isActive = true
        txtPostText
            .topAnchor.constraint(equalTo: txtPostTitle.bottomAnchor, constant:-5).isActive = true
        txtPostText.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant:-10).isActive = true
        txtPostText.heightAnchor.constraint(equalToConstant: 80).isActive = true
        txtPostText.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        
        lblNoCharsRemainingPostText.leadingAnchor.constraint(equalTo: txtPostText.safeAreaLayoutGuide.trailingAnchor, constant: -25).isActive = true
        lblNoCharsRemainingPostText.bottomAnchor.constraint(equalTo: txtPostText.bottomAnchor, constant: 0).isActive = true
        
        lblPlaceHolder.topAnchor.constraint(equalTo: txtPostText.topAnchor, constant:8).isActive = true
        lblPlaceHolder.leadingAnchor.constraint(equalTo: txtPostText.leadingAnchor, constant: 8).isActive = true
        
        
        btnDeletePost.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -49).isActive = true
        btnDeletePost.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        btnDeletePost.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        btnDeletePost.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        
        activityIndicatorDeleteButton.centerYAnchor.constraint(equalTo: btnDeletePost.centerYAnchor).isActive = true
        activityIndicatorDeleteButton.centerXAnchor.constraint(equalTo: btnDeletePost.centerXAnchor).isActive = true
        
        dimView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        dimView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        dimView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
    }
    
    
    // MARK: Helpers
    
    //hide keyboard
    @objc private func hideKeyboard(_ sender: Any){
        self.view.endEditing(true)
    }
    
    // keyboard show/hide adjust scrollview
    @objc func keyboardWillShow(notification: NSNotification) {
        
        //adjust contentInset of scroll view
        let keyboardInfo = (notification.userInfo![UIKeyboardFrameEndUserInfoKey]) as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        
        //if contentInset has NOT already been set to keyboard height, set it to keyboard height
        if (!scrollViewAdjusted) {
            scrollViewAdjusted = true
            let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height + 50, right: 0)
            self.scrollView.contentInset = contentInset
            
            //move scrollview up
            self.scrollView.bounds.origin.y += keyboardSize.height + txtPostText.frame.size.height
        }
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        scrollViewAdjusted = false
        
        //set contentInset to original position
        self.scrollView.contentInset.bottom = 50
        
        //move scrollview back to original position
        self.scrollView.bounds.origin.y = 0
    }
    
    // enable save button
    func enableSaveButton(){
        if (isPostImageSelected && isPostCategorySelected && isPostPlacesSelected &&  isPostTextSet && isPostTitleSet){
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        } else {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
        
    }
    
    //update appearnce post under operation
    func updateAppearanceUpdatingPost(_ sender: Any){
        //hide keboard
        hideKeyboard(sender)
        
        //show activity view
        self.dimView.isHidden = false
        self.activityIndicator.startAnimating()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activityIndicator)
    }
    
    //update appearance post operation done
    func updateAppearanceUpdatingPostDone(_ sender: Any){
        self.activityIndicator.stopAnimating()
        self.dimView.isHidden = true
        self.navigationItem.rightBarButtonItem =  UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.plain, target: self, action: #selector(savePost))
    }
    
    func updateAppearanceDeletingPost(_ sender: Any){
        //hide keboard
        hideKeyboard(sender)
        
        //show activity view
        self.dimView.isHidden = false
        self.activityIndicatorDeleteButton.startAnimating()
    }
    
    //update appearance post operation done
    func updateAppearanceDeletingPostDone(_ sender: Any){
        self.dimView.isHidden = true
        self.activityIndicatorDeleteButton.startAnimating()
    }
    
    // MARK : Actions
    
    // launch image picker
    @objc func selectImage(_sender: UIImageView){
        let imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        let alertControllerTitle = NSLocalizedString("Select Source", comment: "")
        let cameraTitle = NSLocalizedString("Camera", comment: "")
        let libraryTitle = NSLocalizedString("Photo Library", comment: "")
        
        //show options for camera or library
        let alertController = UIAlertController(title: alertControllerTitle, message: "", preferredStyle: .actionSheet)
        
        //camera
        let camera = UIAlertAction(title: cameraTitle, style: .default){handler in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                imagePicker.sourceType = .camera
                imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
                self.present(imagePicker, animated: true, completion: nil)
            } else {
                let error = NSError(domain: "TaskForSelectImage", code: 1, userInfo : ["JopaJopaErrorType" : JopaJopaClient.JopaJopaErrorType.MEDIA_SOURCE_NOT_AVAILABLE])
                self.present(JopaJopaClient.sharedInstance().getAlertControllerForErrors(error, ""), animated: true, completion: nil)
            }
        }
        
        //photo library
        let library = UIAlertAction(title: libraryTitle, style: .default){ handler in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                imagePicker.sourceType = .photoLibrary
                imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
                self.present(imagePicker, animated: true, completion: nil)
            } else {
                let error = NSError(domain: "TaskForSelect", code: 1, userInfo : ["JopaJopaErrorType" : JopaJopaClient.JopaJopaErrorType.MEDIA_SOURCE_NOT_AVAILABLE])
                self.present(JopaJopaClient.sharedInstance().getAlertControllerForErrors(error, ""), animated: true, completion: nil)
            }
        }
        
        //cancel
        let cancel = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        
        //add actions
        alertController.addAction(camera)
        alertController.addAction(library)
        alertController.addAction(cancel)
        
        //show imagepicker option
        self.present(alertController, animated: true, completion: nil)
    }
    
    // select place
    @objc func addPlace(_ sender: UIButton){
        let selectPlacesViewController = SelectPlacesViewController()
        selectPlacesViewController.delegate = self
        selectPlacesViewController.selectedPlacesId = self.selectedPlacesId
        let nc = UINavigationController(rootViewController: selectPlacesViewController)
        self.navigationController?.present(nc, animated: true, completion: nil)
    }
    
    // select category
    @objc func addCategory(_ sender: UIButton){
        //show activity view
        self.dimView.isHidden = false
        
        let nc = SelectPostCategoryViewController()
        nc.delegate = self
        nc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.navigationController?.present(nc, animated: true, completion: nil)
    }
    
    // save post
    @objc func savePost(_ sender: UIBarButtonItem){
        
        //update appearance
        updateAppearanceUpdatingPost(sender)
        
        //convert selectedPlacesId to an array
        let placeIDs = Array(selectedPlacesId)
        
        post = JopaJopaPost(postID:post.postID, userID: JopaJopaClient.sharedInstance().getUserID(), postTitle: txtPostTitle.text, postText: txtPostText.text, postCategory: btnPostCategory.titleLabel?.text, placeIDs: placeIDs)
        
        
        // encode json
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let data = try! encoder.encode(post)
        
        //json to string
        let postString = String(data: data, encoding: .utf8)!

       
        //call conveniece method to update
        firstly {
            
            JopaJopaClient.sharedInstance().updatePost(jsonBody: postString, postID: (post.postID)!)
            }
            // upload image to digital ocean spaces
            .then {(postID, url) -> Promise<Bool> in
                //image data
                let img = self.imgViewPost.image?.resized(toWidth: 400)
                let image_data = UIImageJPEGRepresentation(img!, 0.5)!
                return JopaJopaClient.sharedInstance().uploadImageToSpaces(signedURL: url, data: image_data)
            }
            .done { result in
                
                //update apperance
                performUIUpdatesOnMain {
                    self.updateAppearanceUpdatingPostDone(sender)
                    self.navigationController?.popViewController(animated: true)
                    //call delegate
                    self.delegate?.didFinishUpdatingPost(self)
                }
                
            }
            .catch{ error in
                
                let nsError = error as NSError
                
                //alert controller
                let alertController = JopaJopaClient.sharedInstance().getAlertControllerForErrors(nsError, JopaJopaClient.JopaJopaTask.Update_Post)
                // if error is authentication failure, log user out
                if (nsError.userInfo["JopaJopaErrorType"] as! String == JopaJopaClient.JopaJopaErrorType.AUTHENTICATION_FAILURE){
                    JopaJopaClient.sharedInstance().logoutUser()
                    let logoutAction = UIAlertAction(title: "Close", style: .default){ handler in
                        self.present(MainTabViewController(), animated: true, completion: nil)
                    }
                    alertController.addAction(logoutAction)
                }
                performUIUpdatesOnMain {
                    self.present(alertController, animated: true, completion: nil)
                    self.updateAppearanceUpdatingPostDone(sender)
                }
                
                
        }
        
    }
    
    
    // Delete post
    
    @objc func deletePost(_ sender: UIButton) {
        
        //update appeara
        
        //confirm deletion
        let alertView: UIAlertController = {
            let myAlertView = UIAlertController(title: NSLocalizedString("Delete", comment:""), message: NSLocalizedString("Are you sure?", comment:""), preferredStyle: UIAlertControllerStyle.alert)
            let alertAction1 = UIAlertAction(title: NSLocalizedString("Cancel", comment:""), style: UIAlertActionStyle.default, handler:nil)
            let alertAction2 = UIAlertAction(title: NSLocalizedString("Yes", comment:""), style: UIAlertActionStyle.default){handler in
                self.proceedDeletePost(sender)
            }
            myAlertView.addAction(alertAction1)
            myAlertView.addAction(alertAction2)
            return myAlertView
        }()
        self.present(alertView, animated: true, completion: nil)
    }
    
    private func proceedDeletePost(_ sender: Any){

        //update appearance
        updateAppearanceDeletingPost(sender)
        
        //place deleted mesage
        JopaJopaClient.sharedInstance().deletePost(postID: post.postID!){error in
            
            if let error = error {
                
                //alert controller
                let alertController = JopaJopaClient.sharedInstance().getAlertControllerForErrors(error, JopaJopaClient.JopaJopaTask.Delete_Post)
                
                // if error is authentication failure, log user out
                if (error.userInfo["JopaJopaErrorType"] as! String == JopaJopaClient.JopaJopaErrorType.AUTHENTICATION_FAILURE){
                    JopaJopaClient.sharedInstance().logoutUser()
                    let logoutAction = UIAlertAction(title: "Close", style: .default){ handler in
                        self.present(MainTabViewController(), animated: true, completion: nil)
                    }
                    alertController.addAction(logoutAction)
                }
                performUIUpdatesOnMain {
                    self.present(alertController, animated: true, completion: nil)
                }
                
            } else {
                performUIUpdatesOnMain {
                    self.navigationController?.popViewController(animated: true)
                    self.delegate?.didFinishDeletingPost(self.indexPath!)
                }
            }
        }
        
    }
    
}


// MARK: Text Field Delegates

extension PostDetailViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // get email and password from text
        var updatedPostTitle = txtPostTitle.text!
        
        if let postTitle = txtPostTitle.text,
            let textRange = Range(range, in: postTitle) {
            updatedPostTitle = postTitle.replacingCharacters(in: textRange,
                                                             with: string)
            //check to enable save button
            if (updatedPostTitle != ""){
                isPostTitleSet  = true
            } else {
                isPostTitleSet  = false
            }
            enableSaveButton()
            
            //update character count label
            let n = noCharsPostTitle - updatedPostTitle.count
            lblNoCharsRemainingPostTitle.text = n < 0 ? "\(0)" : "\(n)"
            if (updatedPostTitle.count <= noCharsPostTitle) {
                return true
            }
        }
        
        return false
    }
    
    
}



// MARK: Extension UITextViewDelegate


extension PostDetailViewController: UITextViewDelegate {
    
    //hide textview placeholder
    func textViewDidChange(_ textView: UITextView) {
        if(textView.text != ""){
            lblPlaceHolder.isHidden = true
            textView.textColor = UIColor.black
        } else {
            lblPlaceHolder.isHidden = false
            textView.textColor = ViewConstants.LIGHT_TEXT_COLOR
        }
    }
    
    //return
    
    //
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        //ignore return by resigning first responder
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        
        // get email and password from text
        var updatedPostText = txtPostText.text!
        
        if let postText = txtPostText.text,
            let textRange = Range(range, in: postText) {
            updatedPostText = postText.replacingCharacters(in: textRange,
                                                           with: text)
            
            //check to enable save button
            if (updatedPostText != ""){
                isPostTextSet  = true
            } else {
                isPostTextSet  = false
            }
            enableSaveButton()
            
            //update character count label
            let n = noCharsPostText - updatedPostText.count
            lblNoCharsRemainingPostText.text = n < 0 ? "\(0)" : "\(n)"
            if (updatedPostText.count <= noCharsPostText) {
                return true
            }
        }
        
        return false
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
    }
    
}

// MARK: ImagePickerControllerDelegate

extension PostDetailViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //image picked
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        //get chosen image
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        //dismiss picker
        dismiss(animated: true, completion: nil)
        
        //instantiate ImageCropperViewController
        let imageCropperViewController = ImageCropperViewController(image: chosenImage)
        
        //make this view controller the delegate
        imageCropperViewController.delegate = self
        
        //presnet ImageCropperViewController
        self.present(imageCropperViewController, animated: true, completion: nil)
        
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}

// MARK: Extension ImageCropperViewControllerDelegate

extension PostDetailViewController: ImageCropperViewControllerDelegate {
    
    func imageDidFinishCropping(imageCropperViewController: ImageCropperViewController) {
        self.imgViewPost.image = imageCropperViewController.croppedImage
        self.imgViewAddButton.isHidden = true
        
        self.isPostImageSelected = true
        
        //check to enable save button
        enableSaveButton()
    }
}

// MARK: Extension SelectPlacesViewControllerDelegate

extension PostDetailViewController: SelectPlacesViewControllerDelegate {
    
    func didFinishSelectingPlaces(selectPlacesViewController: SelectPlacesViewController) {
        //self.txtPostLocation.text = "\(selectPlacesViewController.noPlacesSelected) selected"
        
        self.selectedPlacesId = selectPlacesViewController.selectedPlacesId
        
        btnPostLocation.setTitle("\(self.selectedPlacesId.count) selected", for: .normal)
        btnPostLocation.setTitleColor(UIColor.black, for: .normal)
        
        if (self.selectedPlacesId.count > 0) {
            self.isPostPlacesSelected = true
        } else {
            self.isPostPlacesSelected = false
        }
        
        //check to enable save button
        enableSaveButton()
    }
}


// MARK: Extension SelectPostCategoryViewControllerDelegate


extension PostDetailViewController: SelectPostCategoryViewControllerDelegate {
    
    func postCategoryDidFinishSelecting(selectPostCategoryViewController: SelectPostCategoryViewController) {
        btnPostCategory.setTitle(selectPostCategoryViewController.postCategory!, for: .normal)
        btnPostCategory.setTitleColor(UIColor.black, for: .normal)
        
        //stop activity view
        self.dimView.isHidden = true
        
        self.isPostCategorySelected = true
        
        //check to enable save button
        enableSaveButton()
        
    }
    
    func postCategoryDidCancelSelecting(selectPostCategoryViewController: SelectPostCategoryViewController) {
        
        //stop activity view
        self.dimView.isHidden = true
    }
    
    
}
