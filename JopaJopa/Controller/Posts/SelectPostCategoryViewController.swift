//
//  SelectPostCategoryViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 31/7/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit

protocol SelectPostCategoryViewControllerDelegate {
    func postCategoryDidFinishSelecting(selectPostCategoryViewController: SelectPostCategoryViewController)
    
    func postCategoryDidCancelSelecting(selectPostCategoryViewController: SelectPostCategoryViewController)
}

class SelectPostCategoryViewController: UIViewController {
    
    // MARK: Properties
    
    var postCategory : String?
    var delegate: SelectPostCategoryViewControllerDelegate?
    
    let postTypes = [JopaJopaClient.PostCategories.eatDrink, JopaJopaClient.PostCategories.shop, JopaJopaClient.PostCategories.whatsOn, JopaJopaClient.PostCategories.healthBeauty, JopaJopaClient.PostCategories.community, NSLocalizedString("Cancel", comment: "")]
    
    //table view for post categories
    let tableView: UITableView = {
        let tblView = UITableView()
        tblView.translatesAutoresizingMaskIntoConstraints = false
        tblView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tblView.layer.borderWidth = 1
        tblView.layer.borderColor = UIColor.lightGray.cgColor
        tblView.layer.cornerRadius = 10
        tblView.isScrollEnabled = false
        
        return tblView
    }()

    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //title
        self.title = NSLocalizedString("Select Posts", comment: "")
        
        //assign delegates
        tableView.delegate = self
        tableView.dataSource = self
        
        //add sub views
        self.view.addSubview(tableView)
        
        //adjust table size to fit content
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        //setup
        setupLayout()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = CGRect(x: tableView.frame.origin.x, y: tableView.frame.origin.y, width: tableView.frame.size.width, height: tableView.contentSize.height)
        tableView.reloadData()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Setup Views
    
    private func setupLayout(){
        //table view
        let yOffset = self.view.frame.size.height / 4
        self.tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: yOffset).isActive = true
        self.tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant:20).isActive = true
        self.tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant:-20).isActive = true
    }

}

// MARK: TableView extension

extension SelectPostCategoryViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(postTypes.count)
        return self.postTypes.count
        //return places!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.layer.masksToBounds = true
        cell.clipsToBounds = true
        cell.textLabel?.text = postTypes[indexPath.row]
        cell.textLabel?.textAlignment = NSTextAlignment.center
        if (indexPath.row == 5){
            cell.textLabel?.textColor = ViewConstants.BUTTON_COLOR_DEFAULT
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.row < 5){
            let cell = tableView.cellForRow(at: indexPath)
            self.postCategory = cell?.textLabel?.text
            self.delegate?.postCategoryDidFinishSelecting(selectPostCategoryViewController: self)
        } else {
            self.delegate?.postCategoryDidCancelSelecting(selectPostCategoryViewController: self)
        }
        self.dismiss(animated: true, completion: nil)

    }
}
