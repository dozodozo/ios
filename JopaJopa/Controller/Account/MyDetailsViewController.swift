//
//  MyDetailsViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 28/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit

// MARK : MyDetailViewController

class MyDetailsViewController: UIViewController {
    
    // MARK: Properties
    
    //booleans for enabling save btn
    var isFirstNameSet = false
    var isLastNameSet = false
        
    let lblFirstName: UILabel = {
        let lbl = UILabel()
        lbl.text = NSLocalizedString("First Name", comment: "")
        lbl.textColor = ViewConstants.LIGHT_TEXT_COLOR
        lbl.font = lbl.font.withSize(ViewConstants.SMALL_FONT)
        //lbl.isHidden = true
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()

    let lblLastName: UILabel = {
        let lbl = UILabel()
        lbl.text = NSLocalizedString("Last Name", comment: "")
        lbl.textColor = ViewConstants.LIGHT_TEXT_COLOR
        lbl.font = lbl.font.withSize(ViewConstants.SMALL_FONT)
        //lbl.isHidden = true
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let txtFirstName: InputTextField = {
        let txtField = InputTextField()
        txtField.placeholder = NSLocalizedString("First Name", comment:"")
        //txtField.isHidden = true
        return txtField
    }()
    
    let txtLastName: InputTextField = {
        let txtField = InputTextField()
        txtField.placeholder = NSLocalizedString("Last Name", comment:"")
        //txtField.isHidden = true
        return txtField
    }()
    
    let activityIndicator : UIActivityIndicatorView = {
        let ai = UIActivityIndicatorView()
        ai.activityIndicatorViewStyle = .gray
        ai.color = ViewConstants.BUTTON_COLOR_DEFAULT
        ai.translatesAutoresizingMaskIntoConstraints = false
        return ai
    }()
    
    
    let dimView : UIView = {
        let view = UIView()
        view.backgroundColor = ViewConstants.BACKGROUND_COLOR_DIM
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    //activity indicator to show when loading page
    let activityIndicatorPageLoading: UIActivityIndicatorView = {
        let actInd = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        actInd.translatesAutoresizingMaskIntoConstraints = false
        return actInd
    }()
    
    
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //title
        self.title = "Details"
        
        //hide tab bar
        self.tabBarController?.tabBar.isHidden = true
        
        
        //delegates
        txtFirstName.delegate = self
        txtLastName.delegate = self
        
        //right bar button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(updateUserDetails))
        
        //initialize save btn
        enableSaveBtn()
        
        //add views
        //self.view.addSubview(activityIndicatorPageLoading)
        self.view.addSubview(self.lblFirstName)
        self.view.addSubview(self.txtFirstName)
        self.view.addSubview(self.lblLastName)
        self.view.addSubview(self.txtLastName)
        self.view.addSubview(dimView)
        self.dimView.addSubview(self.activityIndicatorPageLoading)
        
        //setup layout
        self.setupLayout()
        
    }
    
    override func viewDidLayoutSubviews() {
        
        //add bottom borders, done after adding views
        txtFirstName.addBottomBorder()
        txtLastName.addBottomBorder()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //get user details
        getUserDetails()
        
    }
    
    
    // MARK: UI Layout
    
    func setupLayout(){
        
        lblFirstName.topAnchor.constraint(equalTo: view.topAnchor, constant:100).isActive = true
        lblFirstName.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        lblFirstName.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        txtFirstName.topAnchor.constraint(equalTo: lblFirstName.bottomAnchor, constant:10).isActive = true
        txtFirstName.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        txtFirstName.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        lblLastName.topAnchor.constraint(equalTo: txtFirstName.bottomAnchor, constant:20).isActive = true
        lblLastName.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        lblLastName.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        txtLastName.topAnchor.constraint(equalTo: lblLastName.bottomAnchor, constant:10).isActive = true
        txtLastName.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        txtLastName.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        //dim view
        dimView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        dimView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        dimView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        activityIndicatorPageLoading.centerXAnchor.constraint(equalTo: self.dimView.centerXAnchor).isActive = true
        activityIndicatorPageLoading.centerYAnchor.constraint(equalTo: self.dimView.centerYAnchor).isActive = true
    }
    
    // MARK: Helpers
    
    //helper enable save btn
    
    func enableSaveBtn(){
        if (isFirstNameSet && isLastNameSet){
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        } else {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
    
    //updateAppearance getting user details
    
    func updateAppearanceGettingUserDetails(){
        //show dimView
        dimView.isHidden = false
        activityIndicatorPageLoading.startAnimating()
    }
    
    //updateAppearance getting user details complete
    func updateAppearanceGettingUserDetailsComplete(){
        //show dimView
        self.dimView.isHidden = true
        self.activityIndicatorPageLoading.stopAnimating()
    }
    
    //updateAppearance updating
    
    func updateAppearanceUpdating(_ sender : Any){
        //hide keyboard
        hideKeyboard(sender)
        //show dimView
        dimView.isHidden = false
        //show activity indicator
        self.activityIndicator.startAnimating()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activityIndicator)
    }
    
    //updateAppearance updatingComplete
    func updateAppearanceUpdatingComplete(_ sender : Any){
        //show dimView
        self.dimView.isHidden = true
        //reset navigation item
        self.activityIndicator.stopAnimating()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(self.updateUserDetails))
    }
    
    
    // MARK: Actions
    
    @objc private func hideKeyboard(_ sender: Any){
        self.view.endEditing(true)
    }
    
    //get user details
    func getUserDetails(){
        
        //update appearance
        self.updateAppearanceGettingUserDetails()
        
        //get user details
        JopaJopaClient.sharedInstance().getUserDetails{user, error in
            
            //update appearance
            performUIUpdatesOnMain {
                self.updateAppearanceGettingUserDetailsComplete()
            }
            
            //check error
            if let error = error {
                //alert controller
                let alertController = JopaJopaClient.sharedInstance().getAlertControllerForErrors(error, JopaJopaClient.JopaJopaTask.Get_User_Details)
                
                // if error is authentication failure, log user out
                if (error.userInfo["JopaJopaErrorType"] as! String == JopaJopaClient.JopaJopaErrorType.AUTHENTICATION_FAILURE){
                    JopaJopaClient.sharedInstance().logoutUser()
                    let logoutAction = UIAlertAction(title: "Close", style: .default){ handler in
                        self.present(MainTabViewController(), animated: true, completion: nil)
                    }
                    alertController.addAction(logoutAction)
                }
                performUIUpdatesOnMain {
                    self.present(alertController, animated: true, completion: nil)
                }
                
            } else {
                performUIUpdatesOnMain {
                    //show fields
                    self.lblFirstName.isHidden = false
                    self.txtFirstName.isHidden = false
                    self.lblLastName.isHidden = false
                    self.txtLastName.isHidden = false
                    
                    self.txtFirstName.text = user?.firstName!
                    self.txtLastName.text = user?.lastName!
                }
            }
            
        }
    }
    
    
    //update user details
    @objc func updateUserDetails(_ sender: UIBarButtonItem) -> Void {
        
        //update appearance
        updateAppearanceUpdating(sender)
        
        //create user
        let user = JopaJopaUser(firstName: txtFirstName.text!, lastName: txtLastName.text!)
        
        // encode json
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let data = try! encoder.encode(user)
        
        //json to string
        let userString = String(data: data, encoding: .utf8)!
        
        //
        JopaJopaClient.sharedInstance().updateUserDetails(jsonBody: userString){ result, error in
            
            //update appearnce
            performUIUpdatesOnMain {
                self.updateAppearanceUpdatingComplete(sender)
            }
            
            //show success message
            func displaySuccessMessage(){
                
                //create alert  view
                let alertView: UIAlertController = {
                    let title = NSLocalizedString("Details Updated", comment: "")
                    let message =  NSLocalizedString("Your details were successfully updated", comment: "")
                    let myAlertView = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
                    //action
                    let action = UIAlertAction(title: NSLocalizedString("Close", comment:""), style: UIAlertActionStyle.default){handler in
                        self.navigationController?.popViewController(animated: true)
                    }
                    myAlertView.addAction(action)
                    return myAlertView
                }()
                
                performUIUpdatesOnMain {
                    self.present(alertView, animated: true, completion: nil)
                }
                
            }
            
            //check if request was successful
            if let error = error {
                let alertController = JopaJopaClient.sharedInstance().getAlertControllerForErrors(error, JopaJopaClient.JopaJopaTask.Update_Details)
                performUIUpdatesOnMain {
                    self.present(alertController, animated: true){}
                }
                
            } else {
                displaySuccessMessage()
            }
        }
        
    }
    
}


// MARK: SignUpViewController Extension (TextField Delegate)

extension MyDetailsViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // get email and password from text
        var updatedFirstName = txtFirstName.text!
        var updatedLastName = txtLastName.text!
        
        if let firstName = txtFirstName.text,
            let textRange = Range(range, in: firstName) {
            if (textField == txtFirstName){
                updatedFirstName = firstName.replacingCharacters(in: textRange,
                                                                 with: string)
            }
        }
        
        if let lastName = txtLastName.text,
            let textRange = Range(range, in: lastName) {
            if (textField == txtLastName){
                updatedLastName = lastName.replacingCharacters(in: textRange,
                                                               with: string)
            }
        }
        
        //enable update button
        if (!updatedFirstName.isEmpty){
            self.isFirstNameSet = true
        } else {
            self.isFirstNameSet = false
        }
        
        
        if (!updatedLastName.isEmpty){
            self.isLastNameSet = true
        } else {
            self.isLastNameSet = false
        }
        
        enableSaveBtn()
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
