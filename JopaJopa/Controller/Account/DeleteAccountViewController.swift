//
//  DeleteAccountViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 2/7/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit

class DeleteAccountViewController: UIViewController {
    
    // MARK: Properties
    
    let lblWarning: UILabel = {
        let lbl = UILabel()
        let warningText: String = "By deleting your account, all your account information and posts will be permanently removed."
        lbl.text = NSLocalizedString(warningText, comment: "")
        lbl.textAlignment = .justified
        lbl.lineBreakMode = NSLineBreakMode.byWordWrapping
        lbl.numberOfLines = 0
        lbl.textColor = UIColor.red
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let txtPassword: InputTextField = {
        let txtField = InputTextField()
        txtField.isSecureTextEntry = true
        txtField.placeholder = NSLocalizedString("Enter Password To Delete", comment:"")
        return txtField
    }()
    
    let btnDelete: NormalButton = {
        let btn = NormalButton()
        btn.setTitle(NSLocalizedString("Delete", comment:""), for: UIControlState.normal)
        //btn.addTarget(self, action: #selector(updateEmail), for: .touchUpInside)
        btn.alpha = 0.5
        btn.isEnabled = false
        return btn
    }()
    
    // MARK: Activity Indicator
    let activityIndicator: UIActivityIndicatorView = {
        let actInd = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        actInd.translatesAutoresizingMaskIntoConstraints = false
        return actInd
    }()
    
    
    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //title
        self.title = NSLocalizedString("Delete Account", comment: "")
        
        //hide tab bar
        self.tabBarController?.tabBar.isHidden = true

        //delegates
        txtPassword.delegate = self
        
        //add textFields
        self.view.addSubview(lblWarning)
        self.view.addSubview(txtPassword)
        self.view.addSubview(btnDelete)
        self.view.addSubview(activityIndicator)
        
        //setup layout
        setupLayout()
        
    }
    
    override func viewDidLayoutSubviews() {
        
        //add bottom borders, done after adding views
        txtPassword.addBottomBorder()
    }

    
    // MARK: UI Layout
    
    func setupLayout(){
        
        lblWarning.topAnchor.constraint(equalTo: view.topAnchor, constant:100).isActive = true
        lblWarning.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        lblWarning.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        txtPassword.topAnchor.constraint(equalTo: lblWarning.bottomAnchor, constant:20).isActive = true
        txtPassword.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        txtPassword.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        btnDelete.topAnchor.constraint(equalTo: txtPassword.bottomAnchor, constant:40).isActive = true
        btnDelete.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        btnDelete.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        activityIndicator.topAnchor.constraint(equalTo: btnDelete.centerYAnchor).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: btnDelete.centerXAnchor).isActive = true
    }
    

}



// TODO : DELETE FUNCTION




// MARK: DeleteAccountViewController Extension (TextField Delegate)


extension DeleteAccountViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // get email and password from text
        var updatedPassword = txtPassword.text!
        
        if let password = txtPassword.text,
            let textRange = Range(range, in:password) {
            if (textField == txtPassword){
                updatedPassword = password.replacingCharacters(in: textRange,
                                                                  with: string)
            }
        }
        
        //enable update button only if email is valid and password not empty
        //small bug: if email being edited and password edited again with backspace
        
        if (!updatedPassword.isEmpty){
            btnDelete.isEnabled = true
            btnDelete.alpha = 1.0
        } else {
            btnDelete.isEnabled = false
            btnDelete.alpha = 0.5
        }
        
        return true
    }
    
}
