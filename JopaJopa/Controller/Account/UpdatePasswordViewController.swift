//
//  UpdatePasswordViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 28/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit

// MARK : UpdatePasswordViewController

class UpdatePasswordViewController: UIViewController {
    
    // MARK: Properties
    
    //booleans for enabling save btn
    var isOldPasswordSet = false
    var isNewPasswordSet = false
    var isConfirmNewPasswordSet = false
    
    let txtOldPassword: InputTextField = {
        let txtField = InputTextField()
        txtField.isSecureTextEntry = true
        txtField.placeholder = NSLocalizedString("Enter Current Password", comment:"")
        return txtField
    }()
    
    let txtNewPassword: InputTextField = {
        let txtField = InputTextField()
        txtField.isSecureTextEntry = true
        txtField.placeholder = NSLocalizedString("Enter New Password", comment:"")
        return txtField
    }()
    
    let txtConfirmNewPassword: InputTextField = {
        let txtField = InputTextField()
        txtField.isSecureTextEntry = true
        txtField.placeholder = NSLocalizedString("Confirm New Password", comment:"")
        return txtField
    }()
    
    let activityIndicator : UIActivityIndicatorView = {
        let ai = UIActivityIndicatorView()
        ai.activityIndicatorViewStyle = .gray
        ai.color = ViewConstants.BUTTON_COLOR_DEFAULT
        ai.translatesAutoresizingMaskIntoConstraints = false
        return ai
    }()
    
    
    let dimView : UIView = {
        let view = UIView()
        view.backgroundColor = ViewConstants.BACKGROUND_COLOR_DIM
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //title
        self.title = "Password"
        
        //hide tab bar
        self.tabBarController?.tabBar.isHidden = true
        
        //right bar button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(updatePassword))
        
        //initialize save btn
        enableSaveBtn()
        
        //delegates
        txtOldPassword.delegate = self
        txtNewPassword.delegate = self
        txtConfirmNewPassword.delegate = self
        
        //add textFields
        self.view.addSubview(txtOldPassword)
        self.view.addSubview(txtNewPassword)
        self.view.addSubview(txtConfirmNewPassword)
        
        //dim view
        self.view.addSubview(dimView)
        
        //setup layout
        setupLayout()
        
    }
    
    override func viewDidLayoutSubviews() {
        
        //add bottom borders, done after adding views
        txtOldPassword.addBottomBorder()
        txtNewPassword.addBottomBorder()
        txtConfirmNewPassword.addBottomBorder()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("MyDetailsView: Did Appear")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UI Layout
    
    func setupLayout(){
        
        txtOldPassword.topAnchor.constraint(equalTo: view.topAnchor, constant:100).isActive = true
        txtOldPassword.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        txtOldPassword.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        txtNewPassword.topAnchor.constraint(equalTo: txtOldPassword.bottomAnchor, constant:10).isActive = true
        txtNewPassword.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        txtNewPassword.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        txtConfirmNewPassword.topAnchor.constraint(equalTo: txtNewPassword.bottomAnchor, constant:10).isActive = true
        txtConfirmNewPassword.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        txtConfirmNewPassword.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        dimView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        dimView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        dimView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    // MARK: Helpers
    
    //enable save btn
    func enableSaveBtn(){
        if (isOldPasswordSet && isNewPasswordSet && isConfirmNewPasswordSet){
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        } else {
             self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
    
    //updateAppearance updating
    func updateAppearanceUpdating(_ sender : Any){
        //hide keyboard
        hideKeyboard(sender)
        //show dimView
        dimView.isHidden = false
        //show activity indicator
        self.activityIndicator.startAnimating()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activityIndicator)
    }
    
    //updateAppearance updatingComplete
    func updateAppearanceUpdatingComplete(_ sender : Any){
        //show dimView
        self.dimView.isHidden = true
        //reset navigation item
        self.activityIndicator.stopAnimating()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(self.updatePassword))
    }
    
    // MARK: Actions
    
    //hide keyboard
    @objc private func hideKeyboard(_ sender: Any){
        self.view.endEditing(true)
    }
    
    //update password
    @objc func updatePassword(_ sender: UIBarButtonItem) -> Void {
        
        //do passwords match?
        if (txtNewPassword.text != txtConfirmNewPassword.text){
            //create alert
            let alertView: UIAlertController = {
                let myAlertView = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
                return myAlertView
            }()
            alertView.title = NSLocalizedString("", comment: "")
            alertView.message =  NSLocalizedString("New passwords don't match", comment: "")
            let action = UIAlertAction(title: NSLocalizedString("Close", comment:""), style: UIAlertActionStyle.default, handler: nil)
            alertView.addAction(action)
            
            //present alert
            self.present(alertView, animated: true, completion: nil)
            
        } else {
            //update appearance
            self.updateAppearanceUpdating(sender)
            
            //create password JSON string
            let passwordJSON = "{\"oldPassword\":\"\(txtOldPassword.text!)\", \"newPassword\":\"\(txtNewPassword.text!)\", \"newPasswordConfirmation\":\"\(txtConfirmNewPassword.text!)\"}"
            
            //start update request
            JopaJopaClient.sharedInstance().updateUserPassword(jsonBody: passwordJSON){ result, error in
                
                //update appearance
                performUIUpdatesOnMain {
                    self.updateAppearanceUpdatingComplete(sender)
                }
                
                /* Display Success Message: */
                func displaySuccessMessage(){
                    //create alert  view
                    let alertView: UIAlertController = {
                        let title = NSLocalizedString(" Password Updated", comment: "")
                        let message =  NSLocalizedString("Your password was successfully updated", comment: "")
                        let myAlertView = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
                        //action
                        let action = UIAlertAction(title: NSLocalizedString("Close", comment:""), style: UIAlertActionStyle.default){ handler in
                            self.navigationController?.popViewController(animated: true)
                        }
                        myAlertView.addAction(action)
                        return myAlertView
                    }()
                    
                    //update UI
                    performUIUpdatesOnMain {
                        self.present(alertView, animated: true, completion: nil)
                    }
                }
                
                // check update successful
                if let error = error {
                    let alertController = JopaJopaClient.sharedInstance().getAlertControllerForErrors(error, JopaJopaClient.JopaJopaTask.Update_Password)
                    performUIUpdatesOnMain {
                        self.present(alertController, animated: true, completion: nil)
                    }
                    
                } else {
                    displaySuccessMessage()
                }
                
            }
        }
        
    }
    
}


// MARK: SignUpViewController Extension (TextField Delegate)


extension UpdatePasswordViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // get email and password from text
        var updatedOldPassword = txtOldPassword.text!
        var updatedNewPassword = txtNewPassword.text!
        var updatedConfirmNewPassword = txtConfirmNewPassword.text!
        
        if let oldPassword = txtOldPassword.text,
            let textRange = Range(range, in: oldPassword) {
            if (textField == txtOldPassword){
                updatedOldPassword = oldPassword.replacingCharacters(in: textRange,
                                                                 with: string)
            }
        }
        
        if let newPassword = txtNewPassword.text,
            let textRange = Range(range, in: newPassword) {
            if (textField == txtNewPassword){
                updatedNewPassword = newPassword.replacingCharacters(in: textRange,
                                                               with: string)
            }
        }
        
        if let confirmNewPassword = txtConfirmNewPassword.text,
            let textRange = Range(range, in: confirmNewPassword) {
            if (textField == txtConfirmNewPassword){
                updatedConfirmNewPassword = confirmNewPassword.replacingCharacters(in: textRange,
                                                                 with: string)
            }
        }
        
        
        //enable update button
        
        if (!updatedOldPassword.isEmpty){
            self.isOldPasswordSet = true
        } else {
            self.isOldPasswordSet = false
        }
        
        if (!updatedNewPassword.isEmpty){
            self.isNewPasswordSet = true
        } else {
            self.isNewPasswordSet = false
        }
        
        if (!updatedConfirmNewPassword.isEmpty){
            self.isConfirmNewPasswordSet = true
        } else {
            self.isConfirmNewPasswordSet = false
        }
        
        //enable save btn
        enableSaveBtn()
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
