//
//  UpdateEmailViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 2/7/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit

class UpdateEmailViewController: UIViewController {

    // MARK: Properties
    
    //booleans for enabling save btn
    var isNewEmailSet = false
    var isPasswordSet = false
    
    let txtNewEmail: InputTextField = {
        let txtField = InputTextField()
        txtField.placeholder = NSLocalizedString("Enter New E-mail", comment:"")
        return txtField
    }()
    
    let txtPassword: InputTextField = {
        let txtField = InputTextField()
        txtField.isSecureTextEntry = true
        txtField.placeholder = NSLocalizedString("Enter Current Password", comment:"")
        return txtField
    }()
    
    let activityIndicator : UIActivityIndicatorView = {
        let ai = UIActivityIndicatorView()
        ai.activityIndicatorViewStyle = .gray
        ai.color = ViewConstants.BUTTON_COLOR_DEFAULT
        ai.translatesAutoresizingMaskIntoConstraints = false
        return ai
    }()
    
    let dimView : UIView = {
        let view = UIView()
        view.backgroundColor = ViewConstants.BACKGROUND_COLOR_DIM
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //title
        self.title = "E-mail"
        
        //hide tab bar
        self.tabBarController?.tabBar.isHidden = true
        
        //delegates
        txtNewEmail.delegate = self
        txtPassword.delegate = self
        
        //right bar button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(updateEmail))
        
        //intialize save btn
        enableSaveBtn()
        
        //add textFields
        self.view.addSubview(txtNewEmail)
        self.view.addSubview(txtPassword)
        
        //dim view
        self.view.addSubview(dimView)
        
        //setup layout
        setupLayout()
        
    }
    
    override func viewDidLayoutSubviews() {
        
        //add bottom borders, done after adding views
        txtNewEmail.addBottomBorder()
        txtPassword.addBottomBorder()
    }
    
    
    // MARK: UI Layout
    
    func setupLayout(){
        
        txtNewEmail.topAnchor.constraint(equalTo: view.topAnchor, constant:100).isActive = true
        txtNewEmail.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        txtNewEmail.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        txtPassword.topAnchor.constraint(equalTo: txtNewEmail.bottomAnchor, constant:10).isActive = true
        txtPassword.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        txtPassword.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        dimView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        dimView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        dimView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
    }
    
    // MARK: Helpers
    
    //enable save btn
    func enableSaveBtn(){
        if (isNewEmailSet && isPasswordSet){
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        } else {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
    
    //updateAppearance updating
    
    func updateAppearanceUpdating(_ sender : Any){
        //hide keyboard
        hideKeyboard(sender)
        //show dimView
        dimView.isHidden = false
        //show activity indicator
        self.activityIndicator.startAnimating()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activityIndicator)
    }
    
    //updateAppearance updatingComplete
    func updateAppearanceUpdatingComplete(_ sender : Any){
        performUIUpdatesOnMain {
            //show dimView
            self.dimView.isHidden = true
            //reset navigation item
            self.activityIndicator.stopAnimating()
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(self.updateEmail))
        }
    }
    
    // MARK: Actions
    
    //hide keyboard
    @objc private func hideKeyboard(_ sender: Any){
        self.view.endEditing(true)
    }
    
    //update e-mail
    @objc func updateEmail(_ sender: UIBarButtonItem) -> Void {
        
        //update appearance
        updateAppearanceUpdating(sender)
        
        //create password JSON string
        let newEmailJSON = "{\"password\":\"\(txtPassword.text!)\", \"email\":\"\(txtNewEmail.text!)\"}"
        
        //call convenience method to update
        JopaJopaClient.sharedInstance().updateUserEmail(jsonBody: newEmailJSON){ result, error in
            
            //update appearance
            performUIUpdatesOnMain {
                self.updateAppearanceUpdatingComplete(sender)
            }
            
            /* Display Success Message: */
            func displaySuccessMessage(){
                //create alert  view
                let alertView: UIAlertController = {
                    let title = NSLocalizedString("Email Sent", comment: "")
                    let message =  NSLocalizedString("An activation link has been sent to your new e-mail. You e-mail will only be updated by clicking on the activation link", comment: "")
                    let myAlertView = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
                    //action
                    let action = UIAlertAction(title: NSLocalizedString("Close", comment:""), style: UIAlertActionStyle.default){ handler in
                        self.navigationController?.popViewController(animated: true)
                    }
                    myAlertView.addAction(action)
                    return myAlertView
                }()
                
                //update UI
                performUIUpdatesOnMain {
                    self.present(alertView, animated: true, completion: nil)
                }
            }
            
            // error
            if let error = error {
                let alertController = JopaJopaClient.sharedInstance().getAlertControllerForErrors(error, JopaJopaClient.JopaJopaTask.Update_Email)
                self.present(alertController, animated: true, completion: nil)
            } else {
                displaySuccessMessage()
            }
            
            
        }
        
    }
    
}


// MARK: SignUpViewController Extension (TextField Delegate)

extension UpdateEmailViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // get email and password from text
        var updatedNewEmail = txtNewEmail.text!
        var updatedNewPassword = txtPassword.text!
        
        if let oldEmail = txtNewEmail.text,
            let textRange = Range(range, in: oldEmail) {
            if (textField == txtNewEmail){
                updatedNewEmail = oldEmail.replacingCharacters(in: textRange,
                                                                     with: string)
            }
        }
        
        if let password = txtPassword.text,
            let textRange = Range(range, in:password) {
            if (textField == txtPassword){
                updatedNewPassword = password.replacingCharacters(in: textRange,
                                                                     with: string)
            }
        }
        
        //enable update button
        
        if (!updatedNewEmail.isEmpty && InputValidator.isValidEmail(updatedNewEmail)){
            isNewEmailSet = true
        } else {
            isNewEmailSet = false
        }
        
        if (!updatedNewPassword.isEmpty){
            isPasswordSet = true
        } else {
            isPasswordSet = false
        }
        
        enableSaveBtn()
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
