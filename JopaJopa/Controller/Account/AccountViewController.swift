//
//  AccountViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 20/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit

// MARK : AccountViewController

class AccountViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    // MARK : Properties
    
    let accountItems = [
        NSLocalizedString("My Details", comment:""),
        NSLocalizedString("Update Password", comment:""),
        NSLocalizedString("Update Email", comment:""),
        NSLocalizedString("Delete Account", comment:""),
    ]
    let tableView: UITableView = {
        let tblView = UITableView()
        tblView.translatesAutoresizingMaskIntoConstraints = false
        tblView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        return tblView
    }()
    
    // MARK : Lifecyle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //title
        self.title = NSLocalizedString("Account", comment:"")
        
        //add sign out bar button item
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Sign Out", style: UIBarButtonItemStyle.plain, target: self, action: #selector(signOut))
        
        //data source and delegate
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        //add subviews
        self.view.addSubview(tableView)
        
        //layout ui
        setupLayout()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //show tabbar if hidden
        self.tabBarController?.tabBar.isHidden = false
    }
    
    // MARK : UI Layout
    
    private func setupLayout() {
        
        //table view
        self.tableView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return accountItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        cell.textLabel?.text = self.accountItems[indexPath.row]
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch (indexPath.row) {
        case 0: self.navigationController?.pushViewController(MyDetailsViewController(), animated: true)
        case 1: self.navigationController?.pushViewController(UpdatePasswordViewController(), animated: true)
        case 2: self.navigationController?.pushViewController(UpdateEmailViewController(), animated: true)
        case 3: self.navigationController?.pushViewController(DeleteAccountViewController(), animated: true)
        default: break
        }
        
    }
    
    // MARK : Actions
    
    @objc func signOut(){
        
        //alert view
        let alertView: UIAlertController = {
            let myAlertView = UIAlertController(title: NSLocalizedString("Sign Out", comment:""), message: NSLocalizedString("You will be signed out of JopaJopa", comment: ""), preferredStyle: .alert)
            return myAlertView
        }()
       
        //add actions
        
        let actionCancel = UIAlertAction(title: NSLocalizedString("Cancel", comment:""), style: UIAlertActionStyle.default, handler: nil)
        
        let actionContinue = UIAlertAction(title: NSLocalizedString("Continue", comment:""), style: UIAlertActionStyle.default){handler in
            //update user defaults
            UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
            self.present(MainTabViewController(), animated: true, completion: nil)
        }
        
        alertView.addAction(actionCancel)
        alertView.addAction(actionContinue)
        
        //show alert
        self.present(   alertView, animated: true, completion: nil)
    }

}
