//
//  MapViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 28/8/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
    
    // MARK: Properties
    
    let map : MKMapView = {
        let mkMapView = MKMapView()
        mkMapView.translatesAutoresizingMaskIntoConstraints  = false
        return mkMapView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Map"
        
        self.view.addSubview(map)

        setupLayout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setupLayout(){
        map.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        map.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        map.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
    }

}
