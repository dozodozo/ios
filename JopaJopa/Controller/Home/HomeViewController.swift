//
//  ViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 6/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    var tabBar: UITabBar!
    
    let containerView: UIView = {
        let view = UIView()
        //view.layer.borderColor = UIColor.red.cgColor
        //view.layer.borderWidth = 4
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let ttView: UIView = {
        let view = UIView()
        //view.layer.borderColor = UIColor.yellow.cgColor
        //view.layer.borderWidth = 4
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    //jopajopa label
    let lblJopaJopa: UILabel = {
        let lbl = UILabel()
        lbl.text = "JopaJopa"
        lbl.textColor = UIColor.white
        lbl.textColor = UIColor(red: 1.0, green: 0.867, blue: 0, alpha: 1)
        lbl.font = UIFont(name: "DancingScript-Bold", size: 30)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
        
    }()
    
    let lblJopaJopaTag: UILabel = {
        let lbl = UILabel()
        lbl.text = "by locals,for locals"
        lbl.textColor = UIColor.white
        lbl.font = UIFont.preferredFont(forTextStyle: .caption1).bold()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
        
    }()
    
    let userButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "head"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    //backgroundI/Users/kinzang/Documents/gitLab/dozodozo/mobileApps/ios/JopaJopa/JopaJopa/Controller/Home/HomeViewControllerTest.swiftmage
    let imgViewBackground: UIImageView = {
        //let imageview = UIImageView(image: #imageLiteral(resourceName: "darlingStreetBlur"))
        let imageview = UIImageView(image: #imageLiteral(resourceName: "cityNight"))
        //let imageview = UIImageView(image: #imageLiteral(resourceName: "streetDay"))
        //let imageview = UIImageView(image: #imageLiteral(resourceName: "cobbledStreet"))
        imageview.isOpaque = true
        imageview.translatesAutoresizingMaskIntoConstraints = false
        return imageview
    }()
    
    //backgroundImage
    let darkFilter: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        //view.layer.opacity = 0.5
        view.layer.opacity = 0.5
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    //categories
    let btnEatDrink: CategoryButton = {
        let btn = CategoryButton()
        btn.setTitle(NSLocalizedString("Eat & Drink", comment:""), for: UIControlState.normal)
        btn.addTarget(self, action: #selector(getPostsInCategoryViewController), for: .touchUpInside)
        return btn
    }()
    
    let btnShop: CategoryButton = {
        let btn = CategoryButton()
        btn.setTitle(NSLocalizedString("Shop", comment:""), for: UIControlState.normal)
        btn.addTarget(self, action: #selector(getPostsInCategoryViewController), for: .touchUpInside)
        return btn
    }()
    
    let btnEntertainment: CategoryButton = {
        let btn = CategoryButton()
        
        btn.setTitle(NSLocalizedString("What's On", comment:""), for: UIControlState.normal)
        btn.addTarget(self, action: #selector(getPostsInCategoryViewController), for: .touchUpInside)
        return btn
    }()
    
    let btnEvents: CategoryButton = {
        let btn = CategoryButton()
        btn.setTitle("Events", for: UIControlState.normal)
        btn.addTarget(self, action: #selector(getPostsInCategoryViewController), for: .touchUpInside)
        return btn
    }()
    
    let btnHealthBeauty: CategoryButton = {
        let btn = CategoryButton()
        btn.setTitle(NSLocalizedString("Health & Beauty", comment:""), for: UIControlState.normal)
        btn.addTarget(self, action: #selector(getPostsInCategoryViewController), for: .touchUpInside)
        return btn
    }()
    
    let btnCommunity: CategoryButton = {
        let btn = CategoryButton()
        btn.setTitle(NSLocalizedString("Community", comment:""), for: UIControlState.normal)
        btn.addTarget(self, action: #selector(getPostsInCategoryViewController), for: .touchUpInside)
        return btn
    }()
    
    let lblMadeInBalmain : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.white
        lbl.text = "Made with \u{2665} in Balmain"
        //lbl.text = "Made with \u{2665} in InnerWest Sydney"
        lbl.font = UIFont.preferredFont(forTextStyle: .subheadline).bold()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    //charityPartner
    let lblCharityPartnerMsg: UILabel = {
        let lbl = UILabel()
        lbl.text = NSLocalizedString("Proud Partner Of", comment:"")
        lbl.textColor = UIColor.white
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let lblCharityPartnerName: UILabel = {
        let lbl = UILabel()
        lbl.text = "Australian Himalayan Foundation"
        lbl.textColor = UIColor.white
        lbl.font = UIFont.boldSystemFont(ofSize: 15)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    /*
     *
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("in home view")
        print(JopaJopaClient.sharedInstance().locationManager.requestLocation())
        print("last location: \(JopaJopaClient.sharedInstance().locationManager.location)")
        JopaJopaClient.sharedInstance().searchLocation = JopaJopaClient.sharedInstance().userLocation
        
        self.title = "JopaJopa"
        print("HomeView Did Load")
        
        //get tab bar for this view contorller
        tabBar = self.tabBarController?.tabBar
        
        
        //bacground image and color
        view.backgroundColor = ViewConstants.BACKGROUND_COLOR
        
        self.view.addSubview(containerView)
        self.view.addSubview(ttView)
        
        //add background views
        self.view.addSubview(imgViewBackground)
        self.view.addSubview(darkFilter)
        
        //add jopajopa
        self.view.addSubview(lblJopaJopa)
        self.view.addSubview(lblJopaJopaTag)
        
        //add login signup button
        self.view.addSubview(userButton)
        
        //add category views
        self.view.addSubview(btnEatDrink)
        self.view.addSubview(btnShop)
        self.view.addSubview(btnEntertainment)
        self.view.addSubview(btnHealthBeauty)
        self.view.addSubview(btnCommunity)
        self.view.addSubview(lblMadeInBalmain)
        
        //charity partner
//        self.view.addSubview(lblCharityPartnerMsg)
//        self.view.addSubview(lblCharityPartnerName)
        
        
        
        setupLayout()
        
    }
    
    /*
     *
     */
    
    override func viewWillAppear(_ animated: Bool) {
        print("HomeView Will Appear")
        
       
        //hide navigation bar of home view, for visual design
        self.navigationController?.navigationBar.isHidden = true
        
        //show tab bar of MainTabView only if logged in
        if (JopaJopaClient.sharedInstance().isUserLoggedIn!) {
            userButton.isHidden = true
            self.tabBar.isHidden = false
        } else {
            self.tabBar.isHidden = true
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("HomeView Did Disappear")
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    /*
     * Layout setup
     */
    
    func setupLayout(){
        
         //for testing - not shown
        containerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        containerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        
        imgViewBackground.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        imgViewBackground.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        imgViewBackground.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        imgViewBackground.bottomAnchor.constraint(equalTo:view.bottomAnchor).isActive = true
        
        userButton.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        userButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant:-10).isActive = true
        userButton.widthAnchor.constraint(equalToConstant: 44).isActive = true
        userButton.heightAnchor.constraint(equalTo: userButton.widthAnchor).isActive=true
        userButton.addTarget(self, action: #selector(showLoginSignUpViewController), for: UIControlEvents.touchUpInside)
        
        lblJopaJopa.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        //lblJopaJopa.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        lblJopaJopa.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
        lblJopaJopaTag.topAnchor.constraint(equalTo: lblJopaJopa.bottomAnchor, constant: 0).isActive = true
        lblJopaJopaTag.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
        darkFilter.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        darkFilter.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        darkFilter.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        darkFilter.bottomAnchor.constraint(equalTo:self.view.bottomAnchor).isActive = true
        
        // padding - divide view into 8 - use middle 6 for showing categories
        let paddingForCategoriesContainer = view.safeAreaLayoutGuide.layoutFrame.height/8
        let categoryBtnHeight = paddingForCategoriesContainer - 40
        
        //for testing - not shown
        ttView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: paddingForCategoriesContainer).isActive = true
        ttView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -paddingForCategoriesContainer).isActive = true
        ttView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        
        btnEatDrink.topAnchor.constraint(equalTo: ttView.topAnchor, constant: 20).isActive = true
        btnEatDrink.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        btnEatDrink.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant:-20).isActive = true
        btnEatDrink.heightAnchor.constraint(equalToConstant: categoryBtnHeight).isActive = true
       
        
        btnShop.topAnchor.constraint(equalTo: btnEatDrink.bottomAnchor, constant: 20).isActive = true
        btnShop.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        btnShop.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant:-20).isActive = true
        btnShop.heightAnchor.constraint(equalToConstant:categoryBtnHeight).isActive = true
        
        btnEntertainment.topAnchor.constraint(equalTo: btnShop.bottomAnchor, constant: 20).isActive = true
        btnEntertainment.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        btnEntertainment.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant:-20).isActive = true
        btnEntertainment.heightAnchor.constraint(equalToConstant: categoryBtnHeight).isActive = true

        
        btnHealthBeauty.topAnchor.constraint(equalTo: btnEntertainment.bottomAnchor, constant: 20).isActive = true
        btnHealthBeauty.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        btnHealthBeauty.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant:-20).isActive = true
        btnHealthBeauty.heightAnchor.constraint(equalToConstant: categoryBtnHeight).isActive = true
        
        btnCommunity.topAnchor.constraint(equalTo: btnHealthBeauty.bottomAnchor, constant: 20).isActive = true
        btnCommunity.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        btnCommunity.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant:-20).isActive = true
        btnCommunity.heightAnchor.constraint(equalToConstant: categoryBtnHeight).isActive = true
        
        
        if (!tabBar.isHidden) {
            let tabHeight = -1 * (self.tabBarController?.tabBar.frame.height)!
            print(tabHeight)
            lblMadeInBalmain.topAnchor.constraint(equalTo: self.view.bottomAnchor, constant: tabHeight-40).isActive = true
            lblMadeInBalmain.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        } else {
            lblMadeInBalmain.topAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -10).isActive = true
            lblMadeInBalmain.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        }
       
        
        
//        if (!tabBar.isHidden) {
//            let tabHeight = -1 * (self.tabBarController?.tabBar.frame.height)!
//            print(tabHeight)
//            lblCharityPartnerName.topAnchor.constraint(equalTo: self.view.bottomAnchor, constant: tabHeight-40).isActive = true
//            lblCharityPartnerName.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
//        } else {
//            lblCharityPartnerName.topAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -10).isActive = true
//            lblCharityPartnerName.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
//        }
//        lblCharityPartnerMsg.topAnchor.constraint(equalTo: lblCharityPartnerName.topAnchor, constant: -20).isActive = true
//        lblCharityPartnerMsg.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
    }
    
    // MARK: LoginSignUpViewController
    
    @objc func showLoginSignUpViewController(_ sender: Any){
        self.navigationController?.navigationBar.isHidden = false
        
        let loginSignUpTabBarController = LoginSignUpTabBarController()
        self.navigationController?.pushViewController(loginSignUpTabBarController, animated: true)
    }
    
    // MARK: show posts
    
    @objc func getPostsInCategoryViewController(_ sender: UIButton){
        
        self.navigationController?.navigationBar.isHidden = false
        let getPostsInCategoryController = GetPostsInCategoryViewController()
        getPostsInCategoryController.categoryTitle = sender.titleLabel?.text
        
        
        print("has navigation ctonroller:\(self.navigationController)")
        self.navigationController?.pushViewController(getPostsInCategoryController, animated: true)
    }
    
    
}
