//
//  GetPostsInCategoryViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 26/8/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import GooglePlaces

class GetPostsInCategoryViewController: UIViewController {
    
    // MARK : Properties
    
    //store list of places returned from db
    var publishedPosts: [JopaJopaPublishedPost]? = [JopaJopaPublishedPost]()
    
    // for loading posts automatically when end is reached
    // lastDateCreated keeps track of date created for last post
    // initilized to current date
    var dateUpperLimit: String = TimeDistanceHelper.isoFormattedDate(date: Date())
    
    //boolean to check whether to fetch more data
    //only fetch more if last get post request returned 10 items which is the limit per request
    var fetchMore: Bool = true
    var fetchBatchSize: Int = 3 //how many per batch to fetch
    
    var categoryTitle: String?
    
    //google places
    let gmsAutoCompleteController = GMSAutocompleteViewController()
    
    
    //use for animation
    var locationInputContainerIsVisible = false
    var locationInputContainerTopAnchorConstraint: NSLayoutConstraint?
    let locationInputHeight: CGFloat = 120
    
    //view to show if places is empty, consists of label and image view
    let nothingView: UIView = {
        let tempView = UIView()
        tempView.translatesAutoresizingMaskIntoConstraints = false
        return tempView
    }()
    
    let labelEmptyResults: UILabel = {
        let lbl = UILabel()
        lbl.text = NSLocalizedString("Sorry! It is pretty quiet right now. Check again later.", comment: "")
        lbl.textAlignment =  NSTextAlignment.center
        lbl.font = UIFont.preferredFont(forTextStyle: .body)
        lbl.numberOfLines = 0
        lbl.textColor = ViewConstants.LIGHT_TEXT_COLOR
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let imageViewEmptyResults: UIImageView = {
        let imgView = UIImageView(image: #imageLiteral(resourceName: "bench"))
        imgView.translatesAutoresizingMaskIntoConstraints = false
        return imgView
    }()
    
    let tableView: UITableView = {
        let tblView = UITableView()
        tblView.translatesAutoresizingMaskIntoConstraints = false
        tblView.register(PublishedPostsTableViewCell.self, forCellReuseIdentifier: "publishedPostCell")
        tblView.separatorStyle = UITableViewCellSeparatorStyle.none
        return tblView
    }()
    
    // MARK : Lifecycle
    let locationInputContainer : UIView = {
        let view = UIView()
        view.backgroundColor = ViewConstants.TAB_BAR_BACKGROUND_COLOR
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.borderWidth = 1.0
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let txtLocation: UITextField = {
        let txtField = UITextField()
        txtField.textColor = ViewConstants.LIGHT_TEXT_COLOR
        txtField.translatesAutoresizingMaskIntoConstraints = false
        txtField.borderStyle = UITextBorderStyle.roundedRect
        txtField.placeholder = NSLocalizedString("Choose Location", comment: "")
        return txtField
    }()
    
    let imgYourLocation: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "locationPin"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let lblYourLocation: UILabel = {
        let lbl = UILabel()
        lbl.text = NSLocalizedString("Use Your Location", comment: "")
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    // MARK: Activity Indicator
    
    let dimView : UIView = {
        let view = UIView()
        view.backgroundColor = ViewConstants.BACKGROUND_COLOR_DIM
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let activityIndicator: UIActivityIndicatorView = {
        let actInd = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        actInd.translatesAutoresizingMaskIntoConstraints = false
        return actInd
    }()
    
    //refresh control
    let refreshControl: UIRefreshControl = {
        let rc = UIRefreshControl()
        rc.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        return rc
    }()
    
    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //title
        self.title = categoryTitle
        
        //intial search address
        txtLocation.text = JopaJopaClient.sharedInstance().searchAddress
        
        //google places delegate
        gmsAutoCompleteController.delegate = self
        txtLocation.delegate = self
        
        //right bar button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "locationPin"), style: .plain, target: self, action: #selector(showHideLocationInputView))
        
        //add nothing views
        self.view.addSubview(nothingView)
        self.nothingView.addSubview(labelEmptyResults)
        self.nothingView.addSubview(imageViewEmptyResults)
        
        //tableview setup
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        //add tapgesture to your location
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(setSearchLocationToYourLocation))
        lblYourLocation.addGestureRecognizer(tapGestureRecognizer)
        lblYourLocation.isUserInteractionEnabled = true
        
        
        if #available(iOS 10.0, *) {
            self.tableView.refreshControl = refreshControl
        } else {
            self.tableView.addSubview(refreshControl)
        }
        
        //add views
        self.view.addSubview(tableView)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 400
        
        self.view.addSubview(locationInputContainer)
        self.locationInputContainer.addSubview(txtLocation)
        self.locationInputContainer.addSubview(imgYourLocation)
        self.locationInputContainer.addSubview(lblYourLocation)
        
        // dim view
        self.view.addSubview(dimView)
        self.dimView.addSubview(activityIndicator)
        
        
        //layout
        setUpLayout()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //check if location services available and get posts if so
        checkIfLocationServicesOK()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Layout
    
    func setUpLayout(){
        
        //table view
        self.tableView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        //locationInput
        locationInputContainerTopAnchorConstraint = self.locationInputContainer.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: -locationInputHeight)
        locationInputContainerTopAnchorConstraint?.isActive = true
        self.locationInputContainer.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.locationInputContainer.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        self.locationInputContainer.heightAnchor.constraint(equalToConstant: locationInputHeight).isActive = true
        
        txtLocation.topAnchor.constraint(equalTo: locationInputContainer.topAnchor, constant: 10).isActive = true
        txtLocation.leadingAnchor.constraint(equalTo: locationInputContainer.leadingAnchor, constant: 10).isActive = true
        txtLocation.trailingAnchor.constraint(equalTo: locationInputContainer.trailingAnchor, constant: -10).isActive = true
        txtLocation.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        imgYourLocation.topAnchor.constraint(equalTo: txtLocation.bottomAnchor, constant: 20).isActive = true
        imgYourLocation.leadingAnchor.constraint(equalTo: locationInputContainer.leadingAnchor, constant: 10).isActive = true
        imgYourLocation.widthAnchor.constraint(equalToConstant: 20).isActive = true
        imgYourLocation.heightAnchor.constraint(equalTo: imgYourLocation.widthAnchor).isActive = true
        
        lblYourLocation.topAnchor.constraint(equalTo: txtLocation.bottomAnchor, constant: 20).isActive = true
        lblYourLocation.leadingAnchor.constraint(equalTo: imgYourLocation.trailingAnchor, constant: 5).isActive = true
        lblYourLocation.trailingAnchor.constraint(equalTo: locationInputContainer.trailingAnchor, constant: -10).isActive = true
        
        //empty result image view
        self.imageViewEmptyResults.centerYAnchor.constraint(equalTo: self.nothingView.centerYAnchor, constant: 10).isActive = true
        self.imageViewEmptyResults.centerXAnchor.constraint(equalTo: self.nothingView.centerXAnchor).isActive = true
        
        //empty result message
        self.labelEmptyResults.leadingAnchor.constraint(equalTo: self.nothingView.leadingAnchor, constant: 10).isActive = true
        self.labelEmptyResults.trailingAnchor.constraint(equalTo: self.nothingView.trailingAnchor, constant: -10).isActive = true
        self.labelEmptyResults.topAnchor.constraint(equalTo: self.imageViewEmptyResults.bottomAnchor, constant: 5).isActive = true
        
        //nothing view
        self.nothingView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        self.nothingView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        self.nothingView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.nothingView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        
        
        //activity indicator
        
        dimView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        dimView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        dimView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        activityIndicator.centerYAnchor.constraint(equalTo: self.dimView.centerYAnchor, constant:40).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: self.dimView.centerXAnchor).isActive = true
    }
    
    // MARK : Helpers
    
    func checkIfLocationServicesOK(){
        if (!JopaJopaClient.sharedInstance().isLocationServiceOK()){
            
            //alert view
            let alertView: UIAlertController = {
                let locationUnavailable = NSLocalizedString("Location Not Available", comment: "")
                let msg = NSLocalizedString("Please check that location services has been turned on and the app is allowed to access your location", comment: "")
                let myAlertView = UIAlertController(title: locationUnavailable, message: msg, preferredStyle: UIAlertControllerStyle.alert)
                return myAlertView
            }()
            
            //action
            let a1 = UIAlertAction(title: "OK", style: .default){handler in
                self.navigationController?.popViewController(animated: true)
            }
            alertView.addAction(a1)
            
            self.present(alertView, animated: true, completion: nil)
            
        } else {
            //get posts
            getPublishedPosts(lastDateCreated: dateUpperLimit)
        }
    }
    
    // MARK: Actions
    
    @objc func refreshList(_ sender: UIRefreshControl){
        //clear post list
        self.publishedPosts?.removeAll()
        
        //reset fetchMore
        self.fetchMore = true
        
        //update dateUpperLimit to current time
        dateUpperLimit = TimeDistanceHelper.isoFormattedDate(date: Date())
        getPublishedPosts(lastDateCreated: dateUpperLimit)
        sender.endRefreshing()
    }
    
    @objc func setSearchLocationToYourLocation(_ sender: Any){
        
        //update search location
        JopaJopaClient.sharedInstance().searchAddress = NSLocalizedString("Your Location", comment: "")
        JopaJopaClient.sharedInstance().searchLocation = JopaJopaClient.sharedInstance().userLocation
        
        //update textField
        txtLocation.text = NSLocalizedString("Your Location", comment: "")
        
        //hide location input
        showHideLocationInputView(txtLocation)
        
        //update current time and get posts
        dateUpperLimit = TimeDistanceHelper.isoFormattedDate(date: Date())
        getPublishedPosts(lastDateCreated: dateUpperLimit)
    }
    
    @objc func showHideLocationInputView(_ sender : Any){
        
        if !locationInputContainerIsVisible{
            UIView.transition(with: locationInputContainer, duration: 0.5, options: .transitionFlipFromTop, animations: {
                self.locationInputContainerTopAnchorConstraint?.constant = 0
                self.view.layoutIfNeeded()
            }, completion: nil)
        } else {
            UIView.transition(with: locationInputContainer, duration: 0.5, options: .transitionFlipFromBottom, animations: {
                self.locationInputContainerTopAnchorConstraint?.constant = -self.locationInputHeight
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
        
        locationInputContainerIsVisible = !locationInputContainerIsVisible
        
    }
    
    // MARK: get posts
    
    func getPublishedPosts(lastDateCreated: String) -> Void {
        
        let lat  = JopaJopaClient.sharedInstance().searchLocation?.coordinate.latitude
        let long = JopaJopaClient.sharedInstance().searchLocation?.coordinate.longitude
        
        //show activity indicator
        self.dimView.isHidden = false
        self.activityIndicator.startAnimating()

        //get places list
        JopaJopaClient.sharedInstance().getPublishedPosts(lat: lat!, long: long!, lastDateCreated: lastDateCreated, postCategory: categoryTitle!) {result, error in
            
            //hide activity indicator
            performUIUpdatesOnMain {
                self.activityIndicator.stopAnimating()
                self.dimView.isHidden = true
            }
            
            //check if request was successful
            if let error = error {
                //alert controller
                let alertController = JopaJopaClient.sharedInstance().getAlertControllerForErrors(error, JopaJopaClient.JopaJopaTask.Get_Published_Posts)
                
                // if error is authentication failure, log user out
                if (error.userInfo["JopaJopaErrorType"] as! String == JopaJopaClient.JopaJopaErrorType.AUTHENTICATION_FAILURE){
                    JopaJopaClient.sharedInstance().logoutUser()
                    let logoutAction = UIAlertAction(title: "Close", style: .default){ handler in
                        self.present(MainTabViewController(), animated: true, completion: nil)
                    }
                    alertController.addAction(logoutAction)
                }
                performUIUpdatesOnMain {
                    self.present(alertController, animated: true, completion: nil)
                }
            } else {
                //save posts
                //self.publishedPosts = publishedPosts!
                self.publishedPosts?.append(contentsOf: result!)
                
                if (self.publishedPosts!.isEmpty){
                    self.fetchMore = false
                    performUIUpdatesOnMain {
                        self.tableView.reloadData()
                        self.tableView.isHidden = true
                        self.nothingView.isHidden = false
                    }
                } else {
                    if ((result?.count)! < self.fetchBatchSize){
                        self.fetchMore = false
                    }
                    performUIUpdatesOnMain {
                        self.tableView.reloadData()
                        self.tableView.isHidden = false
                        self.nothingView.isHidden = true
                    }
                }
            }
            
        }
    }
    
    

}

extension GetPostsInCategoryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return publishedPosts?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let publishedPost = self.publishedPosts![indexPath.row]
        
        print("Date:\(publishedPost.dateCreated)")
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "publishedPostCell", for: indexPath) as! PublishedPostsTableViewCell

        cell.postTitle.text =  publishedPost.postTitle
        cell.postText.text = publishedPost.postText
        cell.placeName.text = publishedPost.placeName
        cell.placeAddress.text = publishedPost.placeLocation?.address
        
        //distance from user
        
        //is search location and user location same?
        if (JopaJopaClient.sharedInstance().searchLocation == JopaJopaClient.sharedInstance().userLocation){
            cell.distanceFromUser.text = TimeDistanceHelper.formatDistance(dist: publishedPost.distance!)
        } else {
            let placeLocation = CLLocation(latitude: publishedPost.placeGeoLocation![1], longitude: publishedPost.placeGeoLocation![0])
            let d = JopaJopaClient.sharedInstance().userLocation?.distance(from: placeLocation)
            cell.distanceFromUser.text = TimeDistanceHelper.formatDistance(dist: d!)
        }
        

        //add action to launch map
        // for launching map
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showMap))
        cell.mapIconImageView.addGestureRecognizer(tapGestureRecognizer)
        cell.mapIconImageView.tag = indexPath.row
        cell.mapIconImageView.isUserInteractionEnabled = true
        cell.distanceFromUser.addGestureRecognizer(tapGestureRecognizer)
        cell.distanceFromUser.tag = indexPath.row
        cell.distanceFromUser.isUserInteractionEnabled = true

        //calculate time since post published
        let datePosted = publishedPost.dateCreated
        let timeInterval = (datePosted?.timeIntervalSinceNow)! * -1
        cell.timePosted.text = " \(TimeDistanceHelper.secondsToHoursOrMinutesOnlyFormatted(seconds: Int(timeInterval))) ago"
        
        //get post image
        var url = URL(string: publishedPost.postImageURL!)
        JopaJopaClient.sharedInstance().taskForGETImageFromSpaces(url: url!, useCachedImage: true){ imageData, error in
            
            //show image loading indicator
            
            if (error != nil){
                performUIUpdatesOnMain {
                    cell.postImageView.image = UIImage(named: "defaultImage")
                }

            } else
            {
                if let imageData = imageData {
                    performUIUpdatesOnMain {
                        //activityIndicator.stopAnimating()
                        cell.postImageView.image = UIImage(data: imageData)
                    }
                }
            }
        }

        //get place image
        url = URL(string: publishedPost.placeImageURL!)
        JopaJopaClient.sharedInstance().taskForGETImageFromSpaces(url: url!, useCachedImage: true){ imageData, error in
            if (error != nil){
                performUIUpdatesOnMain {
                    cell.placeImageView.image = UIImage(named: "defaultImage")
                }

            } else
            {
                if let imageData = imageData {
                    performUIUpdatesOnMain {
                        cell.placeImageView.image = UIImage(data: imageData)
                    }
                }
            }
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        print("Drawing Cell at row:\(indexPath.row)")
        //check whether to load more data
        print("FECHMO MORE: \(self.fetchMore)")
        if (self.fetchMore && indexPath.row == (self.publishedPosts?.count)! - 1){
            print("FETCHING MORE")
            //update dateUpperLimit to dateTime of last post
            let dateCreatedLastPost = self.publishedPosts![(self.publishedPosts?.count)! - 1].dateCreated
            dateUpperLimit = TimeDistanceHelper.isoFormattedDate(date: dateCreatedLastPost!)
            getPublishedPosts(lastDateCreated: dateUpperLimit)
        }
    }
    
    // Show Map
    
    @objc func showMap(_ sender: UITapGestureRecognizer) {
        let publishedPost = self.publishedPosts![(sender.view?.tag)!]
        
        let userLocation: CLLocation = JopaJopaClient.sharedInstance().userLocation!
        let placeLocation: CLLocation = CLLocation(latitude: publishedPost.placeGeoLocation![1], longitude: publishedPost.placeGeoLocation![0])
        
        let topLeftCoord = CLLocation(latitude: fmax(userLocation.coordinate.latitude, placeLocation.coordinate.latitude), longitude: fmin(userLocation.coordinate.longitude, placeLocation.coordinate.longitude))
        let bottomRightCoord = CLLocation(latitude: fmin(userLocation.coordinate.latitude, placeLocation.coordinate.latitude), longitude: fmax(userLocation.coordinate.longitude, placeLocation.coordinate.longitude))
        
        var region : MKCoordinateRegion = MKCoordinateRegion()
        region.center.latitude = topLeftCoord.coordinate.latitude - (topLeftCoord.coordinate.latitude - bottomRightCoord.coordinate.latitude) * 0.5
        region.center.longitude = topLeftCoord.coordinate.longitude + (bottomRightCoord.coordinate.longitude - topLeftCoord.coordinate.longitude) * 0.5;
        
        // Add a little extra space on the sides
        region.span.latitudeDelta = fabs(topLeftCoord.coordinate.latitude - bottomRightCoord.coordinate.latitude) * 1.5;
        region.span.longitudeDelta = fabs(bottomRightCoord.coordinate.longitude - topLeftCoord.coordinate.longitude) * 1.5;
        
        let mapViewController = MapViewController()
        mapViewController.map.setRegion(region, animated: true)
        
        let annotation1 = MKPointAnnotation()
        annotation1.coordinate = placeLocation.coordinate
        annotation1.title = publishedPost.placeName
        annotation1.subtitle = publishedPost.placeLocation?.address
        
        let annotation2 = MKPointAnnotation()
        annotation2.coordinate = userLocation.coordinate
        annotation2.title = "Your Location"
    
        mapViewController.map.addAnnotation(annotation1)
        mapViewController.map.selectAnnotation(annotation1, animated: true)
        mapViewController.map.addAnnotation(annotation2)
        
        self.navigationController?.pushViewController(mapViewController, animated: true)
        
    }
    
}

// MARK: TextView Extension

extension GetPostsInCategoryViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.present(gmsAutoCompleteController, animated: true, completion: nil)
    }
}

// MARK: Google Place AutoComplete Delegate

extension GetPostsInCategoryViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        //save value to gmsPlace property
        let gmsPlace = place
        
        //update search location
        JopaJopaClient.sharedInstance().searchLocation = CLLocation(latitude: gmsPlace.coordinate.latitude, longitude: gmsPlace.coordinate.longitude)
        
        //update address field
        txtLocation.text = gmsPlace.formattedAddress
        JopaJopaClient.sharedInstance().searchAddress = gmsPlace.formattedAddress
        
        //hide location input
        showHideLocationInputView(txtLocation)
        
        //update current time and get posts
        dateUpperLimit = TimeDistanceHelper.isoFormattedDate(date: Date())
        getPublishedPosts(lastDateCreated: dateUpperLimit)
        
        dismiss(animated: true, completion: nil)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        dismiss(animated: true, completion: nil)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        //hide location input
        showHideLocationInputView(txtLocation)
        
        dismiss(animated: true, completion: nil)
    }
}

