//
//  PaymentHistoryViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 13/9/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit

class PaymentHistoryViewController: UIViewController {
    
    // MARK: Properties
    
    var charges: [JopaJopaStripeCharge] = [JopaJopaStripeCharge]()
    
    let tableView: UITableView = {
        let tblView = UITableView()
        tblView.translatesAutoresizingMaskIntoConstraints = false
        tblView.register(PaymentHistoryTableViewCell.self, forCellReuseIdentifier: "cell")
        tblView.tableFooterView = UIView(frame: .zero)
        return tblView
    }()
    
    let activityIndicator: UIActivityIndicatorView = {
        let ai = UIActivityIndicatorView()
        ai.translatesAutoresizingMaskIntoConstraints = false
        ai.color = UIColor.gray
        return ai
    }()
    
    let dimView : UIView = {
        let view = UIView()
        view.backgroundColor = ViewConstants.BACKGROUND_COLOR_DIM
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //title
        self.title = NSLocalizedString("Top Up History", comment: "")
        
        //hide tabbar
        self.tabBarController?.tabBar.isHidden = true
        
        //add subview
        self.view.addSubview(tableView)
        self.view.addSubview(activityIndicator)
        self.view.addSubview(dimView)
        
        //delegate
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        tableView.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        tableView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
        //activity indicator
        self.activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        self.activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        //dim view
        dimView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        dimView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        dimView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        //get paymetn history
        getPaymentHistory()
    }


    // MARK : ACTION
    
    func getPaymentHistory(){
        
        //ui update
        self.activityIndicator.startAnimating()
        self.dimView.isHidden = false
        
        JopaJopaClient.sharedInstance().getListOfCharges() { charges, error in
            
            //ui update
            performUIUpdatesOnMain {
                self.dimView.isHidden = true
                self.activityIndicator.stopAnimating()
            }
            
            if let error = error {
                //alert controller
                let alertController = JopaJopaClient.sharedInstance().getAlertControllerForErrors(error, JopaJopaClient.JopaJopaTask.Get_Payment_History)
                
                // if error is authentication failure, log user out
                if (error.userInfo["JopaJopaErrorType"] as! String == JopaJopaClient.JopaJopaErrorType.AUTHENTICATION_FAILURE){
                    JopaJopaClient.sharedInstance().logoutUser()
                    let logoutAction = UIAlertAction(title: "Close", style: .default){ handler in
                        self.present(MainTabViewController(), animated: true, completion: nil)
                    }
                    alertController.addAction(logoutAction)
                }
                performUIUpdatesOnMain {
                    self.present(alertController, animated: true, completion: nil)
                }
            } else {
                self.charges = charges!
                performUIUpdatesOnMain {
                    self.tableView.reloadData()
                }
            }
        }
    }
    

}

extension PaymentHistoryViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.charges.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! PaymentHistoryTableViewCell
       
        let c = self.charges[indexPath.row]
        cell.chargeDescription.text = c.chargeDescription
        
        //format decimals in amount
        let numberFormatter = NumberFormatter()
        numberFormatter.minimumFractionDigits = 2
        let amt = numberFormatter.string(from: NSNumber(value: c.chargeAmount!/100))
        cell.chargeAmount.text = "$\(amt!)"
        cell.chargeID.text = "#\(c.chargeID!)"
        cell.chargeTransactionDate.text = c.chargeTransactionDate
        
        return cell
    }
    
}
