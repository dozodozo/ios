//
//  BillingViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 20/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit
import Stripe

class BillingViewController: UIViewController {
    
    let billingItems = [
        "", //for top border - using an empty cell
        NSLocalizedString("Top Up Now", comment:""),
        NSLocalizedString("Top Up History", comment:"")
    ]
    
    let lblTitle : UILabel = {
        let lbl = UILabel()
        lbl.text = NSLocalizedString("Credits Remaining", comment: "")
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.preferredFont(forTextStyle: .subheadline)
        return lbl
    }()
    
    let lblCreditRemaining : UILabel = {
        let lbl = UILabel()
        lbl.text = NSLocalizedString("Getting Your Balance...", comment: "")
        lbl.textColor  = ViewConstants.BUTTON_COLOR_DEFAULT
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.preferredFont(forTextStyle: .title1)
        return lbl
    }()
    
    let tableView: UITableView = {
        let tblView = UITableView()
        //tblView.backgroundColor = ViewConstants.TAB_BAR_BACKGROUND_COLOR
        tblView.translatesAutoresizingMaskIntoConstraints = false
        tblView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tblView.tableFooterView = UIView(frame: .zero)
        return tblView
    }()
    
    let activityIndicator: UIActivityIndicatorView = {
        let ai = UIActivityIndicatorView()
        ai.translatesAutoresizingMaskIntoConstraints = false
        ai.color = UIColor.gray
        return ai
    }()
    
    
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //title
        self.title = "Billing"
        
        
        //delegates
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        //add subviews
        self.view.addSubview(lblTitle)
        self.view.addSubview(lblCreditRemaining)
        self.view.addSubview(tableView)
        self.view.addSubview(activityIndicator)
        
        //layout
        setup()
        
        
    }
    
    // MARK: Lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //show tabbar if hidden
        self.tabBarController?.tabBar.isHidden = false
        
        //get account balance
        getAccountBalance()
    }
    
    
    // MARK:  Helpers
    
    private func setup() {
        
        //lbls
        lblTitle.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 40).isActive = true
        lblTitle.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
        lblCreditRemaining.topAnchor.constraint(equalTo: self.lblTitle.bottomAnchor, constant: 40).isActive = true
        lblCreditRemaining.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
        //table view
        self.tableView.topAnchor.constraint(equalTo: lblCreditRemaining.bottomAnchor, constant: 20).isActive = true
        self.tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        self.tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
        //activity indicator
        self.activityIndicator.centerXAnchor.constraint(equalTo: lblCreditRemaining.centerXAnchor).isActive = true
        self.activityIndicator.centerYAnchor.constraint(equalTo: lblCreditRemaining.centerYAnchor).isActive = true
        
        
    }
    
    //show topup options
    func showTopUpOptions(){
        
        let processPaymentViewController = ProcessPaymentViewController()
        
        //controller post duration
        let topUpOptionsAlertController : UIAlertController = {
            let ac = UIAlertController(title: NSLocalizedString("Top Up Amount", comment: ""), message: NSLocalizedString("", comment: ""), preferredStyle: .actionSheet)
            let a1 = UIAlertAction(title: JopaJopaClient.TopUpAmountKeys.twentyDollars, style: .default) { handler in
                processPaymentViewController.topUpAmount = JopaJopaClient.TopUpAmountValues.twentyDollars
                self.navigationController?.pushViewController(processPaymentViewController, animated: true)
            }
            let a2 = UIAlertAction(title: JopaJopaClient.TopUpAmountKeys.fortyDollars, style: .default) { handler in
                processPaymentViewController.topUpAmount = JopaJopaClient.TopUpAmountValues.fortyDollars
                self.navigationController?.pushViewController(processPaymentViewController, animated: true)
            }
            let a3 = UIAlertAction(title: JopaJopaClient.TopUpAmountKeys.sixtyDollars, style: .default) { handler in
                processPaymentViewController.topUpAmount = JopaJopaClient.TopUpAmountValues.sixtyDollars
               self.navigationController?.pushViewController(processPaymentViewController, animated: true)
            }
            let a4 = UIAlertAction(title: JopaJopaClient.TopUpAmountKeys.eightyDollars, style: .default) { handler in
                processPaymentViewController.topUpAmount = JopaJopaClient.TopUpAmountValues.eightyDollars
                self.navigationController?.pushViewController(processPaymentViewController, animated: true)
            }
            let a5 = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
            ac.addAction(a1)
            ac.addAction(a2)
            ac.addAction(a3)
            ac.addAction(a4)
            ac.addAction(a5)
            return ac
        }()
        
        //present top up option
        self.present(topUpOptionsAlertController, animated: true, completion: nil)
        
    }
    
    //func get credit balance
    func getAccountBalance(){
        
        //ui update
        self.activityIndicator.startAnimating()
        self.lblCreditRemaining.isHidden = true
        
        JopaJopaClient.sharedInstance().getAccountBalance() { balance, error in
            
            //ui update
            performUIUpdatesOnMain {
                self.activityIndicator.stopAnimating()
            }
            
            if let error = error {
                //alert controller
                let alertController = JopaJopaClient.sharedInstance().getAlertControllerForErrors(error, JopaJopaClient.JopaJopaTask.Get_Account_Balance)
                
                // if error is authentication failure, log user out
                if (error.userInfo["JopaJopaErrorType"] as! String == JopaJopaClient.JopaJopaErrorType.AUTHENTICATION_FAILURE){
                    JopaJopaClient.sharedInstance().logoutUser()
                    let logoutAction = UIAlertAction(title: "Close", style: .default){ handler in
                        self.present(MainTabViewController(), animated: true, completion: nil)
                    }
                    alertController.addAction(logoutAction)
                }
                performUIUpdatesOnMain {
                    self.present(alertController, animated: true, completion: nil)
                }
            } else {
                
                //format decimals in amount
                let numberFormatter = NumberFormatter()
                numberFormatter.minimumFractionDigits = 2
                let amount = numberFormatter.string(from: NSNumber(value: balance!))
                performUIUpdatesOnMain {
                    self.lblCreditRemaining.isHidden = false
                    self.lblCreditRemaining.text = "$\(amount!)"
                }
            }
        }
    }
    
    
}

// MARK: Extension TableView

extension BillingViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return billingItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //for top border - using an empty cell
        if (indexPath.row == 0){
            let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.textLabel?.text = self.billingItems[indexPath.row]
            return cell
        } else {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
            cell.textLabel?.text = self.billingItems[indexPath.row]
            //cell.backgroundColor = ViewConstants.TAB_BAR_BACKGROUND_COLOR
            cell.accessoryType = .disclosureIndicator
            return cell
        }
        
    }
    
    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch (indexPath.row) {
        case 0:
            break
        case 1:
            //check if card saved
            showTopUpOptions()
        case 2: self.navigationController?.pushViewController(PaymentHistoryViewController(), animated: true)
        default: break
        }
        
    }
}

