//
//  CardDetailViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 6/9/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit
import Stripe

class ProcessPaymentViewController: UIViewController {
    
    //
    var topUpAmount: Double = 0
    
    //topUpAmount label
    let topUpAmountLabel : UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.preferredFont(forTextStyle: .title1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    // post image
    let imgViewCard: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "stripeCard")
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        return imageView
    }()
    
    let paymentCardTextField: STPPaymentCardTextField  = {
        let pct = STPPaymentCardTextField()
        pct.numberPlaceholder = "4444333322221111"
        pct.translatesAutoresizingMaskIntoConstraints = false
        return pct
    }()
    
    let activityIndicator : UIActivityIndicatorView = {
        let ai = UIActivityIndicatorView()
        ai.activityIndicatorViewStyle = .gray
        ai.alpha = 0.5
        ai.color = ViewConstants.BUTTON_COLOR_DEFAULT
        ai.translatesAutoresizingMaskIntoConstraints = false
        return ai
    }()
    
    
    let dimView : UIView = {
        let view = UIView()
        view.backgroundColor = ViewConstants.BACKGROUND_COLOR_DIM
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    
    
    // MARK: Lifecycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //hide tab bar
        self.tabBarController?.tabBar.isHidden = true
        
        self.title = NSLocalizedString("Top Up Now ", comment: "")
        
        //format top up mount to show two decimal places
        let numberFormatter = NumberFormatter()
        numberFormatter.minimumFractionDigits = 2
        topUpAmountLabel.text = "$" + numberFormatter.string(from: NSNumber(value: topUpAmount))!
        
        //show activity indicator
        activityIndicator.startAnimating()

        //bar button items
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Pay", comment: ""), style: .done, target: self, action: #selector(topUp))
         self.navigationItem.rightBarButtonItem?.isEnabled = false
        
        // Setup payment card text field
        paymentCardTextField.delegate = self
        
        
        // Add payment card text field to view
        
        view.addSubview(imgViewCard)
        view.addSubview(topUpAmountLabel)
        view.addSubview(paymentCardTextField)
        view.addSubview(dimView)
        
        
        // lay out
        
        imgViewCard.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant:20).isActive = true
        imgViewCard.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        let widthImage = view.frame.width -  6 * (view.frame.width/12)
        imgViewCard.widthAnchor.constraint(equalToConstant: widthImage).isActive = true
        
        let aspectRatio = (imgViewCard.image?.size.width)! / (imgViewCard.image?.size.height)!
        imgViewCard.heightAnchor.constraint(equalToConstant: widthImage / aspectRatio).isActive = true
        
        //topup label
        topUpAmountLabel.topAnchor.constraint(equalTo: imgViewCard.bottomAnchor, constant: 20).isActive  = true
        topUpAmountLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
        
        //card info
        paymentCardTextField.topAnchor.constraint(equalTo: topUpAmountLabel.bottomAnchor, constant: 20).isActive  = true
        paymentCardTextField.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: -5).isActive = true
        paymentCardTextField.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 5).isActive = true
        
        //dim view
        dimView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        dimView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        dimView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    override func viewWillLayoutSubviews() {
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK : Action
    
    
    //process top up
    @objc func topUp(){
        
        //update appearance
        updateAppearanceToppingUp()
        
        //get card params
        let cardParams = STPCardParams()
        cardParams.number = self.paymentCardTextField.cardNumber
        cardParams.expMonth = self.paymentCardTextField.expirationMonth
        cardParams.expYear = self.paymentCardTextField.expirationYear
        cardParams.cvc = self.paymentCardTextField.cvc
        
        let sourceParams = STPSourceParams.cardParams(withCard: cardParams)
        STPAPIClient.shared().createSource(with: sourceParams) { (source, error) in
            
            if let s = source, s.flow == .none && s.status == .chargeable {
                //self.createBackendChargeWithSourceID(s.stripeID)
                let countryCode = (s.cardDetails?.country)!
                //send sourceID, countryCode and amount
                let jsonBody = "{\"sourceID\":\"\(s.stripeID)\",\"rechargeAmount\":\"\(self.topUpAmount)\",\"countryCode\":\"\(countryCode)\"}"
                
                //call convenience method, customer created in backend only if new
                JopaJopaClient.sharedInstance().createStripeCustomerAndCharge(jsonBody: jsonBody) { customerID, error in
                    
                        //update apperance
                        performUIUpdatesOnMain {
                            print("updating appearance")
                            self.updateAppearanceTopUpfinished()
                        }
                    
                        //check errors
                        if let error = error {
                            //alert controller
                            let alertController = JopaJopaClient.sharedInstance().getAlertControllerForErrors(error, JopaJopaClient.JopaJopaTask.Add_Credit)
                            
                            // if error is authentication failure, log user out
                            if (error.userInfo["JopaJopaErrorType"] as! String == JopaJopaClient.JopaJopaErrorType.AUTHENTICATION_FAILURE){
                                JopaJopaClient.sharedInstance().logoutUser()
                                let logoutAction = UIAlertAction(title: "Close", style: .default){ handler in
                                    self.present(MainTabViewController(), animated: true, completion: nil)
                                }
                                alertController.addAction(logoutAction)
                            }
                            performUIUpdatesOnMain {
                                self.present(alertController, animated: true, completion: nil)
                            }
                        } else {
                            //top up successful
                            performUIUpdatesOnMain {
                                //show topup success message and exit
                                self.updateAppearanceTopUpSuccessful()
                            }
                        }
                    
                    }
            }
            
        }
        
    }
    
    
    //appearance for topping up
    func updateAppearanceToppingUp(){
        
        //show dim view
        self.dimView.isHidden = false
        
        //show activity indicator
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activityIndicator)
        
        //disable card text field
        self.paymentCardTextField.isEnabled = false
    }
    
    //appearance for top up finished
    func updateAppearanceTopUpfinished(){
        
        //hide dim view
        self.dimView.isHidden = true
        
        //show activity indicator
        self.navigationItem.rightBarButtonItem  = UIBarButtonItem(title: NSLocalizedString("Pay", comment: ""), style: .done, target: self, action: #selector(topUp))
        
        //disable card text field
        self.paymentCardTextField.isEnabled = true
    }
    
    //topup successful message
    func updateAppearanceTopUpSuccessful(){
        let alertController = UIAlertController(title: NSLocalizedString("Top Up Successful", comment: ""), message: "", preferredStyle: .alert)
        let a1 = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default){handler in
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(a1)
        
        self.present(alertController, animated: true, completion: nil)
    }

}

// MARK: Extension

extension ProcessPaymentViewController: STPPaymentCardTextFieldDelegate {
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        if (textField.isValid) {
             self.navigationItem.rightBarButtonItem?.isEnabled = true
        } else {
             self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
    
}
