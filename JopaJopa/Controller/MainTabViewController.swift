//
//  MainTabViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 11/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit

class MainTabViewController: UITabBarController {

    override func viewDidLoad() {
        print("MainTabView Did Load")
        self.title = "MainTabViewController"
        super.viewDidLoad()
        
        self.tabBar.backgroundColor = ViewConstants.TAB_BAR_BACKGROUND_COLOR
        self.tabBar.layer.borderColor = UIColor.lightGray.cgColor
        
        //tab items and view controllers
        let homeViewController = UINavigationController(rootViewController: HomeViewController())
        let postsViewController = UINavigationController(rootViewController: PostsViewController())
        let placesViewController = UINavigationController(rootViewController: PlacesViewController())
        let billingViewController = UINavigationController(rootViewController: BillingViewController())
        let accountViewController = UINavigationController(rootViewController: AccountViewController())
        
//        let homeViewController =  HomeViewController()
//        let testViewController =  TestViewController()
        

        homeViewController.tabBarItem = UITabBarItem(title: "Home", image: #imageLiteral(resourceName: "home"), tag: 0)
        
        postsViewController.tabBarItem = UITabBarItem(title: "Posts", image: #imageLiteral(resourceName: "poster"), tag: 1)
        placesViewController.tabBarItem = UITabBarItem(title: "Places", image: #imageLiteral(resourceName: "places"), tag: 2)
        billingViewController.tabBarItem = UITabBarItem(title: "Billing", image: #imageLiteral(resourceName: "creditCard"), tag: 3)
        accountViewController.tabBarItem = UITabBarItem(title: "Account", image: #imageLiteral(resourceName: "head"), tag: 4)

        let controllers = [homeViewController, postsViewController, placesViewController, billingViewController, accountViewController]
        self.viewControllers = controllers
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("MainTabView Did Appear")

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("MainTabView Did Disappear")
    }
    

}
