//
//  AddPlaceViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 2/7/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit
import GooglePlaces
import PromiseKit

// MARK : AddPlaceViewControlDelegate

protocol AddPlaceViewControllerDelegate {
    func didFinishAddingPlace(_ place: JopaJopaPlace)
}

class AddPlaceViewController: UIViewController {
    
    // MARK: Properties
    
    //google places
    let gmsAutoCompleteController = GMSAutocompleteViewController()
    var gmsPlace : GMSPlace!
    
    //JopaJopaPlace
    var place: JopaJopaPlace!
    
    //booleans for enabling save button
    var isPlaceImageSelected = false
    var isPlaceNameSet = false
    var isPlaceAddressSet = false
    var isPlacePhoneNoSet = false
    
    //delegate
    var delegate: AddPlaceViewControllerDelegate?
    
    let imgViewPlace: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "addImageGray"))
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let txtPlaceName: InputTextField = {
        let txtField = InputTextField()
        txtField.placeholder = NSLocalizedString("Place Name", comment:"")
        return txtField
    }()
    
    let txtAddress: InputTextField = {
        let txtField = InputTextField()
        //txtField.font = txtField.font?.withSize(ViewConstants.VERY_SMALL_FONT)
        txtField.placeholder = NSLocalizedString("Address", comment:"")
        return txtField
    }()
    
    let txtPhoneNo: InputTextField = {
        let txtField = InputTextField()
        txtField.keyboardType = UIKeyboardType.phonePad
        txtField.placeholder = NSLocalizedString("Phone Number", comment:"")
        return txtField
    }()
    
    let btnSave: UIBarButtonItem = {
        let btn = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.plain, target: self, action: #selector(btnAddPlace))
        btn.isEnabled = false
        return btn
    }()
    
    // MARK: Activity Indicator
    let dimView : UIView = {
        let view = UIView()
        view.backgroundColor = ViewConstants.BACKGROUND_COLOR_DIM
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let activityIndicator : UIActivityIndicatorView = {
        let ai = UIActivityIndicatorView()
        ai.alpha = 0.5
        ai.color = ViewConstants.BUTTON_COLOR_DEFAULT
        return ai
    }()
    
    
    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //title
        self.title = "Add Place"
        
        //hide tab bar
        self.tabBarController?.tabBar.isHidden = true
        
        //right bar button
        self.navigationItem.rightBarButtonItem = btnSave
        
        //start activity indicator - it is only visible when user clicks save
        activityIndicator.startAnimating()
        
        //initialize place
        place = JopaJopaPlace()
        
        //assing delegates
        gmsAutoCompleteController.delegate = self
        txtPlaceName.delegate = self
        txtAddress.delegate = self
        txtPhoneNo.delegate = self
        
        //tap gesture
        let tapGestureRecoginizer = UITapGestureRecognizer(target: self, action: #selector(selectImage))
        imgViewPlace.addGestureRecognizer(tapGestureRecoginizer)
        imgViewPlace.isUserInteractionEnabled = true

        // add views
        self.view.addSubview(imgViewPlace)
        self.view.addSubview(txtPlaceName)
        self.view.addSubview(txtAddress)
        self.view.addSubview(txtPhoneNo)
        
        self.view.addSubview(dimView)
        self.view.addSubview(activityIndicator)
        
        //layout
        setupLayout()
    }
    
    override func viewDidLayoutSubviews() {
        
        //add bottom borders, done after adding views
        //txtDisplayName.addBottomBorder()
        txtPlaceName.addBottomBorder()
        txtAddress.addBottomBorder()
        txtPhoneNo.addBottomBorder()
    }

    
    // MARK: UI Layout
    
    func setupLayout(){
        
        imgViewPlace.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        imgViewPlace.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imgViewPlace.widthAnchor.constraint(equalToConstant: view.frame.width/4).isActive = true
        imgViewPlace.heightAnchor.constraint(equalTo: imgViewPlace.widthAnchor).isActive = true

        imgViewPlace.layer.cornerRadius = view.frame.width/8
        imgViewPlace.clipsToBounds = true
        
        txtPlaceName.topAnchor.constraint(equalTo: imgViewPlace.bottomAnchor, constant:10).isActive = true
        txtPlaceName.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        txtPlaceName.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        txtAddress.topAnchor.constraint(equalTo: txtPlaceName.bottomAnchor, constant:10).isActive = true
        txtAddress.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        txtAddress.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        txtPhoneNo.topAnchor.constraint(equalTo: txtAddress.bottomAnchor, constant:10).isActive = true
        txtPhoneNo.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        txtPhoneNo.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        dimView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        dimView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        dimView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
    }
    
     // MARK: Helpers
    
    //update appearance adding place
    func updateAppearanceAddingPlace(_ sender: Any?){
        
        //hide keyboard
        hideKeyboard(sender!)
        //disable button
        self.btnSave.isEnabled = false
        //show activity view
        self.dimView.isHidden = false
        //show activity indicator
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activityIndicator)
        
    }
    
    //update apperance finsihed adding - could result in error
    func updateAppearanceAfterAddResult(_ sender: Any?){
        self.dimView.isHidden = true
        self.navigationItem.rightBarButtonItem  = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.btnAddPlace))
    }
    
    //helper - enable save button
    func enableSaveButton(){
        if (isPlaceNameSet && isPlaceAddressSet && isPlacePhoneNoSet && isPlaceImageSelected){
            btnSave.isEnabled = true
        } else {
            btnSave.isEnabled = false
        }
    }
    
    // MARK: Actions
    
    //hide keyboard
    @objc private func hideKeyboard(_ sender: Any){
        self.view.endEditing(true)
    }
    
    //select image from camera or library
    @objc func selectImage(_sender: UIImageView){
        let imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        let alertControllerTitle = NSLocalizedString("Select Source", comment: "")
        let cameraTitle = NSLocalizedString("Camera", comment: "")
        let libraryTitle = NSLocalizedString("Photo Library", comment: "")
        
        //show options for camera or library
        let alertController = UIAlertController(title: alertControllerTitle, message: "", preferredStyle: .actionSheet)
        
        //camera
        let camera = UIAlertAction(title: cameraTitle, style: .default){handler in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                imagePicker.sourceType = .camera
                imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
                self.present(imagePicker, animated: true, completion: nil)
            } else {
               let error = NSError(domain: "TaskForSelectImage", code: 1, userInfo : ["JopaJopaErrorType" : JopaJopaClient.JopaJopaErrorType.MEDIA_SOURCE_NOT_AVAILABLE])
                self.present(JopaJopaClient.sharedInstance().getAlertControllerForErrors(error, ""), animated: true, completion: nil)
            }
        }
        
        //photo library
        let library = UIAlertAction(title: libraryTitle, style: .default){ handler in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                imagePicker.sourceType = .photoLibrary
                imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
                self.present(imagePicker, animated: true, completion: nil)
            } else {
                let error = NSError(domain: "TaskForSelect", code: 1, userInfo : ["JopaJopaErrorType" : JopaJopaClient.JopaJopaErrorType.MEDIA_SOURCE_NOT_AVAILABLE])
                self.present(JopaJopaClient.sharedInstance().getAlertControllerForErrors(error, ""), animated: true, completion: nil)
            }
        }
        
        //cancel
        let cancel = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        
        //add actions
        alertController.addAction(camera)
        alertController.addAction(library)
        alertController.addAction(cancel)
        
        //show imagepicker option
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    //add place
    @objc func btnAddPlace(_ sender: UIBarButtonItem){
        
        //update appearance
        updateAppearanceAddingPlace(sender)
        
        //get country from gmsPlace
        var country: String?
        for component in (gmsPlace.addressComponents)! {
            if component.type == "country" {
                country = component.name
            } 
        }
        
        //create location
        let location = JopaJopaPlace.Location(address: gmsPlace.formattedAddress!, country: country)
        
        //create JopaJopaPlace
        place = JopaJopaPlace(placeName: txtPlaceName.text, placeLocation: location, placeGeoLocation: [gmsPlace.coordinate.longitude, gmsPlace.coordinate.latitude], placePhoneNo: txtPhoneNo.text)
        
        // encode json
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let data = try! encoder.encode(place)
        
        //json to string
        let placeString = String(data: data, encoding: .utf8)!
        
        // add place with expiry (ttl) and get signed url
        
        var savedPlaceID : String! //id of place returned by db
        var placeImageURL: String?
        
        //call convenience method to save place details
        firstly {
             JopaJopaClient.sharedInstance().addPlace(jsonBody: placeString)
        }
        // upload image to digital ocean spaces
        .then { (id, url) -> Promise<Bool> in
            
            //extract image url from spaces image authorized url
            let urlString = "\(url)"
            let indexParams = urlString.index(of: "?")
            placeImageURL = String(urlString[..<indexParams!])
            
            //image data
            savedPlaceID = id
            let placeImage = self.imgViewPlace.image?.resized(toWidth: 400)
            
            let image_data = UIImageJPEGRepresentation(placeImage!, 0.5)!
            return JopaJopaClient.sharedInstance().uploadImageToSpaces(signedURL: url, data: image_data)
        }
        //if image upload succsseful, remove data expiry from place
        .then { (uploadSucceded) -> Promise<Bool> in
            //path is used for patch request
            let jsonString = "{\"path\":\"\(JopaJopaClient.JSONReponseKeys.DateExpiry)\", \"value\":\"\", \"op\": \"remove\"}"
            return JopaJopaClient.sharedInstance().placeRemoveDateExpiry(jsonBody: jsonString, placeID: savedPlaceID)
        }
        //if no error, show success message to user
        .done{result in
            
            //update appearance
            performUIUpdatesOnMain {
                self.updateAppearanceAfterAddResult(sender)
            }
            
            //send saved place and image
            self.place.placeID = savedPlaceID
            self.place.placeImageURL = placeImageURL
            
            self.navigationController?.popViewController(animated: true)
            self.delegate?.didFinishAddingPlace(self.place)
            
        }
        .catch{ error in
            //update appearance
            performUIUpdatesOnMain {
                self.updateAppearanceAfterAddResult(sender)
            }
            
            //cast error to NSError
            let nsError = error as NSError
            
            //show activity view
            self.dimView.isHidden = true
            self.activityIndicator.stopAnimating()
            
            //alert controller
            let alertController = JopaJopaClient.sharedInstance().getAlertControllerForErrors(nsError, JopaJopaClient.JopaJopaTask.Add_Place)
            
            // if error is authentication failure, log user out
            if (nsError.userInfo["JopaJopaErrorType"] as! String == JopaJopaClient.JopaJopaErrorType.AUTHENTICATION_FAILURE){
                JopaJopaClient.sharedInstance().logoutUser()
                let logoutAction = UIAlertAction(title: "Close", style: .default){ handler in
                    self.present(MainTabViewController(), animated: true, completion: nil)
                }
                alertController.addAction(logoutAction)
            }
            performUIUpdatesOnMain {
                self.present(alertController, animated: true, completion: nil)
            }
            
        }
        
        
    }

}

// MARK: ImagePickerControllerDelegate

extension AddPlaceViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //image picked
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        //get chosen image
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        //dismiss picker
        dismiss(animated: true, completion: nil)
        
        //instantiate ImageCropperViewController
        let imageCropperViewController = ImageCropperViewController(image: chosenImage)
        
        //make this view controller the delegate
        imageCropperViewController.delegate = self
        
        //presnet ImageCropperViewController
        self.present(imageCropperViewController, animated: true, completion: nil)
        
        //enable save button
        isPlaceImageSelected = true
        
        //check whether to enable save btn
        enableSaveButton()
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}

// MARK: Image Cropper Delegate

extension AddPlaceViewController: ImageCropperViewControllerDelegate {
    
    // image finished cropping, update image view
    func imageDidFinishCropping(imageCropperViewController: ImageCropperViewController) {
        
        //for debugging - print cropped image size
        //print("Cropped Image  size: \(imageCropperViewController.croppedImage.size)")
        
        //update imageView
         imgViewPlace.image = imageCropperViewController.croppedImage
        
    }
    
}

// MARK: Text Field Delegates

extension AddPlaceViewController: UITextFieldDelegate {
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if (textField == txtAddress) {
            self.present(gmsAutoCompleteController, animated: true, completion: nil)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // get email and password from text
        var updatedPlaceName = txtPlaceName.text!
        var updatedAddress = txtAddress.text!
        var updatedPhoneNo = txtPhoneNo.text!
        
        if let placeName = txtPlaceName.text,
            let textRange = Range(range, in: placeName) {
            if (textField == txtPlaceName){
                updatedPlaceName = placeName.replacingCharacters(in: textRange,
                                                                 with: string)
            }
        }
        
        if let address = txtAddress.text,
            let textRange = Range(range, in: address) {
            if (textField == txtAddress){
                updatedAddress = address.replacingCharacters(in: textRange,
                                                             with: string)
            }
        }
        
        if let phoneNo = txtPhoneNo.text,
            let textRange = Range(range, in: phoneNo) {
            if (textField == txtPhoneNo){
                updatedPhoneNo = phoneNo.replacingCharacters(in: textRange,
                                                             with: string)
            }
        }
        
        
        //for enabling save button
        
        if(!updatedPlaceName.isEmpty){
            isPlaceNameSet = true
        } else {
            isPlaceNameSet = false
        }
        
        if(!updatedAddress.isEmpty){
            isPlaceAddressSet = true
        } else {
            isPlaceAddressSet = false
        }
        
        if(!updatedPhoneNo.isEmpty){
            isPlacePhoneNoSet = true
        } else {
            isPlacePhoneNoSet = false
        }
        
        //check whether to enable save btn
        enableSaveButton()
        
        
        return true
    }
}

// MARK: Goole Place AutoComplete Delegate

extension AddPlaceViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        //save value to gmsPlace property
        gmsPlace = place
        
        //update address field
        txtAddress.text = place.formattedAddress!
        
        dismiss(animated: true, completion: nil)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
         dismiss(animated: true, completion: nil)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
}


