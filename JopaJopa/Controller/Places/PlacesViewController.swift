//
//  PlacesViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 20/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit

class PlacesViewController: UIViewController {
    
    // MARK: Properties
    
    //store list of places returned from db
    var places: [JopaJopaPlace] = [JopaJopaPlace]()
    
    //refresh control boolean
    var isRefreshControlActive = false
 
    //view to show if places is empty, consists of label and image view
    let nothingView: UIView = {
        let tempView = UIView()
        tempView.translatesAutoresizingMaskIntoConstraints = false
        return tempView
    }()
    
    let label: UILabel = {
        let lbl = UILabel()
        lbl.text = NSLocalizedString("You have no places!", comment: "")
        lbl.textAlignment =  NSTextAlignment.center
        lbl.font = UIFont.preferredFont(forTextStyle: .body)
        lbl.numberOfLines = 0
        lbl.textColor = ViewConstants.LIGHT_TEXT_COLOR
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let imageView: UIImageView = {
        let imgView = UIImageView(image: #imageLiteral(resourceName: "places"))
        imgView.translatesAutoresizingMaskIntoConstraints = false
        return imgView
    }()
    
    //table view for places
    let tableView: UITableView = {
        let tblView = UITableView()
        tblView.translatesAutoresizingMaskIntoConstraints = false
        tblView.register(PlacesTableViewCell.self, forCellReuseIdentifier: "placeCell")
        return tblView
    }()
    
    //refresh control
    let refreshControl: UIRefreshControl = {
        let rc = UIRefreshControl()
        rc.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        return rc
    }()
    
    let dimView : UIView = {
        let view = UIView()
        view.backgroundColor = ViewConstants.BACKGROUND_COLOR_DIM
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let activityIndicator: UIActivityIndicatorView = {
        let actInd = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        actInd.translatesAutoresizingMaskIntoConstraints = false
        return actInd
    }()
    
    //message box when place deleted or updated successfully
    let msgLabelSucccess : UILabel = {
        let lbl = UILabel()
        lbl.text = "Hello"
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textAlignment = NSTextAlignment.center
        lbl.backgroundColor = UIColor.black
        lbl.textColor = UIColor.white
        lbl.alpha = 0
        return lbl
    }()
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //title
        self.title = "Places"
         
        //data source and delegate
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        //add button to add place
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(showAddPlace))
        
        //add sub views
        self.view.addSubview(nothingView)
        self.nothingView.addSubview(label)
        self.nothingView.addSubview(imageView)
        
        //for dynamic table height
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension //for dynamic height
        
        self.view.addSubview(tableView)
        
        //refresh view
        if #available(iOS 10.0, *) {
            self.tableView.refreshControl = refreshControl
        } else {
            self.tableView.addSubview(refreshControl)
        }
        
        //activity view
        self.view.addSubview(dimView)
        self.dimView.addSubview(activityIndicator)
        
        //msg
        self.view.addSubview(msgLabelSucccess)
        
        //setup
        setupLayout()
        
        //get places
        getPlaces()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //show tabbar if hidden
        self.tabBarController?.tabBar.isHidden = false

        //get places
        if (!self.nothingView.isHidden){
            getPlaces()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("in view did appear")
    }
    
    // MARK: UI Layout
    
    func setupLayout(){
        
        //imgView
        self.imageView.centerYAnchor.constraint(equalTo: self.nothingView.centerYAnchor, constant: 10).isActive = true
        self.imageView.centerXAnchor.constraint(equalTo: self.nothingView.centerXAnchor).isActive = true
        
        //lblMessage
        self.label.leadingAnchor.constraint(equalTo: self.nothingView.leadingAnchor, constant: 10).isActive = true
        self.label.trailingAnchor.constraint(equalTo: self.nothingView.trailingAnchor, constant: -10).isActive = true
        self.label.topAnchor.constraint(equalTo: self.imageView.bottomAnchor, constant: 5).isActive = true
        
        //table view
        self.tableView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        //nothing view
        self.nothingView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.nothingView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        self.nothingView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.nothingView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        
        //dim view
        dimView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        dimView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        dimView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        activityIndicator.centerYAnchor.constraint(equalTo: self.dimView.centerYAnchor, constant:40).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: self.dimView.centerXAnchor).isActive = true
        
        //msg label
        
        msgLabelSucccess.heightAnchor.constraint(equalToConstant: (self.tabBarController?.tabBar.frame.height)!).isActive = true
        msgLabelSucccess.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -(self.tabBarController?.tabBar.frame.height)!).isActive = true
        msgLabelSucccess.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        msgLabelSucccess.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        
    }
    
    // MARK: Helpers
    
    //update appearane getting places
    func updateAppearanceGettingPlaces(){
        //show activity indicator
        self.dimView.isHidden = false
        self.activityIndicator.startAnimating()
    }
    
    //update appearane got places
    func updateAppearanceGotPlaces(){
        self.activityIndicator.stopAnimating()
        self.dimView.isHidden = true
    }
    
    // MARK: Actions
    
    //table view refresh
    @objc func refreshList(_ sender: UIRefreshControl){
        self.isRefreshControlActive = true
        
        //get place
        getPlaces()
        
        //reset refresh Control
        self.isRefreshControlActive = false
        sender.endRefreshing()
        
    }
    
    //get user places
    func getPlaces() -> Void {
        
        //update appearance only if refresh control not used, avoid double spinners
        if (!self.isRefreshControlActive) {
            updateAppearanceGettingPlaces()
        }
        
        //call convenience method to get places list
        JopaJopaClient.sharedInstance().getPlacesList {places, error in
            
            //update appearance
            performUIUpdatesOnMain {
                self.updateAppearanceGotPlaces()
            }
            
            //check if request was successful
            if let error = error {
               
                //alert controller
                let alertController = JopaJopaClient.sharedInstance().getAlertControllerForErrors(error, JopaJopaClient.JopaJopaTask.Get_Places)
                
                // if error is authentication failure, log user out
                if (error.userInfo["JopaJopaErrorType"] as! String == JopaJopaClient.JopaJopaErrorType.AUTHENTICATION_FAILURE){
                    JopaJopaClient.sharedInstance().logoutUser()
                    let logoutAction = UIAlertAction(title: "Close", style: .default){ handler in
                        self.present(MainTabViewController(), animated: true, completion: nil)
                    }
                    alertController.addAction(logoutAction)
                }
                performUIUpdatesOnMain {
                    self.present(alertController, animated: true, completion: nil)
                }
                
            } else {
                
                //save places
                self.places = places!
                
                if (places!.isEmpty){
                    performUIUpdatesOnMain {
                        self.tableView.reloadData()
                        self.tableView.isHidden = true
                        self.nothingView.isHidden = false
                    }
                } else {
                    performUIUpdatesOnMain {
                        self.tableView.reloadData()
                        self.tableView.isHidden = false
                        self.nothingView.isHidden = true
                    }
                }
            }

        }
    }
    
    //add place
    @objc func showAddPlace(_sender: UIBarButtonItem){
        let addPlaceViewController = AddPlaceViewController()
        addPlaceViewController.delegate = self
        self.navigationController?.pushViewController(addPlaceViewController, animated: true)
    }
    
}

// MARK: TableView extension

extension PlacesViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return !(self.places.isEmpty) ? self.places.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "placeCell", for: indexPath) as! PlacesTableViewCell
        let place = self.places[indexPath.row]
        
        /* Set cell defaults */
        cell.titleLabel.text = place.placeName
        cell.detailLabel.text = place.placeLocation!.address
        
        //get image for cell
        let url = URL(string: place.placeImageURL!)
        JopaJopaClient.sharedInstance().taskForGETImageFromSpaces(url: url!, useCachedImage: true){ imageData, error in
            if (error != nil) {
                performUIUpdatesOnMain {
                    cell.imageViewCell.image = UIImage(named: "defaultImage")
                }
                
            } else
            {
                if let imageData = imageData {
                    performUIUpdatesOnMain {
                        cell.imageViewCell.image = UIImage(data: imageData)
                    }
                }
            }
        }
        
        return cell
    }
    
    
    //insert / delete row
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        //delete
        if editingStyle == UITableViewCellEditingStyle.delete {
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        }
        
        //insert
        if editingStyle == UITableViewCellEditingStyle.insert{
            tableView.insertRows(at: [indexPath], with: UITableViewRowAnimation.bottom)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        let placeDetailViewController = PlaceDetailViewController()
        let cell = tableView.cellForRow(at: indexPath) as! PlacesTableViewCell
        placeDetailViewController.delegate = self
        placeDetailViewController.indexPath = indexPath
        placeDetailViewController.place = places[indexPath.row]
        placeDetailViewController.imgViewPlace.image = cell.imageViewCell.image
        self.navigationController?.pushViewController(placeDetailViewController, animated: true)
    }
    
}

// MARK : Extension AddPlaceViewController


extension PlacesViewController : AddPlaceViewControllerDelegate{
    
    func didFinishAddingPlace(_ place: JopaJopaPlace) {
        
        //insert new place at index 0
        places.insert(place, at: 0)
        
        let indexPath = IndexPath(row: 0, section: 0)
        self.tableView(tableView, commit: .insert, forRowAt: indexPath)
        
        //scroll to top
        self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: true)
        
        //show message
        self.msgLabelSucccess.alpha = 1
        self.msgLabelSucccess.text = NSLocalizedString("Place Added", comment: "")
        UIView.animate(withDuration: 3){
            self.msgLabelSucccess.alpha = 0
        }
    }
    
}

// MARK : Extension PlaceDetailViewController

extension PlacesViewController : PlaceDetailViewControllerDelegate{
    
    func didFinishUpdatingPlace(_ placeDetailViewController: PlaceDetailViewController) {
        
        //update place
        let indexPath = placeDetailViewController.indexPath
        let index = (indexPath?.row)!
        places[index].placeName = placeDetailViewController.place.placeName
        places[index].placeLocation = placeDetailViewController.place.placeLocation
        
        //update cell
        let cell = self.tableView.cellForRow(at: indexPath!) as! PlacesTableViewCell
        cell.imageViewCell.image = placeDetailViewController.imgViewPlace.image
        cell.titleLabel.text = placeDetailViewController.place.placeName
        cell.detailLabel.text = placeDetailViewController.place.placeLocation!.address
        
        //show message
        self.msgLabelSucccess.alpha = 1
        self.msgLabelSucccess.text = NSLocalizedString("Place Updated", comment: "")
        UIView.animate(withDuration: 3){
            self.msgLabelSucccess.alpha = 0
        }

    }
    
    func didFinishDeletingPlace(_ indexPath: IndexPath) {
        
        //remove place rom array
        let index = indexPath.row
        self.places.remove(at: index)
        
        //update table view ui
        self.tableView(tableView, commit: .delete, forRowAt: indexPath)
        
        //show msg
        self.msgLabelSucccess.alpha = 1
        self.msgLabelSucccess.text = NSLocalizedString("Place Deleted", comment: "")
        UIView.animate(withDuration: 3){
            self.msgLabelSucccess.alpha = 0
        }
    }
    
    
}


