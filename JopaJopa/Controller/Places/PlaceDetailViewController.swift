//
//  PlaceDetailViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 2/7/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit
import GooglePlaces
import PromiseKit

// MARK: PlaceDetailViewControllerDelegate

protocol PlaceDetailViewControllerDelegate {
    
    func didFinishUpdatingPlace(_ placeDetailViewController: PlaceDetailViewController)
    
    func didFinishDeletingPlace(_ indexPath: IndexPath)
}

class PlaceDetailViewController: UIViewController {
    
    // MARK: Properties
    
    //google places
    let gmsAutoCompleteController = GMSAutocompleteViewController()
    var gmsPlace : GMSPlace?
    
    //JopaJopaPlace
    var place: JopaJopaPlace!
    var indexPath : IndexPath? //index path of current place in placesviewcontroller
    
    //delegate
    var delegate: PlacesViewController?
    
    //booleans for enabling save button
    var isPlaceImageSelected = true
    var isPlaceNameSet = true
    var isPlaceAddressSet = true
    var isPlacePhoneNoSet = true
    
    let imgViewPlace: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let txtPlaceName: InputTextField = {
        let txtField = InputTextField()
        txtField.placeholder = NSLocalizedString("Place Name", comment:"")
        return txtField
    }()
    
    let txtAddress: InputTextField = {
        let txtField = InputTextField()
        //txtField.font = txtField.font?.withSize(ViewConstants.VERY_SMALL_FONT)
        txtField.placeholder = NSLocalizedString("Address", comment:"")
        return txtField
    }()
    
    let txtPhoneNo: InputTextField = {
        let txtField = InputTextField()
        txtField.keyboardType = UIKeyboardType.phonePad
        txtField.placeholder = NSLocalizedString("Phone Number", comment:"")
        return txtField
    }()
    
    let btnDeletePlace: UIButton = {
        let btn = UIButton()
        btn.setTitle(NSLocalizedString("Delete", comment:""), for: .normal)
        btn.setTitleColor(UIColor.red, for: .normal)
        btn.backgroundColor = ViewConstants.TAB_BAR_BACKGROUND_COLOR
        btn.layer.borderWidth = 0.5
        btn.layer.borderColor = UIColor.lightGray.cgColor
        btn.addTarget(self, action: #selector(deletePlace), for: UIControlEvents.touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let dimView : UIView = {
        let view = UIView()
        view.backgroundColor = ViewConstants.BACKGROUND_COLOR_DIM
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let activityIndicator : UIActivityIndicatorView = {
        let ai = UIActivityIndicatorView()
        ai.activityIndicatorViewStyle = .gray
        ai.alpha = 0.5
        ai.color = ViewConstants.BUTTON_COLOR_DEFAULT
        ai.translatesAutoresizingMaskIntoConstraints = false
        return ai
    }()
    
    //activity indicator centred on delete button
    let activityIndicatorDeleteButton : UIActivityIndicatorView = {
        let ai = UIActivityIndicatorView()
        ai.color = ViewConstants.BUTTON_COLOR_DEFAULT
        ai.translatesAutoresizingMaskIntoConstraints = false
        return ai
    }()
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //title
        self.title = ""
        
        //initialise values
        txtPlaceName.text  = place.placeName!
        txtAddress.text = place.placeLocation?.address!
        txtPhoneNo.text = place.placePhoneNo!
        
        //hide tab bar
        self.tabBarController?.tabBar.isHidden = true
        
        //right bar button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.plain, target: self, action: #selector(btnUpdate))
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        
        //assing delegates
        gmsAutoCompleteController.delegate = self
        txtPlaceName.delegate = self
        txtAddress.delegate = self
        txtPhoneNo.delegate = self
        
        //tap gesture
        let tapGestureRecoginizer = UITapGestureRecognizer(target: self, action: #selector(selectImage))
        imgViewPlace.addGestureRecognizer(tapGestureRecoginizer)
        imgViewPlace.isUserInteractionEnabled = true
        
        
        
        // add views
        self.view.addSubview(imgViewPlace)
        self.view.addSubview(txtPlaceName)
        self.view.addSubview(txtAddress)
        self.view.addSubview(txtPhoneNo)
        self.view.addSubview(btnDeletePlace)
        
        btnDeletePlace.addSubview(activityIndicatorDeleteButton)
        
        self.view.addSubview(dimView)
        
        //layout
        setupLayout()
    }
    
    override func viewDidLayoutSubviews() {
        
        //add bottom borders, done after adding views
        //txtDisplayName.addBottomBorder()
        txtPlaceName.addBottomBorder()
        txtAddress.addBottomBorder()
        txtPhoneNo.addBottomBorder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: UI Layout
    
    func setupLayout(){
        
        imgViewPlace.topAnchor.constraint(equalTo: view.topAnchor, constant:80).isActive = true
        imgViewPlace.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imgViewPlace.widthAnchor.constraint(equalToConstant: view.frame.width/4).isActive = true
        imgViewPlace.heightAnchor.constraint(equalTo: imgViewPlace.widthAnchor).isActive = true
        
        imgViewPlace.layer.cornerRadius = view.frame.width/8
        imgViewPlace.clipsToBounds = true
        
        txtPlaceName.topAnchor.constraint(equalTo: imgViewPlace.bottomAnchor, constant:10).isActive = true
        txtPlaceName.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        txtPlaceName.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        txtAddress.topAnchor.constraint(equalTo: txtPlaceName.bottomAnchor, constant:10).isActive = true
        txtAddress.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        txtAddress.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        
        txtPhoneNo.topAnchor.constraint(equalTo: txtAddress.bottomAnchor, constant:10).isActive = true
        txtPhoneNo.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:-40).isActive = true
        txtPhoneNo.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:40).isActive = true
        
        btnDeletePlace.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -49).isActive = true
        btnDeletePlace.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        btnDeletePlace.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        btnDeletePlace.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        
        activityIndicatorDeleteButton.centerYAnchor.constraint(equalTo: btnDeletePlace.centerYAnchor).isActive = true
        activityIndicatorDeleteButton.centerXAnchor.constraint(equalTo: btnDeletePlace.centerXAnchor).isActive = true
        
        dimView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        dimView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        dimView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
    }
    
    // MARK: Helpers
    
    //update appearance updating place
    func updateAppearanceUpdatingPlace(_ sender : Any){
        
        //start activity indicator
        activityIndicator.startAnimating()
        
        //hide keyboard
        hideKeyboard(sender)
        
        //show activity view
        self.dimView.isHidden = false
        
        //btn update
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activityIndicator)
    }
    
    //update appearance after updating place
    func updateAppearanceUpdatingPlaceDone(_ sender: Any){
        
        //stop activity indicator
        activityIndicator.stopAnimating()
        
        //hide dim view
        self.dimView.isHidden = true
        
        //btn update
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.plain, target: self, action: #selector(btnUpdate))
        
    }
    
    //update appearance deleting place
    func updateAppearanceDeletingPlace(_ sender : Any){
        
        //show dim view
        self.dimView.isHidden = false
        
        //show activity indicator
        self.activityIndicatorDeleteButton.startAnimating()
        
        //btn update
        self.btnDeletePlace.isEnabled = false
        self.btnDeletePlace.alpha = 0.5
    }
    
    //update appearance after deleting place
    func updateAppearanceDeletingPlaceDone(_ sender: Any){
        
        //stop activity indicator
        self.activityIndicatorDeleteButton.stopAnimating()
        
        //hide dim view
        self.dimView.isHidden = true
        
        //btn update
        self.btnDeletePlace.isEnabled = true
        self.btnDeletePlace.alpha = 1
    
    }
    
    //helper - enable save button
    func enableSaveButton(){
        if (isPlaceNameSet && isPlaceAddressSet && isPlacePhoneNoSet && isPlaceImageSelected){
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        } else {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
    
    // MARK: Actions
    
    //hide keyboard
    @objc private func hideKeyboard(_ sender: Any){
        self.view.endEditing(true)
    }
    
    //select image from camera or library
    @objc func selectImage(_sender: UIImageView){
        let imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        let alertControllerTitle = NSLocalizedString("Select Source", comment: "")
        let cameraTitle = NSLocalizedString("Camera", comment: "")
        let libraryTitle = NSLocalizedString("Photo Library", comment: "")
        
        //show options for camera or library
        let alertController = UIAlertController(title: alertControllerTitle, message: "", preferredStyle: .actionSheet)
        
        //camera
        let camera = UIAlertAction(title: cameraTitle, style: .default){handler in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                imagePicker.sourceType = .camera
                imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
                self.present(imagePicker, animated: true, completion: nil)
            } else {
                let error = NSError(domain: "TaskForSelectImage", code: 1, userInfo : ["JopaJopaErrorType" : JopaJopaClient.JopaJopaErrorType.MEDIA_SOURCE_NOT_AVAILABLE])
                self.present(JopaJopaClient.sharedInstance().getAlertControllerForErrors(error, ""), animated: true, completion: nil)
            }
        }
        
        //photo library
        let library = UIAlertAction(title: libraryTitle, style: .default){ handler in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                imagePicker.sourceType = .photoLibrary
                imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
                self.present(imagePicker, animated: true, completion: nil)
            } else {
                let error = NSError(domain: "TaskForSelect", code: 1, userInfo : ["JopaJopaErrorType" : JopaJopaClient.JopaJopaErrorType.MEDIA_SOURCE_NOT_AVAILABLE])
                self.present(JopaJopaClient.sharedInstance().getAlertControllerForErrors(error, ""), animated: true, completion: nil)
            }
        }
        
        //cancel
        let cancel = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        
        //add actions
        alertController.addAction(camera)
        alertController.addAction(library)
        alertController.addAction(cancel)
        
        //show imagepicker option
        self.present(alertController, animated: true, completion: nil)
    }
    
    //update place
    @objc func btnUpdate(_ sender: UIBarButtonItem){
        
        //update appearance
        updateAppearanceUpdatingPlace(sender)
        
        // updated place
        var updatedPlace: JopaJopaPlace
        
        //check if user updated his location, gms will not be nil if so
        if let gmsPlace = gmsPlace {
            //get country from gmsPlace
            var country: String?
            for component in (gmsPlace.addressComponents)! {
                if component.type == "country" {
                    country = component.name
                }
            }
            //create location
            let location = JopaJopaPlace.Location(address: gmsPlace.formattedAddress!, country: country)
            
            //create JopaJopaPlace
            updatedPlace = JopaJopaPlace(placeName: txtPlaceName.text, placeLocation: location, placeGeoLocation: [gmsPlace.coordinate.longitude, gmsPlace.coordinate.latitude], placePhoneNo: txtPhoneNo.text)
            
        }
        //if user didn't update his location
        else {
            updatedPlace = JopaJopaPlace(placeName: txtPlaceName.text, placeLocation: place.placeLocation, placeGeoLocation: place.placeGeoLocation, placePhoneNo: txtPhoneNo.text)
        }
    
        // encode json
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let data = try! encoder.encode(updatedPlace)
        
        //json to string
        let placeString = String(data: data, encoding: .utf8)!
        
        //call convenience method to update place details and get upload url
        firstly {
            JopaJopaClient.sharedInstance().updatePlace(jsonBody: placeString, placeID: place.placeID!)
            }
            // upload image to digital ocean spaces
            .then { (id, url) -> Promise<Bool> in
                //image data
                let img = self.imgViewPlace.image?.resized(toWidth: 400)
                let image_data = UIImageJPEGRepresentation(img!, 0.5)!
                return JopaJopaClient.sharedInstance().uploadImageToSpaces(signedURL: url, data: image_data)
            }
            //if no error, show success message to user
            .done{ result in
                
                //update appearance
                performUIUpdatesOnMain {
                    self.updateAppearanceUpdatingPlaceDone(sender)
                }
                
                //save updated place to place so we can use it later in the placeUpdated Delegate
                self.place = updatedPlace
                
                //pop view controller
                self.navigationController?.popViewController(animated: true)
                self.delegate?.didFinishUpdatingPlace(self)
                
            }
            .catch{ error in
                
                //cast error to NSError
                let nsError = error as NSError
                
                //show activity view
                self.dimView.isHidden = true
                self.activityIndicator.stopAnimating()
                
                //alert controller
                let alertController = JopaJopaClient.sharedInstance().getAlertControllerForErrors(nsError, JopaJopaClient.JopaJopaTask.Update_Place)
                
                // if error is authentication failure, log user out
                if (nsError.userInfo["JopaJopaErrorType"] as! String == JopaJopaClient.JopaJopaErrorType.AUTHENTICATION_FAILURE){
                    JopaJopaClient.sharedInstance().logoutUser()
                    let logoutAction = UIAlertAction(title: "Close", style: .default){ handler in
                        self.present(MainTabViewController(), animated: true, completion: nil)
                    }
                    alertController.addAction(logoutAction)
                }
                
                performUIUpdatesOnMain {
                    self.present(alertController, animated: true, completion: nil)
                    self.updateAppearanceUpdatingPlaceDone(sender)
                }
            }
        
    }
    
    
    // delete place confirmation
    @objc func deletePlace(_ sender: NormalButton) {
        
        //confirm deletion
        let alertView: UIAlertController = {
            let myAlertView = UIAlertController(title: NSLocalizedString("Delete", comment:""), message: NSLocalizedString("Are you sure?", comment:""), preferredStyle: UIAlertControllerStyle.alert)
            let alertAction1 = UIAlertAction(title: NSLocalizedString("Cancel", comment:""), style: UIAlertActionStyle.default){handler in
                self.updateAppearanceDeletingPlaceDone(sender)
            }
            let alertAction2 = UIAlertAction(title: NSLocalizedString("Yes", comment:""), style: UIAlertActionStyle.default){handler in
                self.proceedDeletePlace(sender)
            }
            myAlertView.addAction(alertAction1)
            myAlertView.addAction(alertAction2)
            return myAlertView
        }()
        
        //update appearance
        self.updateAppearanceDeletingPlace(sender)
        
        self.present(alertView, animated: true, completion: nil)
    }
    
    
    //delete place proceed
    private func proceedDeletePlace(_ sender : Any){
        
        
        //convenience method  deleted place
        JopaJopaClient.sharedInstance().deletePlace(placeID: place.placeID!){error in
            
            //update appearance
            performUIUpdatesOnMain {
                self.updateAppearanceDeletingPlaceDone(sender)
            }
           
            //check error
            if let error = error {
                //alert controller
                let alertController = JopaJopaClient.sharedInstance().getAlertControllerForErrors(error, JopaJopaClient.JopaJopaTask.Delete_Place)
                
                // if error is authentication failure, log user out
                if (error.userInfo["JopaJopaErrorType"] as! String == JopaJopaClient.JopaJopaErrorType.AUTHENTICATION_FAILURE){
                    JopaJopaClient.sharedInstance().logoutUser()
                    let logoutAction = UIAlertAction(title: "Close", style: .default){ handler in
                        self.present(MainTabViewController(), animated: true, completion: nil)
                    }
                    alertController.addAction(logoutAction)
                }
                performUIUpdatesOnMain {
                    self.present(alertController, animated: true, completion: nil)
                }
                
            }
            //place deleted successfully, call delegate didFinishDeleting place
            else {
                performUIUpdatesOnMain {
                    self.navigationController?.popViewController(animated: true)
                    self.delegate?.didFinishDeletingPlace(self.indexPath!)
                }
            }
        }
        
    }
    
}

// MARK: ImagePickerControllerDelegate

extension PlaceDetailViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //image picked
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        // update boolean
        isPlaceImageSelected = true
        
        //check enable save btn
        enableSaveButton()
        
        //get chosen image
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        //dismiss picker
        dismiss(animated: true, completion: nil)
        
        //instantiate ImageCropperViewController
        let imageCropperViewController = ImageCropperViewController(image: chosenImage)
        
        //make this view controller the delegate
        imageCropperViewController.delegate = self
        
        //presnet ImageCropperViewController
        self.present(imageCropperViewController, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}

// MARK: Image Cropper Delegate

extension PlaceDetailViewController: ImageCropperViewControllerDelegate {
    
    // image finished cropping, update image view
    func imageDidFinishCropping(imageCropperViewController: ImageCropperViewController) {
        
        //for debugging - print cropped image size
        //print("Cropped Image  size: \(imageCropperViewController.croppedImage.size)")
        
        //update imageView
        imgViewPlace.image = imageCropperViewController.croppedImage
        
    }
    
}

// MARK: Text Field Delegates

extension PlaceDetailViewController: UITextFieldDelegate {
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if (textField == txtAddress) {
            self.present(gmsAutoCompleteController, animated: true, completion: nil)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // get email and password from text
        var updatedPlaceName = txtPlaceName.text!
        var updatedAddress = txtAddress.text!
        var updatedPhoneNo = txtPhoneNo.text!
        
        if let placeName = txtPlaceName.text,
            let textRange = Range(range, in: placeName) {
            if (textField == txtPlaceName){
                updatedPlaceName = placeName.replacingCharacters(in: textRange,
                                                                 with: string)
            }
        }
        
        if let address = txtAddress.text,
            let textRange = Range(range, in: address) {
            if (textField == txtAddress){
                updatedAddress = address.replacingCharacters(in: textRange,
                                                             with: string)
            }
        }
        
        if let phoneNo = txtPhoneNo.text,
            let textRange = Range(range, in: phoneNo) {
            if (textField == txtPhoneNo){
                updatedPhoneNo = phoneNo.replacingCharacters(in: textRange,
                                                             with: string)
            }
        }
        
        //for enabling save button
        
        if(!updatedPlaceName.isEmpty){
            isPlaceNameSet = true
        } else {
            isPlaceNameSet = false
        }
        
        if(!updatedAddress.isEmpty){
            isPlaceAddressSet = true
        } else {
            isPlaceAddressSet = false
        }
        
        if(!updatedPhoneNo.isEmpty){
            isPlacePhoneNoSet = true
        } else {
            isPlacePhoneNoSet = false
        }
        
        //enable save btn
        enableSaveButton()
        
        return true
    }
}

// MARK: Goole Place AutoComplete Delegate

extension PlaceDetailViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        //save value to gmsPlace property
        gmsPlace = place
        
        //update address field
        txtAddress.text = place.formattedAddress!
        
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        
        dismiss(animated: true, completion: nil)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        
        dismiss(animated: true, completion: nil)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
}


