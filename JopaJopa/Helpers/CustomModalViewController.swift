//
//  CustomModalViewController.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 31/7/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit

protocol CustomModalViewControllerDelegate {
    func didFinishSelecting(viewController: UIViewController)
    
    func didCancelSelecting(viewController: UIViewController)
}

class CustomModalViewController: UIViewController {
    
    // MARK: Properties
    var modalTitle: String?
    var dataArray : [String]?
    var delegate: CustomModalViewControllerDelegate?
    var selectedValue: String?
    
    //table view for post categories
    let tableView: UITableView = {
        let tblView = UITableView()
        tblView.translatesAutoresizingMaskIntoConstraints = false
        tblView.register(UITableViewCell.self, forCellReuseIdentifier: "customModalCell")
        tblView.layer.borderWidth = 1
        tblView.layer.borderColor = UIColor.lightGray.cgColor
        tblView.layer.cornerRadius = 10
        tblView.isScrollEnabled = false
        
        return tblView
    }()
    
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //assign delegates
        tableView.delegate = self
        tableView.dataSource = self
        
        //add sub views
        self.view.addSubview(tableView)
        
        //adjust table size to fit content
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        //setup
        setupLayout()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = CGRect(x: tableView.frame.origin.x, y: tableView.frame.origin.y, width: tableView.frame.size.width, height: tableView.contentSize.height)
        tableView.reloadData()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Setup Views
    
    private func setupLayout(){
        //table view
        let yOffset = self.view.frame.size.height / 4
        self.tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: yOffset).isActive = true
        self.tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant:20).isActive = true
        self.tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant:-20).isActive = true
    }
    
}

// MARK: TableView extension

extension CustomModalViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let dataArray = self.dataArray else {
            return 0
        }
        return (dataArray.count + 2) //add 2 for title and cancel
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "customModalCell", for: indexPath)
//        cell.layer.masksToBounds = true
//        cell.clipsToBounds = true
        if (self.modalTitle != nil && indexPath.row == 0){
            //set title
            cell.textLabel?.text = self.modalTitle
            cell.backgroundColor = ViewConstants.TAB_BAR_BACKGROUND_COLOR
            cell.textLabel?.textAlignment = NSTextAlignment.center
        } else if (indexPath.row == (dataArray?.count)! + 1) {
            //set cancel button
            cell.backgroundColor = ViewConstants.TAB_BAR_BACKGROUND_COLOR
            cell.textLabel?.text = NSLocalizedString("Cancel", comment: "")
            cell.textLabel?.textAlignment = NSTextAlignment.center
            cell.textLabel?.textColor = ViewConstants.BUTTON_COLOR_DEFAULT
        } else {
            //data
            //cell.backgroundColor = ViewConstants.TAB_BAR_BACKGROUND_COLOR
            cell.textLabel?.text = dataArray?[indexPath.row-1]
            cell.textLabel?.textAlignment = NSTextAlignment.center
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.didFinishSelecting(viewController: self)
        
        if (indexPath.row == (dataArray?.count)! + 1) {
            self.dismiss(animated: true, completion: nil)
            self.delegate?.didCancelSelecting(viewController: self)
        } else {
            //data
            let cell = tableView.cellForRow(at: indexPath)
            self.selectedValue = cell?.textLabel?.text
            self.delegate?.didFinishSelecting(viewController: self)
        }
    }
    
}
