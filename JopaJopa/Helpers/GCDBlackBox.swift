//
//  GCDBlackBox.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 21/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import Foundation

func performUIUpdatesOnMain(_ updates: @escaping () -> Void) {
    DispatchQueue.main.async {
        updates()
    }
}

func performUIUpdatesOnMainWithDelay(_ updates: @escaping () -> Void) {
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
        updates()
    }
}
