//
//  InputValidator.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 8/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import Foundation

class InputValidator {
    
    static func isValidEmail (_ email: String?) -> Bool{
        guard email != nil else { return false }
        
        let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let pred = NSPredicate(format:"SELF MATCHES %@", regEx)
        return pred.evaluate(with: email)
    }
}
