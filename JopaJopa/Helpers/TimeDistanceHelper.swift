//
//  TimeHelper.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 28/8/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import Foundation

class TimeDistanceHelper {
    
    // MARK: Time Helpers
    
    //returns hours, mins and seconds
    static func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    static func secondsToHoursMinutesSecondsColonFormatted(seconds : Int) -> String {
        let hour = "\(seconds / 3600)"
        
        let mins = (seconds % 3600) / 60
        let minString = mins < 10 ? "0\(mins)": "\(mins)"
        
        let seconds = (seconds % 3600) % 60
        let secondsString = seconds < 10 ? "0\(seconds)": "\(seconds)"
        
        return "\(hour):\(minString):\(secondsString)"
    }
    
    //returns hours and minutes
    static func secondsToHoursMinutesFormatted (seconds : Int) -> String {
        let hour = seconds / 3600
        let mins = (seconds % 3600) / 60
        
        if (hour == 0 && mins == 0) {
            return "0m"
        }
        else if hour == 0 {
            return "\(mins)m"
        } else {
             return "\(hour)h \(mins)m"
        }
    }
    
    //returns hours only or minutes only
    static func secondsToHoursOrMinutesOnlyFormatted (seconds : Int) -> String {
        let hour = seconds / 3600
        let mins = (seconds % 3600) / 60
        
        if (hour == 0 && mins == 0) {
            return "0m"
        }
        else if hour == 0 {
            return "\(mins)m"
        } else {
            let minString = mins < 10 ? "0\(mins)": "\(mins)"
            return "\(hour)h \(minString)m"
        }
    }
    
    //returns time remaining between time1 and time2
    static func timeRemaining (time1InSeconds : Int, time2InSeconds: Int) -> String {
        
        if (time1InSeconds < time2InSeconds){
            return "0:00"
        }
        
        let time1 = secondsToHoursMinutesSeconds(seconds: time1InSeconds)
        let time2 = secondsToHoursMinutesSeconds(seconds: time2InSeconds)
        
        let hours_remaining = time1.0 - time2.0
        let mins_remaining = time1.1 - time2.1
        let secs_remaining = time1.2 - time2.2
        
        if (mins_remaining < 10){
             return "\(hours_remaining):0\(mins_remaining)"
        }
        
        return "\(hours_remaining):\(mins_remaining)"
    }
    
    //convert date to ISO Date
    static func isoFormattedDate(date: Date) -> String {
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]
        let dateString = formatter.string(from: date)
        return dateString
    }
    
    //return trimmed ISO String, removes seconds thousandth part from time string returned by db
    static func isoTrimmedDate(isoDateString: String) -> Date?{
        let trimmedIsoString = isoDateString.replacingOccurrences(of: "\\.\\d+", with: "", options: .regularExpression)
        let formatter = ISO8601DateFormatter()
        return formatter.date(from: trimmedIsoString)
    }
    
    
    // MARK: Distance Helpers
    
    
    //return distance as String in metres if less than 1000 and in km otherwise
    static func formatDistance(dist: Double) -> String {
        if (dist >= 1000){
            return "\(round(dist / 1000))km"
        } else {
            return "\(Int(dist))m"
        }
    }
    
    
}
