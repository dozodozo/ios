//
//  JopaJopaConvenienceSpaces.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 6/7/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import Foundation
import PromiseKit

// MARK: Convenience Methods for managing image uploads

extension JopaJopaClient {
    
    // MARK: Upload Image
    
    func uploadImageToSpaces(signedURL: URL, data: Data) -> Promise<Bool> {
        
        return Promise{ seal in
           
            /* Configure request */
            let request = NSMutableURLRequest(url: signedURL)
            request.httpMethod = "PUT"
            request.addValue("\(data.count)", forHTTPHeaderField: "Content-Length")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue("testImage", forHTTPHeaderField: "fileName")
            
            //make image public
            request.addValue("public-read", forHTTPHeaderField: "x-amz-acl")
            request.httpBody = data
            
            let task = JopaJopaClient.sharedInstance().session.dataTask(with: request as URLRequest){ data, response, error in
                
                // send error to completionHandlerForDelete
                func sendError(_ errorType: String) {
                    let error = NSError(domain: "TaskForUploadImage", code: 1, userInfo : ["JopaJopaErrorType" : errorType])
                    seal.reject(error)
                }
                
                /* GUARD: Was there an error? */
                guard error == nil else {
                    sendError(JopaJopaErrorType.SESSION_ERROR)
                    return
                }
                
                /* GUARD: Is there a status code? */
                guard let statusCode = (response as? HTTPURLResponse)?.statusCode else {
                    sendError(JopaJopaErrorType.NO_STATUS_CODE_ERROR)
                    return
                }
                
                //check if status code is other than 2xx
                guard (statusCode < 300) else {
                    sendError(JopaJopaErrorType.SERVER_ERROR)
                    return
                }
                
                seal.fulfill(true)

            }
            task.resume()
        }
    }
    
    
    // MARK: Get Image From Cache
    
    //TODO
    
    
    
    // MARK: Get Image With URL Revalidating Cache Data
    
    func taskForGETImageFromSpaces(url: URL, useCachedImage: Bool? = nil, completionHandlerForImage: @escaping (_ imageData: Data?, _ error: NSError?) -> Void) -> Void {
        
        /* 1. Set the parameters */
        // There are none...
        

        /* 4. Make the request */
        var request: URLRequest
        if let useCachedImage = useCachedImage {
            request = URLRequest(url: url)
        } else {
            request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadRevalidatingCacheData, timeoutInterval: 60.0)
        }
       
        //let request = URLRequest(url: url) // from cache
        //let request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadRevalidatingCacheData, timeoutInterval: 60.0)
        let task = JopaJopaClient.sharedInstance().session.dataTask(with: request) { (data, response, error) in
            
            func sendError(_ error: String) {
                print(error)
                let userInfo = [NSLocalizedDescriptionKey : error]
                completionHandlerForImage(nil, NSError(domain: "taskForGETMethod", code: 1, userInfo: userInfo))
            }
            
            /* GUARD: Was there an error? */
            guard (error == nil) else {
                sendError("There was an error with your request: \(error!)")
                return
            }
            
            /* GUARD: Did we get a successful 2XX response? */
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                sendError("Your request returned a status code other than 2xx!")
                return
            }
            
            /* GUARD: Was there any data returned? */
            guard let data = data else {
                sendError("No data was returned by the request!")
                return
            }
            
            /* 5/6. Parse the data and use the data (happens in completion handler) */
            completionHandlerForImage(data, nil)
        }
        
        /* 7. Start the request */
        task.resume()
        
    }
    
}
