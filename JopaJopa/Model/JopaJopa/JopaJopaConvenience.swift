//
//  JopaJopaConvenience.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 26/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import Foundation
import UIKit

// MARK : JopaJopaClient Resource Methods

extension JopaJopaClient {
    
    // MARK : SIGN UP
    
    func signup(jsonBody: String, _ completionHandlerForSignUp: @escaping (_ error: NSError?) ->Void ) -> Void {
        
        JopaJopaClient.sharedInstance().taskForPostMethod(JopaJopaClient.APIResources.SignUp, jsonBody: jsonBody){result, error in
            
            if error != nil {
                completionHandlerForSignUp(error)
            } else {
                completionHandlerForSignUp(nil)
            }
            
        }
    }
    
    
    // MARK : Login
    
    func login(jsonBody: String, _ completionHandlerForLogin: @escaping (_ error: NSError?) ->Void ) -> Void {
        
        JopaJopaClient.sharedInstance().taskForPostMethod(JopaJopaClient.APIResources.Login, jsonBody: jsonBody){result, error in
            
            //check if errors 
            if let error = error {
                completionHandlerForLogin(error)
            } else {
                //update userdefaults
                let user = JopaJopaUser(dict: result![JSONReponseKeys.Data] as! [String : AnyObject])
                let id = user.id
                let accessToken = user.accessToken
                
                JopaJopaClient.sharedInstance().user = user
                UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
                UserDefaults.standard.set(id, forKey: "id")
                UserDefaults.standard.set(accessToken, forKey: "accessToken")

                JopaJopaClient.sharedInstance().isUserLoggedIn = true
    
                completionHandlerForLogin(nil)
            }
            
        }
    }
    
    // MARK : Reset Password E-mail
    
    func sendResetPasswordEmail(jsonBody: String, _ completionHandlerForResetPassword: @escaping (_ error: NSError?) ->Void ) -> Void {
        JopaJopaClient.sharedInstance().taskForPostMethod(JopaJopaClient.APIResources.GetPasswordResetLink, jsonBody: jsonBody){result, error in
            
            if let error = error {
                completionHandlerForResetPassword(error)
            } else {
                completionHandlerForResetPassword(nil)
            }
            
        }
    }
    
    // MARK : Get User Details
    
    func getUserDetails(_ completionHandlerForGetUserDetails: @escaping(_ result: JopaJopaUser?, _ error: NSError?) -> Void) -> Void {
        
        //get accessToken and user id from UserDefaults
        let accessToken = JopaJopaClient.sharedInstance().getUserAccessToken()
        let id = JopaJopaClient.sharedInstance().getUserID()
        
        /* 1. Specify parameters, method (if has {key}), and HTTP body (if POST) */
        
        let parameters = [JopaJopaClient.ParameterKeys.AccessToken: accessToken]
        var mutableMethod: String = JopaJopaClient.APIResources.GetUserDetails
        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.ID, value:id)!
        
        /* 2. Make the request */
        
        JopaJopaClient.sharedInstance().taskForGetMethod(mutableMethod, parameters as [String: AnyObject]){result, error in
            

            if let error = error {
               completionHandlerForGetUserDetails(nil, error)
            } else {
                //create user from json data
                if let result = result as? [String:AnyObject] {
                    let user = JopaJopaUser(dict: result[JopaJopaClient.JSONReponseKeys.Data]![JSONReponseKeys.User] as! [String:AnyObject])
                    completionHandlerForGetUserDetails(user, nil)
                }
            }
            
        }
        
    }
    
    // MARK : Update User Details
    
    func updateUserDetails(jsonBody: String, _ completionHandlerForUpdateUserDetails: @escaping (_ result: AnyObject?, _ error: NSError?) ->Void ) -> Void {
        
        //get accessToken and user id from UserDefaults
        let accessToken = JopaJopaClient.sharedInstance().getUserAccessToken()
        let id = JopaJopaClient.sharedInstance().getUserID()
        
        /* 1. Specify parameters, method (if has {key}), and HTTP body (if POST) */
        
        let parameters = [JopaJopaClient.ParameterKeys.AccessToken: accessToken]
        var mutableMethod: String = JopaJopaClient.APIResources.UpdateUserDetails
        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.ID, value:id)!
        
        /* 2. Make the request */
        
        JopaJopaClient.sharedInstance().taskForPutMethod(mutableMethod, parameters as [String: AnyObject], jsonBody: jsonBody){result, error in
            
            if let error = error {
                completionHandlerForUpdateUserDetails(nil, error)
            } else {
                completionHandlerForUpdateUserDetails(result,nil)
            }
            
        }
    }
    
    // MARK : Update User Password
    
    func updateUserPassword(jsonBody: String, _ completionHandlerForUpdateUserPassword: @escaping (_ result: AnyObject?, _ error: NSError?) ->Void ) -> Void {
        
        //get accessToken and user id from UserDefaults
        let accessToken = JopaJopaClient.sharedInstance().getUserAccessToken()
        let id = JopaJopaClient.sharedInstance().getUserID()
        
        /* 1. Specify parameters, method (if has {key}), and HTTP body (if POST) */
        
        let parameters = [JopaJopaClient.ParameterKeys.AccessToken: accessToken]
        var mutableMethod: String = JopaJopaClient.APIResources.UpdateUserPassword
        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.ID, value:id)!
        
        /* 2. Make the request */
        
        JopaJopaClient.sharedInstance().taskForPutMethod(mutableMethod, parameters as [String: AnyObject], jsonBody: jsonBody){result, error in
            
            if let error = error {
               completionHandlerForUpdateUserPassword(nil, error)
            } else {
                completionHandlerForUpdateUserPassword(result, nil)
            }
            
        }
    }
    
    
    // MARK : Update User Email
    
    func updateUserEmail(jsonBody: String, _ completionHandlerForUpdateUserEmail: @escaping (_ result: AnyObject?, _ error: NSError?) ->Void ) -> Void {
        
        //get accessToken and user id from UserDefaults
        let accessToken = JopaJopaClient.sharedInstance().getUserAccessToken()
        let id = JopaJopaClient.sharedInstance().getUserID()
        
        /* 1. Specify parameters, method (if has {key}), and HTTP body (if POST) */
        
        let parameters = [JopaJopaClient.ParameterKeys.AccessToken: accessToken]
        var mutableMethod: String = JopaJopaClient.APIResources.UpdateUserEmail
        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.ID, value:id)!
        
        /* 2. Make the request */
        
        JopaJopaClient.sharedInstance().taskForPutMethod(mutableMethod, parameters as [String: AnyObject], jsonBody: jsonBody){result, error in
            
            if let error = error {
               completionHandlerForUpdateUserEmail(nil, error)
            } else {
                completionHandlerForUpdateUserEmail(result, nil)
            }
            
        }
    }
    
    // MARK : Get user account balance
    
    func getAccountBalance(_ completionHandlerForGetAccountBalance: @escaping(_ balance: Double?, _ error: NSError?) -> Void) -> Void{
        
        //get accessToken and user id from UserDefaults
        let accessToken = JopaJopaClient.sharedInstance().getUserAccessToken()
        let id = JopaJopaClient.sharedInstance().getUserID()
        
        /* 1. Specify parameters, method (if has {key}), and HTTP body (if POST) */
        
        let parameters = [JopaJopaClient.ParameterKeys.AccessToken: accessToken]
        var mutableMethod: String = JopaJopaClient.APIResources.GetUserAccountBalance
        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.ID, value:id)!
        
        //call api
        JopaJopaClient.sharedInstance().taskForGetMethod(mutableMethod, parameters as [String: AnyObject]){ result, error in
            var balance : Double? = 0.0
            if let error = error {
                completionHandlerForGetAccountBalance(nil, error)
            } else {
                let data = result![JopaJopaClient.JSONReponseKeys.Data] as! [String: AnyObject]
                balance = data[JopaJopaClient.JSONReponseKeys.AccountBalance] as! Double
                completionHandlerForGetAccountBalance(balance, error)
            }
        }
    }
    
    
}
