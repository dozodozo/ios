//
//  JopaJopaConveniencePlaces.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 5/7/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import Foundation
import PromiseKit

// MARK: Convenience Methods for managing places

extension JopaJopaClient {
    
    // MARK: Get Places List
    
    func getPlacesList(_ completionHandlerForGetPlacesList: @escaping(_ places: [JopaJopaPlace]?, _ error: NSError?) -> Void) -> Void {
        
        //get accessToken and user id from UserDefaults
        let accessToken = JopaJopaClient.sharedInstance().getUserAccessToken()
        let id = JopaJopaClient.sharedInstance().getUserID()
        
        /* 1. Specify parameters, method (if has {key}), and HTTP body (if POST) */
        
        let parameters = [JopaJopaClient.ParameterKeys.AccessToken: accessToken]
        var mutableMethod: String = JopaJopaClient.APIResources.UserPlaces
        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.ID, value:id)!
        
        
        /* 2. Make the request */
        JopaJopaClient.sharedInstance().taskForGetMethod(mutableMethod, parameters as [String: AnyObject]){result, error in
            
            if error != nil {
                completionHandlerForGetPlacesList(nil, error)
            } else {
                var places : [JopaJopaPlace] = [JopaJopaPlace]()
                let data = result?[JopaJopaClient.JSONReponseKeys.Data]  as! [[String:AnyObject]]
                for place in data {
                    places.append(JopaJopaPlace(dict: place))
                }
                
                completionHandlerForGetPlacesList(places, nil)
                
            }
            
        }
        
    }
    
    
    // MARK: Add Place
    
    func addPlace(jsonBody: String) -> Promise<(id: String, url: URL)> {
        
        //get accessToken and user id from UserDefaults
        let accessToken = JopaJopaClient.sharedInstance().getUserAccessToken()
        let id = JopaJopaClient.sharedInstance().getUserID()
        
        /* 1. Specify parameters, method (if has {key}), and HTTP body (if POST) */
        
        let parameters = [JopaJopaClient.ParameterKeys.AccessToken: accessToken]
        var mutableMethod: String = JopaJopaClient.APIResources.UserPlaces
        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.ID, value:id)!
        
        return Promise{ seal in
            JopaJopaClient.sharedInstance().taskForPostMethod(mutableMethod, parameters as [String: AnyObject], jsonBody: jsonBody){ result, error in
                if let error = error {
                    seal.reject(error)
                } else {
                    if let result = result as? [String:AnyObject] {
                        print(result)
                        let data = result[JopaJopaClient.JSONReponseKeys.Data]  as! [String:AnyObject]
                        let url = URL(string: data[JopaJopaClient.JSONReponseKeys.URL] as! String)!
                        let id = data[JopaJopaClient.JSONReponseKeys.ID] as! String
                        seal.fulfill((id, url))
                    }
                }
            }
        }
    }
    
    // MARK: Update Place
    
    func updatePlace(jsonBody: String, placeID: String) -> Promise<(id: String, url: URL)> {
        
        //get accessToken and user id from UserDefaults
        let accessToken = JopaJopaClient.sharedInstance().getUserAccessToken()
        let id = JopaJopaClient.sharedInstance().getUserID()
        
        /* 1. Specify parameters, method (if has {key}), and HTTP body (if POST) */
        
        let parameters = [JopaJopaClient.ParameterKeys.AccessToken: accessToken]
        var mutableMethod: String = JopaJopaClient.APIResources.UserPlaceDetails
        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.ID, value:id)!
        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.PlaceID, value:placeID)!
        
        return Promise{ seal in
            JopaJopaClient.sharedInstance().taskForPutMethod(mutableMethod, parameters as [String: AnyObject], jsonBody: jsonBody){ result, error in
                if (error != nil){
                    seal.reject(error!)
                } else {
                    if let result = result as? [String:AnyObject] {
                        let data = result[JopaJopaClient.JSONReponseKeys.Data]  as! [String:AnyObject]
                        let url = URL(string: data[JopaJopaClient.JSONReponseKeys.URL] as! String)!
                        let id = data[JopaJopaClient.JSONReponseKeys.PlaceID] as! String
                        seal.fulfill((id, url))
                    }
                }
            }
        }
    }
    
    
    // MARK: Remove dateExpiry field from places in db - remove place if image was not uploaded
    
    func placeRemoveDateExpiry(jsonBody: String, placeID: String) -> Promise<Bool> {
        
        //get accessToken and user id from UserDefaults
        let accessToken = JopaJopaClient.sharedInstance().getUserAccessToken()
        let id = JopaJopaClient.sharedInstance().getUserID()
        
        /* 1. Specify parameters, method (if has {key}), and HTTP body (if POST) */
        
        let parameters = [JopaJopaClient.ParameterKeys.AccessToken: accessToken]
        var mutableMethod: String = JopaJopaClient.APIResources.UserPlaceDetails
        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.ID, value:id)!
        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.PlaceID, value:placeID)!
        
        return Promise{ seal in
            JopaJopaClient.sharedInstance().taskForPatchMethod(mutableMethod, parameters as [String: AnyObject], jsonBody: jsonBody){ result, error in
                if let error = error {
                    seal.reject(error)
                } else {
                    seal.fulfill(true)
                }
            }
        }
    }
    
    
    // MARK: Delete Place
    
    func deletePlace(placeID: String, _ completionHandlerForDeletePlace: @escaping(_ error: NSError?) -> Void) -> Void{
        
        //get accessToken and user id from UserDefaults
        let accessToken = JopaJopaClient.sharedInstance().getUserAccessToken()
        let id = JopaJopaClient.sharedInstance().getUserID()
        
        /* 1. Specify parameters, method (if has {key}), and HTTP body (if POST) */
        
        let parameters = [JopaJopaClient.ParameterKeys.AccessToken: accessToken]
        var mutableMethod: String = JopaJopaClient.APIResources.UserPlaceDetails
        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.ID, value:id)!
        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.PlaceID, value:placeID)!
        
        JopaJopaClient.sharedInstance().taskForDeleteMethod(mutableMethod, parameters as [String: AnyObject], jsonBody: ""){result, error in
            if let error = error {
                completionHandlerForDeletePlace(error)
            } else {
                completionHandlerForDeletePlace(nil)
            }
        }
    }
    
    
    
}
