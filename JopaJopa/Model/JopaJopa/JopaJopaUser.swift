//
//  JopaJopaUser.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 26/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import Foundation

// MARK: JopaJopaUser

class JopaJopaUser: Codable {
    
    // MARK: Properties
    
    var id: String?
    var firstName: String?
    var lastName: String?
    var email : String?
    var password: String?
    var accessToken : String?
    var role: String?
    var accountStatus : String?
    var accountBalance: Double?
    
    // MARK: Initializer
    
    //for sign up
    init (id: String? = nil, firstName: String, lastName: String, email: String? = nil, password: String? = nil, accessToken: String? = nil, role: String? = nil, accountStatus: String? = nil, accountBalance: Double? = nil){
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.password = password
        self.accessToken = accessToken
        self.role = role
        self.accountStatus = accountStatus
        self.accountBalance = accountBalance
    }
    
    //for parsing data
    init (dict: [String : AnyObject]) {
        self.id = dict[JopaJopaClient.JSONReponseKeys.ID] as! String?
        self.firstName =  dict[JopaJopaClient.JSONReponseKeys.FirstName] as! String?
        self.lastName = dict[JopaJopaClient.JSONReponseKeys.LastName] as! String?
        self.email = dict[JopaJopaClient.JSONReponseKeys.Email] as! String?
        self.password = nil
        self.accessToken = dict[JopaJopaClient.JSONReponseKeys.AccessToken] as! String?
        self.role = dict[JopaJopaClient.JSONReponseKeys.Role] as! String?
        self.accountStatus = dict[JopaJopaClient.JSONReponseKeys.AccountStatus] as! String?
        self.accountBalance = dict[JopaJopaClient.JSONReponseKeys.AccountBalance] as! Double?
        
    }
    
}
