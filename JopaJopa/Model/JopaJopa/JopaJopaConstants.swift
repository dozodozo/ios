//
//  JopaJopaConstants.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 20/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

// MARK: - JoaJopa (Constants)

import Foundation

extension JopaJopaClient {
    
    
    // MARK: Constants
    
    struct Constants {
        
        // MARK: API Key
        static let APIKey = "YOUR_API_KEY_HERE"
        
        // MARK: URLs
        static let APIScheme = "http"
        static let APIHost = "127.0.0.1"
        static let APIPort = 3000
        static let APIPath = ""
        
        // MARK: Google
        static let GOOGLE_PLACES_KEY = "AIzaSyBKE6AptOmQWrL5essHdNPY_mj95gyTkl8"
        
        // MARK: STRIPE
        static let STRIPE_KEY = "pk_test_WWW0L9G2eoxJaMTLCBE0c5hs"
        static let STRIPE_VERSION = "2018-08-23"
        
    }
    
    // MARK: Methods
    struct APIResources {
        
        // MARK: Sign up / Log in
        static let SignUp = "/signup"
        static let Login = "/login"
        static let GetPasswordResetLink = "/reset"
        
        // MARK: User Details
        static let GetUserDetails = "/users/{id}"
        static let UpdateUserDetails = "/users/{id}"
        static let UpdateUserPassword = "/users/{id}/password"
        static let UpdateUserEmail = "/users/{id}/email"
        
        // MARK: Places
        static let UserPlaces = "/users/{id}/places"
        static let UserPlaceDetails = "/users/{id}/places/{placeID}"
        
        // MARK: Posts
        static let UserPosts = "/users/{id}/posts"
        static let UserPostDetails = "/users/{id}/posts/{postID}"
        static let UserPostPublishUnpublish = "/users/{id}/posts/{postID}/publishUnPublish"
        static let PublishedPosts = "/publishedPosts"
        
        // MARK: Billing
        static let CreateStripeCustomer = "/stripe/{id}"
        static let GetCustomerListOfCharges = "/stripe/{id}/charges"
        static let GetUserAccountBalance = "/users/{id}/accountBalance"
        static let StripeEphemeral = "/stripe/{id}/ephemeralKey"
        
    }
    
    // MARK: URL Keys
    struct URLKeys {
        static let ID = "id"
        static let PlaceID = "placeID"
        static let PostID = "postID"
    }
    
    // MARK: Parameter Keys
    struct ParameterKeys {
        static let AccessToken = "accessToken"
        static let StripeVersion = "stripe_version"
        static let Latitude = "lat"
        static let Longitude = "long"
        static let LastDateCreated = "dateCreated"
        static let PostCategory = "postCategory"
    }
    
    // MARK: JSON Response Keys
    
    struct JSONReponseKeys {
        
        // MARK: General
        static let MONGO_ID = "_id"
        static let Access_Token = "accessToken"
        static let Success = "success"
        static let Message = "message"
        static let Data = "data"
        static let User = "user"
        static let ID = "id"
        static let URL = "url"
        static let DateCreated = "dateCreated"
        static let DateExpiry = "dateExpiry"
        
        // MARK: Account
        
        static let FirstName = "firstName"
        static let LastName = "lastName"
        static let Email = "email"
        static let AccessToken = "accessToken"
        static let Role = "role"
        static let AccountStatus = "accountStatus"
        static let AccountBalance = "accountBalance"
        
        // MARK: Places
        static let Place = "place"
        static let PlaceID = "placeID"
        static let Places = "places"
        static let UserID = "userID"
        static let PlaceName = "placeName"
        static let PlaceLocation = "placeLocation"
        static let PlaceAddress = "address"
        static let PlaceCountry = "country"
        static let PlaceGeoLocation = "placeGeoLocation"
        static let PlacePhoneNo = "placePhoneNo"
        static let PlaceImageURL = "placeImageURL"
        
        // MARK: Posts
        static let PostID = "postID"
        static let PostTitle = "postTitle"
        static let PostText = "postText"
        static let PostImageURL = "postImageURL"
        static let PostIsPublished = "postIsPublished"
        static let PostCategory = "postCategory"
        static let PostType = "postType"
        static let PostPlaceIDs = "placeIDs"
        static let PostDateCreated = "dateCreated"
        static let PostDateExpiry = "dateExpiry"
        static let PostDatePublished = "datePublished"
        static let PostDateUnPublish = "dateUnPublish"
        static let PostTimeRemaining = "postTimeRemaining"
        
        //MARK: Published Posts - uses fields from Places and Posts
        static let Distance = "distance" //computed by database
        
        // MARK: Stripe
        static let EphemeralKey = "ephemeralKey"
        
        // MARK: Stripe Charge
        static let ChargeID = "id"
        static let ChargeDescription = "description"
        static let ChargeTransactionDate = "created"
        static let ChargeAmount = "amount"
        static let ChargeCurrency = "currency"
        
        
    }
    
    
    static let UserID = "id"
    
    struct JopaJopaTask{
        static let Log_In = "Logging_In"
        static let Sign_Up = "Sign_Up"
        static let Get_User_Details = "Get_User_Details" //
        static let Update_Email = "Change_Email"
        static let Update_Password = "Change_Password"
        static let Reset_Password = "Reset_Password"
        static let Update_Details = "Update_Details"
        static let Delete_Account = "Delete_Account"
        static let Get_Places = "Get_Places" //
        static let Get_Place_Details = "Get_Place_Details" //
        static let Add_Place = "Add_Place"
        static let Update_Place = "Update_Place"
        static let Delete_Place = "Delete_Place"
        static let Get_Posts = "Get_Posts" //
        static let Get_Post_Details = "Get_Post_Details" //
        static let Add_Post = "Add_Post"
        static let Update_Post = "Update_Post"
        static let Delete_Post = "Delete_Post"
        static let Publish_Post = "Publish_Post"
        static let Unpublish_Post = "Unpublish_Post"
        static let Get_Published_Posts = "Get_Published_Posts"
        static let Add_Credit = "Add_Credit"
        static let Get_Account_Balance = "Get_Account_Balance"
        static let Get_Payment_History = "Get_Payment_History"
    }
    
    
    
    // MARK: Custom Error Codes
    
    struct JopaJopaErrorType{
        //api error account
        static let ACCOUNT_SUSPENDED = "ACCOUNT_SUSPENDED"
        static let INCORRECT_EMAIL_PASSWORD = "INCORRECT_EMAIL_PASSWORD"
        static let INVALID_EMAIL_PASSWORD = "INVALID_EMAIL_PASSWORD"
        static let PASSWORD_INCORRECT = "INCORRECT PASSWORD"
        static let PASSWORD_INVALID = "PASSWORD_INVALID"
        static let PASSWORDS_DONT_MATCH = "PASSWORDS_DONT_MATCH"
        static let INVALID_FORM_DATA = "INVALID_FORM_DATA"
        static let EMAIL_EXISTS = "EMAIL_EXISTS"
        static let EMAIL_INVALID = "EMAIL_INVALID"
        static let AUTHENTICATION_FAILURE = "AUTHENTICATION_FAILURE"
        //api error place
        static let PLACE_IN_USE_UPDATE_FORBIDDEN = "PLACE_IN_USE_UPDATE_FORBIDDEN"
        static let PLACE_IN_USE_DELETE_FORBIDDEN = "PLACE_IN_USE_DELETE_FORBIDDEN"
        
        //api error post
        static let POST_PUBLISHED_UPDATE_FORBIDDEN = "POST_PUBLISHED_UPDATE_FORBIDDEN"
        static let POST_PUBLISHED_DELETE_FORBIDDEN = "POST_PUBLISHED_DELETE_FORBIDDEN"
        //api error stripe
        static let STRIPE_CARD_DECLINED = "STRIPE_CARD_DECLINED"
        static let INSUFFICIENT_CREDIT = "INSUFFICIENT_CREDIT"
        //api error general
        static let CLIENT_ERROR = "CLIENT_ERROR"
        static let SERVER_ERROR = "SERVER_ERROR"
        static let DATABASE_ERROR = "DATABASE_ERROR"
        //ios App error
        static let MEDIA_SOURCE_NOT_AVAILABLE = "MEDIA_SOURCE_NOT_AVAILABLE"
        static let SESSION_ERROR = "SESSION_ERROR"
        static let DATA_ERROR = "DATA_ERROR"
        static let JSON_PARSING_ERROR = "JSON_PARSING_ERROR"
        static let NO_STATUS_CODE_ERROR = "NO_STATUS_CODE_ERROR"
       
    }
    
    struct PostCategories {
        static let eatDrink = NSLocalizedString("Eat & Drink", comment: "")
        static let shop = NSLocalizedString("Shop", comment: "")
        static let whatsOn = NSLocalizedString("What's On", comment: "")
        static let healthBeauty = NSLocalizedString("Health & Beauty", comment: "")
        static let community = NSLocalizedString("Community", comment: "")
        
    }
    
    // MARK: Post Duration Keys
    struct PostDurationKeys {
        static let TwoHours = NSLocalizedString("2 Hours", comment: "")
        static let FourHours = NSLocalizedString("4 Hours", comment: "")
        static let EightHours = NSLocalizedString("8 Hours", comment: "")
    }
    
    // MARK: Post Duration Values
    struct PostDurationValues {
        static let TwoHours = 2
        static let FourHours = 4
        static let EightHours = 8
    }
    
    // MARK: Top Up Amount
    struct TopUpAmountKeys {
        static let twentyDollars = NSLocalizedString("$20.00", comment: "")
        static let fortyDollars = NSLocalizedString("$40.00", comment: "")
        static let sixtyDollars = NSLocalizedString("$60.00", comment: "")
        static let eightyDollars = NSLocalizedString("$80.00", comment: "")
        
    }
    
    struct TopUpAmountValues {
        static let twentyDollars = 20.00
        static let fortyDollars = 40.00
        static let sixtyDollars = 60.00
        static let eightyDollars = 80.00
        
    }
    
    
    
}
