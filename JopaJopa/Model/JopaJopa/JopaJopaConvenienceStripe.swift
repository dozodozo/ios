//
//  JopaJopaConvenienceStripe.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 5/9/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import Foundation

extension JopaJopaClient {
    
    // MARK : Called after card details sent to Stripe and token is returned; the token is used to create a stripe customer
    
    func createStripeCustomerAndCharge(jsonBody: String, _ completionHandlerForAddCardAndCreateCustomer: @escaping(_ customerID: String?, _ error: NSError?) -> Void) -> Void{
        
        //get accessToken and user id from UserDefaults
        let accessToken = JopaJopaClient.sharedInstance().getUserAccessToken()
        let id = JopaJopaClient.sharedInstance().getUserID()
        
        /* 1. Specify parameters, method (if has {key}), and HTTP body (if POST) */
        
        let parameters = [JopaJopaClient.ParameterKeys.AccessToken: accessToken]
        var mutableMethod: String = JopaJopaClient.APIResources.CreateStripeCustomer
        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.ID, value:id)!
       
        //call api
        JopaJopaClient.sharedInstance().taskForPostMethod(mutableMethod, parameters as [String: AnyObject], jsonBody: jsonBody){ result, error in
           
            if error == nil {
                let data = result![JopaJopaClient.JSONReponseKeys.Data] as! [String: String]
                let customerID = data[JopaJopaClient.JSONReponseKeys.ID]
                completionHandlerForAddCardAndCreateCustomer(customerID, nil)
            } else {
                completionHandlerForAddCardAndCreateCustomer(nil, error)
            }
        }
        
    }
    
    // MARK : Get list of charges - payment history
    
    func getListOfCharges(_ completionHandlerForGetListOfCharges: @escaping(_ charges: [JopaJopaStripeCharge]?, _ error: NSError?) -> Void) -> Void{
        
        //get accessToken and user id from UserDefaults
        let accessToken = JopaJopaClient.sharedInstance().getUserAccessToken()
        let id = JopaJopaClient.sharedInstance().getUserID()
        
        /* 1. Specify parameters, method (if has {key}), and HTTP body (if POST) */
        
        let parameters = [JopaJopaClient.ParameterKeys.AccessToken: accessToken]
        var mutableMethod: String = JopaJopaClient.APIResources.GetCustomerListOfCharges
        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.ID, value:id)!
        
        //call api
        JopaJopaClient.sharedInstance().taskForGetMethod(mutableMethod, parameters as [String: AnyObject]){ result, error in
            var charges = [JopaJopaStripeCharge]()
            if error == nil {
                let data = result![JopaJopaClient.JSONReponseKeys.Data] as! [[String: AnyObject]]
                data.forEach{item in data
                    charges.append(JopaJopaStripeCharge(dict: item))
                }
                completionHandlerForGetListOfCharges(charges, nil)
            } else {
                completionHandlerForGetListOfCharges(nil, error)
            }
        }
    }
    
}
