//
//  JopaJopaConveniencePlaces.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 5/7/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import Foundation
import PromiseKit

// MARK: Convenience Methods for managing places

extension JopaJopaClient {
    
    // MARK: Get Posts
    
    func getPosts(_ completionHandlerForGetPosts: @escaping(_ posts: [JopaJopaPost]?, _ error: NSError?) -> Void) -> Void {
        
        //get accessToken and user id from UserDefaults
        let accessToken = JopaJopaClient.sharedInstance().getUserAccessToken()
        let id = JopaJopaClient.sharedInstance().getUserID()
        
        /* 1. Specify parameters, method (if has {key}), and HTTP body (if POST) */
        
        let parameters = [JopaJopaClient.ParameterKeys.AccessToken: accessToken]
        var mutableMethod: String = JopaJopaClient.APIResources.UserPosts
        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.ID, value:id)!
        
        
        /* 2. Make the request */
        JopaJopaClient.sharedInstance().taskForGetMethod(mutableMethod, parameters as [String: AnyObject]){result, error in
            
            if let error = error{
                completionHandlerForGetPosts(nil, error)
            } else {
                var posts : [JopaJopaPost] = [JopaJopaPost]()
                let data = result?[JopaJopaClient.JSONReponseKeys.Data]  as! [[String:AnyObject]]
                for post in data {
                    posts.append(JopaJopaPost(dict: post))
                }
                completionHandlerForGetPosts(posts, nil)
                
            }
            
        }
        
    }
    
    
    // MARK: Add Post
    
    func addPost(jsonBody: String) -> Promise<(id: String, url: URL)> {
        
        //get accessToken and user id from UserDefaults
        let accessToken = JopaJopaClient.sharedInstance().getUserAccessToken()
        let id = JopaJopaClient.sharedInstance().getUserID()
        
        /* 1. Specify parameters, method (if has {key}), and HTTP body (if POST) */
        
        let parameters = [JopaJopaClient.ParameterKeys.AccessToken: accessToken]
        var mutableMethod: String = JopaJopaClient.APIResources.UserPosts
        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.ID, value:id)!
        
        return Promise{ seal in
                JopaJopaClient.sharedInstance().taskForPostMethod(mutableMethod, parameters as [String: AnyObject], jsonBody: jsonBody){ result, error in
                    if (error != nil){
                        seal.reject(error!)
                    } else {
                        if let result = result as? [String:AnyObject] {
                            print(result)
                            let data = result[JopaJopaClient.JSONReponseKeys.Data]  as! [String:AnyObject]
                            let url = URL(string: data[JopaJopaClient.JSONReponseKeys.PostImageURL] as! String)!
                            let id = data[JopaJopaClient.JSONReponseKeys.ID] as! String
                            seal.fulfill((id, url))
                        }
                    }
                }
            }
    }
    
    // MARK: Remove dateExpirey field from posts in db
    
    func postRemoveDateExpiry(jsonBody: String, postID: String) -> Promise<Bool> {
        
        //get accessToken and user id from UserDefaults
        let accessToken = JopaJopaClient.sharedInstance().getUserAccessToken()
        let id = JopaJopaClient.sharedInstance().getUserID()
        
        /* 1. Specify parameters, method (if has {key}), and HTTP body (if POST) */
        
        let parameters = [JopaJopaClient.ParameterKeys.AccessToken: accessToken]
        var mutableMethod: String = JopaJopaClient.APIResources.UserPostDetails
        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.ID, value:id)!
        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.PostID, value:postID)!
        
        return Promise{ seal in
            JopaJopaClient.sharedInstance().taskForPatchMethod(mutableMethod, parameters as [String: AnyObject], jsonBody: jsonBody){ result, error in
                if (error != nil){
                    seal.reject(error!)
                } else {
                    seal.fulfill(true)
                }
            }
        }
    }
    
    

    func updatePost(jsonBody: String, postID: String) -> Promise<(postID: String, url: URL)> {
        
        //get accessToken and user id from UserDefaults
        let accessToken = JopaJopaClient.sharedInstance().getUserAccessToken()
        let id = JopaJopaClient.sharedInstance().getUserID()
        
        /* 1. Specify parameters, method (if has {key}), and HTTP body (if POST) */
        
        let parameters = [JopaJopaClient.ParameterKeys.AccessToken: accessToken]
        var mutableMethod: String = JopaJopaClient.APIResources.UserPostDetails
        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.ID, value:id)!
         mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.PostID, value: postID)!
        
        return Promise{ seal in
            JopaJopaClient.sharedInstance().taskForPutMethod(mutableMethod, parameters as [String: AnyObject], jsonBody: jsonBody){ result, error in
                if (error != nil){
                    seal.reject(error!)
                } else {
                    if let result = result as? [String:AnyObject] {
                        let data = result[JopaJopaClient.JSONReponseKeys.Data]  as! [String:AnyObject]
                        let url = URL(string: data[JopaJopaClient.JSONReponseKeys.PostImageURL] as! String)!
                        let postID = data[JopaJopaClient.JSONReponseKeys.PostID] as! String
                        seal.fulfill((postID, url))
                    }
                }
            }
        }
    }
    
    
    // MARK: Publish Unpublish Post
    
    func postPublishUnpublish(jsonBody: String, postID: String) -> Promise<Bool> {
        
        //get accessToken and user id from UserDefaults
        let accessToken = JopaJopaClient.sharedInstance().getUserAccessToken()
        let id = JopaJopaClient.sharedInstance().getUserID()
        
        /* 1. Specify parameters, method (if has {key}), and HTTP body (if POST) */
        
        let parameters = [JopaJopaClient.ParameterKeys.AccessToken: accessToken]
        var mutableMethod: String = JopaJopaClient.APIResources.UserPostPublishUnpublish
        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.ID, value:id)!
        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.PostID, value:postID)!
        
        return Promise{ seal in
            JopaJopaClient.sharedInstance().taskForPostMethod(mutableMethod, parameters as [String: AnyObject], jsonBody: jsonBody){ result, error in
                if (error != nil){
                    seal.reject(error!)
                } else {
                    seal.fulfill(true)
                }
            }
        }
    }
    
    
    
    // MARK: Delete Post
  
    func deletePost(postID: String, _ completionHandlerForDeletePlace: @escaping(_ error: NSError?) -> Void) -> Void{
        
        //get accessToken and user id from UserDefaults
        let accessToken = JopaJopaClient.sharedInstance().getUserAccessToken()
        let id = JopaJopaClient.sharedInstance().getUserID()
        
        /* 1. Specify parameters, method (if has {key}), and HTTP body (if POST) */
        
        let parameters = [JopaJopaClient.ParameterKeys.AccessToken: accessToken]
        var mutableMethod: String = JopaJopaClient.APIResources.UserPostDetails
        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.ID, value:id)!
        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.PostID, value: postID)!
        
        JopaJopaClient.sharedInstance().taskForDeleteMethod(mutableMethod, parameters as [String: AnyObject], jsonBody: ""){result, error in
            if (error != nil){
                completionHandlerForDeletePlace(error)
            } else {
                completionHandlerForDeletePlace(nil)
            }
        }
    }
    
    
    // MARK: Get Posts
    
    func getPublishedPosts(lat:Double, long:Double, lastDateCreated: String, postCategory:String, completionHandlerForGetPublishedPosts: @escaping(_ posts: [JopaJopaPublishedPost]?, _ error: NSError?) -> Void) -> Void {
        

        
        /* 1. Specify parameters, method (if has {key}), and HTTP body (if POST) */
        
        let parameters = [JopaJopaClient.ParameterKeys.Latitude: lat, JopaJopaClient.ParameterKeys.Longitude:long, JopaJopaClient.ParameterKeys.LastDateCreated : lastDateCreated, JopaJopaClient.ParameterKeys.PostCategory:postCategory] as [String : AnyObject]
        let mutableMethod: String = JopaJopaClient.APIResources.PublishedPosts
//        mutableMethod = substituteKeyInMethod(mutableMethod, key: JopaJopaClient.URLKeys.ID, value:id)!
        
        
        /* 2. Make the request */
        JopaJopaClient.sharedInstance().taskForGetMethod(mutableMethod, parameters as [String: AnyObject]){result, error in
            
            if error != nil {
                completionHandlerForGetPublishedPosts(nil, error)
            } else {
                var publisehdPosts : [JopaJopaPublishedPost] = [JopaJopaPublishedPost]()
                let data = result?[JopaJopaClient.JSONReponseKeys.Data]  as! [[String:AnyObject]]
                for publishedPost in data {
                    publisehdPosts.append(JopaJopaPublishedPost(dict: publishedPost))
                }
                
                completionHandlerForGetPublishedPosts(publisehdPosts, nil)
                
            }
            
        }
        
    }
    
    
    
}
