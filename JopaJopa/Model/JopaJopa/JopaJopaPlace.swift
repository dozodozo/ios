
//
//  JopaJopaPlace.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 5/7/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import Foundation

// MARK: JopaJopaPlace

class JopaJopaPlace: Codable {
    
    // MARK: Structure Location For Address
    struct Location: Codable {
        var address: String?
        var country: String?
        
        init(address: String?, country: String?) {
            self.address = address
            self.country = country
        }
    }
    
    // MARK: Properties
    var placeID: String?
    var userID: String?
    var placeName: String?
    var placeLocation: Location?
    var placeGeoLocation : [Double]?
    var placePhoneNo: String?
    var placeImageURL : String?
    
    // MARK: Initializer
    
    init(placeID: String? = nil, userID: String? = nil, placeName: String? = nil, placeLocation: Location? = nil, placeGeoLocation: [Double]? = nil, placePhoneNo: String? = nil, placeImageURL: String? = nil ) {
        
        self.placeID = placeID
        self.userID = userID
        self.placeName = placeName?.html2AttributedString?.string
        self.placeLocation = placeLocation
        self.placeGeoLocation = placeGeoLocation
        self.placePhoneNo = placePhoneNo
        self.placeImageURL = placeImageURL
        
    }
    
    //for parsing data
    init (dict: [String : AnyObject]) {
        self.placeID = dict[JopaJopaClient.JSONReponseKeys.MONGO_ID] as! String?
        self.userID = dict[JopaJopaClient.JSONReponseKeys.UserID] as! String?
        self.placeName = (dict[JopaJopaClient.JSONReponseKeys.PlaceName] as! String).html2AttributedString?.string
        let address =  (dict[JopaJopaClient.JSONReponseKeys.PlaceLocation]?[JopaJopaClient.JSONReponseKeys.PlaceAddress] as! String).html2AttributedString?.string
        let country =  (dict[JopaJopaClient.JSONReponseKeys.PlaceLocation]?[JopaJopaClient.JSONReponseKeys.PlaceCountry] as! String).html2AttributedString?.string
        self.placeLocation = Location(address: address, country: country)
        
        let placeGeoLocationObject = dict[JopaJopaClient.JSONReponseKeys.PlaceGeoLocation] as! [String:AnyObject]
        self.placeGeoLocation = placeGeoLocationObject["coordinates"] as! [Double]?

        
        self.placePhoneNo = dict[JopaJopaClient.JSONReponseKeys.PlacePhoneNo] as! String?
        self.placeImageURL = dict[JopaJopaClient.JSONReponseKeys.PlaceImageURL] as! String?
        
    }
    
    
    
}
