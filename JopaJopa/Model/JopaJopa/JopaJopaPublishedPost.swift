//
//  PublishedPost.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 27/8/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import Foundation

class JopaJopaPublishedPost: Codable {
    
    // MARK: Structure Location For Address
    struct Location: Codable {
        var address: String?
        var country: String?
        
        init(address: String?, country: String?) {
            self.address = address
            self.country = country
        }
    }
    
    // MARK: Properties
    var postTitle: String?
    var postText: String?
    var postImageURL: String?
    var placeName: String?
    var placeLocation: Location?
    var placeGeoLocation : [Double]?
    var placePhoneNo: String?
    var placeImageURL : String?
    var distance: Double? // distance from users location, returned by db
    var dateCreated: Date?
    
    // MARK: Initializer
    
    init(postTitle: String? = nil, postText: String? = nil, postImageURL: String? = nil,  dateCreated: Date? = nil, placeName: String? = nil, placeLocation: Location? = nil, placeGeoLocation: [Double]? = nil, placePhoneNo: String? = nil, placeImageURL: String? = nil, distance: Double? = nil) {
        
        self.postTitle = postTitle?.html2AttributedString?.string
        self.postText = postText?.html2AttributedString?.string
        self.postImageURL = postImageURL
        self.placeName = placeName?.html2AttributedString?.string
        self.placeLocation = placeLocation
        self.placeGeoLocation = placeGeoLocation
        self.placePhoneNo = placePhoneNo
        self.placeImageURL = placeImageURL
        self.distance = distance
        self.dateCreated = dateCreated
    }
    
    //for parsing data
    init (dict: [String : AnyObject]) {
        self.postTitle = (dict[JopaJopaClient.JSONReponseKeys.PostTitle] as! String).html2AttributedString?.string
        self.postText = (dict[JopaJopaClient.JSONReponseKeys.PostText] as! String).html2AttributedString?.string
        self.postImageURL = dict[JopaJopaClient.JSONReponseKeys.PostImageURL] as! String?
        
        self.placeName = (dict[JopaJopaClient.JSONReponseKeys.PlaceName] as! String).html2AttributedString?.string
        let address =  (dict[JopaJopaClient.JSONReponseKeys.PlaceLocation]?[JopaJopaClient.JSONReponseKeys.PlaceAddress] as! String).html2AttributedString?.string
        let country =  (dict[JopaJopaClient.JSONReponseKeys.PlaceLocation]?[JopaJopaClient.JSONReponseKeys.PlaceCountry] as! String).html2AttributedString?.string
        self.placeLocation = Location(address: address, country: country)
        
        let placeGeoLocationObject = dict[JopaJopaClient.JSONReponseKeys.PlaceGeoLocation] as! [String:AnyObject]
        self.placeGeoLocation = placeGeoLocationObject["coordinates"] as! [Double]?
        
        
        self.placePhoneNo = dict[JopaJopaClient.JSONReponseKeys.PlacePhoneNo] as! String?
        self.placeImageURL = dict[JopaJopaClient.JSONReponseKeys.PlaceImageURL] as! String?
        
        self.distance = dict[JopaJopaClient.JSONReponseKeys.Distance] as! Double?
        
        let date = dict[JopaJopaClient.JSONReponseKeys.PostDateCreated] as! String
//        let isoDateString = date
//        let trimmedIsoString = isoDateString.replacingOccurrences(of: "\\.\\d+", with: "", options: .regularExpression)
//        let formatter = ISO8601DateFormatter()
//        self.dateCreated = formatter.date(from: trimmedIsoString)
         self.dateCreated = TimeDistanceHelper.isoTrimmedDate(isoDateString: date)
        
        
    }
    
    
}
