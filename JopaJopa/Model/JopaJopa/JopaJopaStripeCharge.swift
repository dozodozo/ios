//
//  JopaJopaStripeCharge.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 13/9/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import Foundation

class JopaJopaStripeCharge {
    // MARK: Properties
    
    var chargeID: String?
    var chargeDescription: String?
    var chargeTransactionDate: String?
    var chargeAmount : Double?
    var chargeCurrency : String?
   
    // MARK: Initializer
    
    //for parsing data
    init (dict: [String : AnyObject]) {
        self.chargeID = dict[JopaJopaClient.JSONReponseKeys.ChargeID] as! String?
        self.chargeDescription =  dict[JopaJopaClient.JSONReponseKeys.ChargeDescription] as! String?
        //get date from timestamp
        let unixtimestamp = dict[JopaJopaClient.JSONReponseKeys.ChargeTransactionDate] as! Double
        let date = Date(timeIntervalSince1970: unixtimestamp)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormatter.dateFormat(fromTemplate: "MMddyyy", options: 0, locale: Locale.current)
        dateFormatter.timeStyle = DateFormatter.Style.none //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.short //Set date style
        dateFormatter.timeZone = TimeZone.current
        let localDate = dateFormatter.string(from: date)
        self.chargeTransactionDate = localDate
        
        self.chargeAmount = dict[JopaJopaClient.JSONReponseKeys.ChargeAmount] as! Double?
        self.chargeCurrency = dict[JopaJopaClient.JSONReponseKeys.ChargeCurrency] as! String?
    }
}
