//
//  StringExtension.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 21/8/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import Foundation

extension String {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: Data(utf8), options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print(error)
            return nil
        }
    }
}
