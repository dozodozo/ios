//
//  JopaJopaPost.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 23/7/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//
/*
 
 */
import Foundation

class JopaJopaPost: Codable {
    
    // MARK: Properties
    var postID: String?
    var userID: String?
    var postTitle: String?
    var postText: String?
    var postImageURL: String?
    var postIsPublished: Bool? // Published or Not Published
    var postCategory: String?
    var postType: String? // Short or Long
    var placeIDs: [String]?
    var dateCreated: Date?
    var datePublished: Date?
    var dateUnPublish: Date?
    var postTimeRemaining: Int? // time remainng before post expires in seconds
   
    // MARK: Initializer
    
    init(postID: String? = nil, userID: String? = nil, postTitle: String? = nil, postText: String? = nil, postImageURL: String? = nil, postIsPublished: Bool? = nil, postCategory: String? = nil, postType: String? = nil, placeIDs: [String]? = nil, dateCreated: Date? = nil, datePublished: Date? = nil, dateUnPublish: Date? = nil, postTimeRemaining: Int? = nil) {
        
        self.postID = postID
        self.userID = userID
        self.postTitle = postTitle?.html2AttributedString?.string
        self.postText = postText?.html2AttributedString?.string
        self.postImageURL = postImageURL
        self.postIsPublished = postIsPublished
        self.postCategory = postCategory
        self.postType = postType
        self.placeIDs = placeIDs
        self.dateCreated = dateCreated
        self.datePublished = datePublished
        self.dateUnPublish = dateUnPublish
        self.postTimeRemaining = postTimeRemaining
    }
    
    //for parsing data
    init (dict: [String : AnyObject]) {
        self.postID = dict[JopaJopaClient.JSONReponseKeys.MONGO_ID] as! String?
        self.userID = dict[JopaJopaClient.JSONReponseKeys.UserID] as! String?
//        self.postTitle = dict[JopaJopaClient.JSONReponseKeys.PostTitle] as! String?
//        self.postText = dict[JopaJopaClient.JSONReponseKeys.PostText] as! String?
        self.postTitle = (dict[JopaJopaClient.JSONReponseKeys.PostTitle] as! String).html2AttributedString?.string
        self.postText = (dict[JopaJopaClient.JSONReponseKeys.PostText] as! String).html2AttributedString?.string
        self.postImageURL = dict[JopaJopaClient.JSONReponseKeys.PostImageURL] as! String?
        self.postIsPublished = dict[JopaJopaClient.JSONReponseKeys.PostIsPublished] as! Bool?
        self.postCategory = dict[JopaJopaClient.JSONReponseKeys.PostCategory] as! String?
        self.postType = dict[JopaJopaClient.JSONReponseKeys.PostType] as! String?
        self.placeIDs = dict[JopaJopaClient.JSONReponseKeys.PostPlaceIDs] as! [String]?
        
        self.dateCreated = TimeDistanceHelper.isoTrimmedDate(isoDateString: dict[JopaJopaClient.JSONReponseKeys.PostDateCreated] as! String)
        self.datePublished = TimeDistanceHelper.isoTrimmedDate(isoDateString: dict[JopaJopaClient.JSONReponseKeys.PostDatePublished] as! String)
        self.dateUnPublish = TimeDistanceHelper.isoTrimmedDate(isoDateString: dict[JopaJopaClient.JSONReponseKeys.PostDateUnPublish] as! String)
        self.postTimeRemaining = dict[JopaJopaClient.JSONReponseKeys.PostTimeRemaining] as! Int?
        
    }
    
}





