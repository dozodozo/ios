//
//  JopaJopaClient.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 20/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

// MARK : Extension CLLocationManagerDelegate

extension JopaJopaClient: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //print("location updated")
        self.userLocation = locations.last!
        
        //set initial search location
        if (searchLocation == nil){
            searchLocation = userLocation
        }
    }
    
    //handle location update error
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.userLocation = nil
    }
    
}

// MARK: JopaJopaClient

class JopaJopaClient: NSObject {
    
    // MARK: Properties
    var user: JopaJopaUser?
    
    // location manager
    let locationManager = CLLocationManager()
    var userLocation: CLLocation? //users actual location
    var searchLocation : CLLocation? //location being searched by user, could be same as userLocation
    var searchAddress: String? = NSLocalizedString("Your Location", comment: "")
    
    // shared url session
    var session = URLSession.shared
    
    // authentication state
    var isUserLoggedIn: Bool? {
        get {
            return UserDefaults.standard.value(forKey: "isUserLoggedIn") as? Bool
        }
        set(newIsUserLoggedIn) {
            UserDefaults.standard.set(newIsUserLoggedIn, forKey: "isUserLoggedIn")
        }
    }
    
    // sharedInstance
    private static var sharedJopaJopaClient : JopaJopaClient = {
        let jopajopaClient = JopaJopaClient()
        jopajopaClient.locationManager.delegate = jopajopaClient
        jopajopaClient.locationManager.startMonitoringSignificantLocationChanges()

        return jopajopaClient
    }()
    
   
    // MARK : Initializer
    
    private override init() {
        super.init()
    }
    
    // MARK : Shared Instance JopaJopa Client
    
    class func sharedInstance() -> JopaJopaClient {
        return sharedJopaJopaClient
    }
    
    // MARK : GET
    
    func taskForGetMethod(_ APIResource: String? = nil, _ parameters: [String:AnyObject]? = nil, completionHandlerForGet: @escaping (_ result: AnyObject?, _ error: NSError?) -> Void) -> Void{
        
        /* Build URL */
        let url = getJopaJopaURLFromParameters(APIResource, parameters)
        
        /* Configure request */
        let request = NSMutableURLRequest(url: url)
        
        /* Make Request */
        let task = JopaJopaClient.sharedInstance().session.dataTask(with: request as URLRequest){ data, response,error in
            
            // send error to completionHandlerForGet
            func sendError(_ errorType: String) {
                print("URL SESSION Error Type: \(errorType)")
                let error = NSError(domain: "TaskForGet", code: 1, userInfo : ["JopaJopaErrorType" : errorType])
                completionHandlerForGet(nil, error)
            }
            
            /* GUARD: Was there an error? */
            guard error == nil else {
                sendError(JopaJopaErrorType.SESSION_ERROR)
                return
            }
            
            /* GUARD: Is there a status code? */
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode else {
                sendError(JopaJopaErrorType.NO_STATUS_CODE_ERROR)
                return
            }
            
            
            /* GUARD: Was there any data returned? */
            guard let data = data else {
                sendError(JopaJopaErrorType.DATA_ERROR)
                return
            }
            
            /* Parse the data */
            let parsedData: AnyObject!
            do {
                parsedData = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as AnyObject
                
            }
            catch {
                //sendError(JopaJopaErrors.JSONParsingError)
                sendError(JopaJopaErrorType.JSON_PARSING_ERROR)
                return
            }
            
            //check if status code is other than 2xx
            guard (statusCode < 300) else {
                //extract error type sent by JopaJopa API
                let data = parsedData  as! [String:AnyObject]
                let errorType = data["type"]! as! String
                print("API Error Type: \(errorType)")
                sendError(errorType)
                return
            }
            
            /* Use the data */
            completionHandlerForGet(parsedData, nil)
            
        }
        
        task.resume()
    }
    
    
    
    // MARK : POST
    
    func taskForPostMethod(_ APIResource: String? = nil, _ parameters: [String:AnyObject]? = nil, jsonBody: String, completionHandlerForPost: @escaping (_ result: AnyObject?, _ error: NSError?) -> Void) -> Void{
        
        /* Build URL */
        let url = getJopaJopaURLFromParameters(APIResource, parameters)

        /* Configure request */
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonBody.data(using: String.Encoding.utf8)
        
        /* Make Request */
        let task = JopaJopaClient.sharedInstance().session.dataTask(with: request as URLRequest){ data, response, error in
            
            // send error to completionHandlerForPOST
            func sendError(_ errorType: String) {
                print("URL SESSION Error Type: \(errorType)")
                let error = NSError(domain: "TaskForPost", code: 1, userInfo : ["JopaJopaErrorType" : errorType])
                completionHandlerForPost(nil, error)
            }
            
            /* GUARD: Was there an error? */
            guard error == nil else {
                sendError(JopaJopaErrorType.SESSION_ERROR)
                return
            }
            
            /* GUARD: Is there a status code? */
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode else {
                sendError(JopaJopaErrorType.NO_STATUS_CODE_ERROR)
                return
            }
            
            
            /* GUARD: Was there any data returned? */
            guard let data = data else {
                sendError(JopaJopaErrorType.DATA_ERROR)
                return
            }
            
            /* Parse the data */
            let parsedData: AnyObject!
            do {
                parsedData = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as AnyObject
                
            }
            catch {
                //sendError(JopaJopaErrors.JSONParsingError)
                sendError(JopaJopaErrorType.JSON_PARSING_ERROR)
                return
            }
            
            //check if status code is other than 2xx
            guard (statusCode < 300) else {
                //extract error type sent by JopaJopa API
                let data = parsedData  as! [String:AnyObject]
                let errorType = data["type"]! as! String
                print("API Error Type: \(errorType)")
                sendError(errorType)
                return
            }
            
            print("parsedData: \(parsedData)")
            /* Use the data */
            completionHandlerForPost(parsedData, nil)
            
        }
        
        task.resume()
        
    }
    
    // MARK : PUT
    
    func taskForPutMethod(_ APIResource: String? = nil, _ parameters: [String:AnyObject]? = nil, jsonBody: String, completionHandlerForPut: @escaping (_ result: AnyObject?, _ error: NSError?) -> Void) -> Void{
        
        /* Build URL */
        let url = getJopaJopaURLFromParameters(APIResource, parameters)
        
        /* Configure request */
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "PUT"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonBody.data(using: String.Encoding.utf8)
        
        /* Make Request */
        let task = JopaJopaClient.sharedInstance().session.dataTask(with: request as URLRequest){ data, response,error in
            
            //send error
            func sendError(_ errorType: String) {
                print("URL SESSION Error Type: \(errorType)")
                let error = NSError(domain: "TaskForPut", code: 1, userInfo : ["JopaJopaErrorType" : errorType])
                completionHandlerForPut(nil, error)
            }
            
            /* GUARD: Was there an error? */
            guard error == nil else {
                sendError(JopaJopaErrorType.SESSION_ERROR)
                return
            }
            
            /* GUARD: Is there a status code? */
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode else {
                sendError(JopaJopaErrorType.NO_STATUS_CODE_ERROR)
                return
            }
            
            
            /* GUARD: Was there any data returned? */
            guard let data = data else {
                sendError(JopaJopaErrorType.DATA_ERROR)
                return
            }
            
            /* Parse the data */
            let parsedData: AnyObject!
            do {
                parsedData = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as AnyObject
                
            }
            catch {
                //sendError(JopaJopaErrors.JSONParsingError)
                sendError(JopaJopaErrorType.JSON_PARSING_ERROR)
                return
            }
            
            //check if status code is other than 2xx
            guard (statusCode < 300) else {
                //extract error type sent by JopaJopa API
                let data = parsedData  as! [String:AnyObject]
                let errorType = data["type"]! as! String
                print("API Error Type: \(errorType)")
                sendError(errorType)
                return
            }
            
            /* Use the data */
            completionHandlerForPut(parsedData, nil)
            
        }
        
        task.resume()
        
    }
    
    // MARK: PATCH
    
    func taskForPatchMethod(_ APIResource: String? = nil, _ parameters: [String:AnyObject]? = nil, jsonBody: String, completionHandlerForPatch: @escaping (_ result: AnyObject?, _ error: NSError?) -> Void) -> Void{
        
        /* Build URL */
        let url = getJopaJopaURLFromParameters(APIResource, parameters)
        
        /* Configure request */
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "PATCH"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonBody.data(using: String.Encoding.utf8)
        
        /* Make Request */
        let task = JopaJopaClient.sharedInstance().session.dataTask(with: request as URLRequest){ data, response,error in
            
            func sendError(_ errorType: String) {
                print("URL SESSION Error Type: \(errorType)")
                let error = NSError(domain: "TaskForPatch", code: 1, userInfo : ["JopaJopaErrorType" : errorType])
                completionHandlerForPatch(nil, error)
            }
            
            /* GUARD: Was there an error? */
            guard error == nil else {
                sendError(JopaJopaErrorType.SESSION_ERROR)
                return
            }
            
            /* GUARD: Is there a status code? */
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode else {
                sendError(JopaJopaErrorType.NO_STATUS_CODE_ERROR)
                return
            }
            
            
            /* GUARD: Was there any data returned? */
            guard let data = data else {
                sendError(JopaJopaErrorType.DATA_ERROR)
                return
            }
            
            /* Parse the data */
            let parsedData: AnyObject!
            do {
                parsedData = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as AnyObject
                
            }
            catch {
                //sendError(JopaJopaErrors.JSONParsingError)
                sendError(JopaJopaErrorType.JSON_PARSING_ERROR)
                return
            }
            
            //check if status code is other than 2xx
            guard (statusCode < 300) else {
                //extract error type sent by JopaJopa API
                let data = parsedData  as! [String:AnyObject]
                let errorType = data["type"]! as! String
                sendError(errorType)
                return
            }
            
            //use the data
            completionHandlerForPatch(parsedData, nil)
            
        }
        
        task.resume()
        
    }
    
    // MARK: DELETE
    
    func taskForDeleteMethod(_ APIResource: String? = nil, _ parameters: [String:AnyObject]? = nil, jsonBody: String, completionHandlerForDelete: @escaping (_ result: AnyObject?, _ error: NSError?) -> Void) -> Void{
        
        /* Build URL */
        let url = getJopaJopaURLFromParameters(APIResource, parameters)
        
        /* Configure request */
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "DELETE"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonBody.data(using: String.Encoding.utf8)
        
        /* Make Request */
        let task = JopaJopaClient.sharedInstance().session.dataTask(with: request as URLRequest){ data, response,error in
            
            // send error to completionHandlerForDelete
            func sendError(_ errorType: String) {
                print("URL SESSION Error Type: \(errorType)")
                let error = NSError(domain: "TaskForDelete", code: 1, userInfo : ["JopaJopaErrorType" : errorType])
                completionHandlerForDelete(nil, error)
            }
            
            /* GUARD: Was there an error? */
            guard error == nil else {
                sendError(JopaJopaErrorType.SESSION_ERROR)
                return
            }
            
            /* GUARD: Is there a status code? */
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode else {
                sendError(JopaJopaErrorType.NO_STATUS_CODE_ERROR)
                return
            }
            
            
            /* GUARD: Was there any data returned? */
            guard let data = data else {
                sendError(JopaJopaErrorType.DATA_ERROR)
                return
            }
            
            /* Parse the data */
            let parsedData: AnyObject!
            do {
                parsedData = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as AnyObject
                
            }
            catch {
                //sendError(JopaJopaErrors.JSONParsingError)
                sendError(JopaJopaErrorType.JSON_PARSING_ERROR)
                return
            }
            
            //check if status code is other than 2xx
            guard (statusCode < 300) else {
                //extract error type sent by JopaJopa API
                let data = parsedData  as! [String:AnyObject]
                let errorType = data["type"]! as! String
                sendError(errorType)
                return
            }
            
            /* Use the data */
            completionHandlerForDelete(parsedData, nil)
            
        }
        
        task.resume()
        
    }
    
    
    
    // MARK: HELPERS
    
    //get userID and AccessKey that was stored when user logged
    func getUserID () -> String {
        return UserDefaults.standard.value(forKey: "id")! as! String
    }
    
    func getUserAccessToken() -> String {
        return UserDefaults.standard.value(forKey: "accessToken") as! String
    }
    
    //log out user
    func logoutUser() {
        UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
    }
    
    //isLocationService Ok - true if userlocation has been set
    func isLocationServiceOK() -> Bool {
        return userLocation != nil
    }
    
    
    // substitute the key for the value that is contained within the method name
    func substituteKeyInMethod(_ method: String, key: String, value: String) -> String? {
        if method.range(of: "{\(key)}") != nil {
            return method.replacingOccurrences(of: "{\(key)}", with: value)
        } else {
            return nil
        }
    }
    
    //build url
    private func getJopaJopaURLFromParameters(_ APIResource: String? = nil, _ parameters: [String:AnyObject]? = nil) -> URL{
        
        var components = URLComponents()
        components.scheme = JopaJopaClient.Constants.APIScheme
        components.host = JopaJopaClient.Constants.APIHost
        components.port = JopaJopaClient.Constants.APIPort
        components.path = JopaJopaClient.Constants.APIPath + (APIResource ?? "")
               
        if let parameters = parameters {
            components.queryItems = [URLQueryItem]()
            for (key, value) in parameters {
                let queryItem = URLQueryItem(name: key, value: "\(value)")
                components.queryItems?.append(queryItem)
            }
        }
        
        return components.url!
        
    }
    
    
    //get an alert controller for handling error messages
    func getAlertControllerForErrors(_ error: NSError, _ task: String) -> UIAlertController {
        
        let alertController = UIAlertController(title: "", message: "", preferredStyle: .alert)
        let closeAction = UIAlertAction(title: "Close", style: .cancel, handler: nil)
        //let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        //let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
       
        switch (error.userInfo["JopaJopaErrorType"] as! String){
        case JopaJopaErrorType.AUTHENTICATION_FAILURE:
            alertController.title = NSLocalizedString("You have been signed out. Please login again to continue", comment: "")
            break
        case JopaJopaErrorType.ACCOUNT_SUSPENDED:
            alertController.title = NSLocalizedString("Your account has been suspended", comment: "")
            alertController.message = NSLocalizedString("Please contact us for more details", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaErrorType.INVALID_EMAIL_PASSWORD:
            alertController.title = NSLocalizedString("Problem Logging In", comment: "")
            alertController.message = NSLocalizedString("Please check e-mail and password. Password must be at least 8 characters long", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaErrorType.INCORRECT_EMAIL_PASSWORD:
            alertController.title = NSLocalizedString("Problem Logging In", comment: "")
            alertController.message = NSLocalizedString("Incorrect E-mail or Password", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaErrorType.PASSWORD_INCORRECT:
            alertController.title = NSLocalizedString("Incorrect Password", comment: "")
            alertController.message = NSLocalizedString("Please check your password", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaErrorType.PASSWORD_INVALID:
            alertController.title = NSLocalizedString("Invalid Password", comment: "")
            alertController.message = NSLocalizedString("Please check your password. Password must be at least 8 characters long", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaErrorType.PASSWORDS_DONT_MATCH:
            alertController.title = NSLocalizedString("Password Don't Match", comment: "")
            alertController.message = NSLocalizedString("Please make sure passwords are the same", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaErrorType.INVALID_FORM_DATA:
            alertController.title = NSLocalizedString("Invalid Data", comment: "")
            alertController.message = NSLocalizedString("Some of the information entered is not valid. Please check them and try again", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaErrorType.EMAIL_EXISTS:
            alertController.title = NSLocalizedString("E-mail Already Exists", comment: "")
            alertController.message = NSLocalizedString("Please use a different e-mail", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaErrorType.EMAIL_INVALID:
            alertController.title = NSLocalizedString("E-mail Invalid", comment: "")
            alertController.message = NSLocalizedString("Please use a proper e-mail", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaErrorType.PLACE_IN_USE_UPDATE_FORBIDDEN:
            alertController.title = NSLocalizedString("Problem Updating Place", comment: "")
            alertController.message = NSLocalizedString("Place is being used in a published post. Please remove place from post first", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaErrorType.PLACE_IN_USE_DELETE_FORBIDDEN:
            alertController.title = NSLocalizedString("Problem Deleting Place", comment: "")
            alertController.message = NSLocalizedString("Place is being used in post. Please remove place from post first", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaErrorType.POST_PUBLISHED_UPDATE_FORBIDDEN:
            alertController.title = NSLocalizedString("Problem Updating Post", comment: "")
            alertController.message = NSLocalizedString("Post is already published. Please unpublish post to update it", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaErrorType.POST_PUBLISHED_DELETE_FORBIDDEN:
            alertController.title = NSLocalizedString("Problem Deleting Post", comment: "")
            alertController.message = NSLocalizedString("Post is already published. Please unpublish post to delete it", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaErrorType.STRIPE_CARD_DECLINED:
            alertController.title = NSLocalizedString("Problem Adding Credit", comment: "")
            alertController.message = NSLocalizedString("There was a problem adding credit. Please check your card", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaErrorType.INSUFFICIENT_CREDIT:
            alertController.title = NSLocalizedString("Insufficient Credit", comment: "")
            alertController.message = NSLocalizedString("You do not have enough credits to publish the post. Please top up your account", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaErrorType.MEDIA_SOURCE_NOT_AVAILABLE:
            alertController.title = NSLocalizedString("Media Source Not Available", comment: "")
            alertController.message = NSLocalizedString("Please check permissions for the app", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaErrorType.CLIENT_ERROR:
            fallthrough
        case JopaJopaErrorType.SERVER_ERROR:
            fallthrough
        case JopaJopaErrorType.DATABASE_ERROR:
            fallthrough
        case JopaJopaErrorType.SESSION_ERROR:
            fallthrough
        case JopaJopaErrorType.DATA_ERROR:
            fallthrough
        case JopaJopaErrorType.JSON_PARSING_ERROR:
            fallthrough
        case JopaJopaErrorType.NO_STATUS_CODE_ERROR:
            fallthrough
        default:
            return getAlertControllerForGeneralErrors(task)
        }
        //return
        return alertController
    }
    
    // returns alert controller to deal with more generic errors
    
    private func getAlertControllerForGeneralErrors(_ task: String) -> UIAlertController{
        let alertController = UIAlertController(title: "", message: "", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let closeAction = UIAlertAction(title: "Close", style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        switch (task){
        case JopaJopaTask.Log_In:
            alertController.title = NSLocalizedString("Problem Logging In", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaTask.Sign_Up:
            alertController.title = NSLocalizedString("Problem Signing Up", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaTask.Get_User_Details:
            alertController.title = NSLocalizedString("Problem Getting Your Details", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaTask.Update_Email:
            alertController.title = NSLocalizedString("Problem Updating E-mail", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaTask.Update_Password:
            alertController.title = NSLocalizedString("Problem Updating Password", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaTask.Reset_Password:
            alertController.title = NSLocalizedString("Problem Resetting Password", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaTask.Update_Details:
            alertController.title = NSLocalizedString("Problem Updating Your Details", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaTask.Delete_Account:
            alertController.title = NSLocalizedString("Problem Deleting Account", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaTask.Get_Places:
            alertController.title = NSLocalizedString("Problem Getting Your Places", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaTask.Get_Place_Details:
            alertController.title = NSLocalizedString("Problem Getting Place Details", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaTask.Add_Place:
            alertController.title = NSLocalizedString("Problem Adding Place", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaTask.Update_Place:
            alertController.title = NSLocalizedString("Problem Updating Place", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaTask.Delete_Place:
            alertController.title = NSLocalizedString("Problem Deleting Place", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaTask.Get_Posts:
            alertController.title = NSLocalizedString("Problem Getting Your Posts", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaTask.Get_Post_Details:
            alertController.title = NSLocalizedString("Problem Getting Post Details", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaTask.Add_Post:
            alertController.title = NSLocalizedString("Problem Adding Post", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaTask.Update_Post:
            alertController.title = NSLocalizedString("Problem Updating Post", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaTask.Delete_Post:
            alertController.title = NSLocalizedString("Problem Deleting Post", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaTask.Publish_Post:
            alertController.title = NSLocalizedString("Problem Publishing Post", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaTask.Unpublish_Post:
            alertController.title = NSLocalizedString("Problem Unpublishing Post", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaTask.Get_Published_Posts:
            alertController.title = NSLocalizedString("Problem Getting Posts", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaTask.Add_Credit:
            alertController.title = NSLocalizedString("Problem Adding Credit", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaTask.Get_Account_Balance:
            alertController.title = NSLocalizedString("Problem Getting Balance", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
            break
        case JopaJopaTask.Get_Payment_History:
            alertController.title = NSLocalizedString("Problem Getting Payment History", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
            break
        default:
            alertController.title = NSLocalizedString("We encountered a problem", comment: "")
            alertController.message = NSLocalizedString("Try again later", comment: "")
            alertController.addAction(closeAction)
        }
        
        return alertController
        
    }
    
    
}


