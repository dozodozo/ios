//
//  AppDelegate.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 6/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit
import GooglePlaces
import Stripe

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    // location manager
    //let locationManager = CLLocationManager()

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        //Google Places
        GMSPlacesClient.provideAPIKey(JopaJopaClient.Constants.GOOGLE_PLACES_KEY)
        
        //Stripe setup
        STPPaymentConfiguration.shared().publishableKey = JopaJopaClient.Constants.STRIPE_KEY
        
        // ask for location permission
       JopaJopaClient.sharedInstance().locationManager.requestAlwaysAuthorization()
        
        //subscribe to location changes - using Visit service
        let authorizationStatus = CLLocationManager.authorizationStatus()
        if authorizationStatus != .authorizedAlways {
            print("User has not authorized access to location information.")
        }
        
        if !CLLocationManager.significantLocationChangeMonitoringAvailable() {
            print(" The service is not available.")
        }
        
        //check if user logged in
        if let isUserLoggedIn = UserDefaults.standard.value(forKey: "isUserLoggedIn") {
            JopaJopaClient.sharedInstance().isUserLoggedIn = isUserLoggedIn as? Bool
        }  else {
            UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
            JopaJopaClient.sharedInstance().isUserLoggedIn = false
        }
        
        // Initialize the window
        window = UIWindow.init(frame: UIScreen.main.bounds)
        
        // Set Background Color of window
        window?.backgroundColor = ViewConstants.BACKGROUND_COLOR
        
        // Allocate memory for an instance of the 'MainViewController' class
        let mainTabViewController = MainTabViewController()
        
        // Set the root view controller of the app's window
        window!.rootViewController = mainTabViewController
        
        // Make the window visible
        window!.makeKeyAndVisible()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        //print("App Will Resign Active")
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        //print("App Did Enter Background")
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        
        if ((self.window?.rootViewController?.childViewControllers[0].childViewControllers[0].navigationController?.childViewControllers.count)! == 1) {
            JopaJopaClient.sharedInstance().searchLocation = JopaJopaClient.sharedInstance().userLocation
            JopaJopaClient.sharedInstance().searchAddress = NSLocalizedString("Your Location", comment: "")
        }
        
//        print("title: \(self.window?.rootViewController?.title)")
//        print("title: \(self.window?.rootViewController?.childViewControllers[0].childViewControllers[0])")
//        print("title: \(self.window?.rootViewController?.childViewControllers[0].childViewControllers[0].navigationController?.childViewControllers)")
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        //print("App Did Become Active")
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}



