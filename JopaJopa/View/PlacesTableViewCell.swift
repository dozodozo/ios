//
//  PlacesTableViewCell.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 10/7/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit

class PlacesTableViewCell: UITableViewCell {
    
    // MARK: Properties
    
    static let imageViewCellHeight = CGFloat(60)
    
    let containerView: UIView = {
        let uiView = UIView()
        //uiView.layer.borderWidth = 0.5
        //uiView.backgroundColor = UIColor(red: 0.98, green: 0.98, blue: 0.98, alpha: 1)
        //uiView.backgroundColor = ViewConstants.LIGHT_TEXT_COLOR
        //uiView.layer.borderColor = ViewConstants.LIGHT_TEXT_COLOR.cgColor
        uiView.translatesAutoresizingMaskIntoConstraints = false
        return uiView
    }()
    
    let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.preferredFont(forTextStyle: .subheadline).bold()
        lbl.numberOfLines = 1
        return lbl
    }()
    
    let detailLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        //lbl.lineBreakMode = .byWordWrapping
        lbl.numberOfLines = 0
        lbl.font = UIFont.preferredFont(forTextStyle: .footnote)
        return lbl
    }()
    
    let imageViewCell: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "defaultImage"))
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 30
        imageView.clipsToBounds = true
        
        return imageView
    }()
    
   
    
    
    // MARK: Constructor
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
        //add views
        self.contentView.addSubview(containerView)
        self.containerView.addSubview(imageViewCell)
        
        self.containerView.addSubview(titleLabel)
        self.containerView.addSubview(detailLabel)
        
        //set up ui layout
        setupUILayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    // MARK: UI Setup
    
    func setupUILayout() {
        
        containerView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 0).isActive = true
        containerView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant:0).isActive = true
        containerView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: 0).isActive = true
        containerView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 0).isActive = true
        
        imageViewCell.centerYAnchor.constraint(equalTo:self.containerView.centerYAnchor).isActive = true
        imageViewCell.leadingAnchor.constraint(equalTo:self.containerView.leadingAnchor, constant:10).isActive = true
        imageViewCell.widthAnchor.constraint(equalToConstant: PlacesTableViewCell.imageViewCellHeight).isActive = true
        imageViewCell.heightAnchor.constraint(equalTo:imageViewCell.widthAnchor).isActive = true
        
        titleLabel.topAnchor.constraint(equalTo: self.containerView.topAnchor, constant: 20).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: self.imageViewCell.trailingAnchor, constant: 10).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor, constant: -10).isActive = true
        
        detailLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5).isActive = true
        detailLabel.leadingAnchor.constraint(equalTo: self.imageViewCell.trailingAnchor, constant: 10).isActive = true
        detailLabel.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor, constant: -10).isActive = true
        detailLabel.bottomAnchor.constraint(equalTo: self.containerView.bottomAnchor, constant:-20).isActive = true
        
        
        
    }
    
}
