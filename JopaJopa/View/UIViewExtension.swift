//
//  UIViewExtension.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 28/8/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func topRounderCorners(radius: Int){
        let maskLayer = CAShapeLayer()
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: radius, height: radius))
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
        
    }
    
    func rotate360Degrees(duration: CFTimeInterval = 0.8) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        let radians = CGFloat.pi / 4
        rotateAnimation.fromValue = radians
        rotateAnimation.toValue = radians + .pi
        rotateAnimation.isRemovedOnCompletion = false
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount=Float.infinity
        self.layer.add(rotateAnimation, forKey: nil)
    }
    
    
    
}
