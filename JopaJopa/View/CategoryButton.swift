//
//  CustomButton.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 6/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit

class CategoryButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        themeBorderedButton()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        themeBorderedButton()
    }
    
    /*
     * Adds bottom border and auto layout
     */
    
    private func themeBorderedButton(){
        self.titleEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        self.titleLabel?.font = UIFont.preferredFont(forTextStyle: .headline)
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 1
        self.layer.borderColor = ViewConstants.BUTTON_COLOR_CATEGORIES.cgColor
        self.translatesAutoresizingMaskIntoConstraints = false
    }

}
