//
//  ViewConstants.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 10/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import Foundation
import UIKit

class ViewConstants {
    
    //background
    static let BACKGROUND_COLOR: UIColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
    static let BACKGROUND_COLOR_DIM = UIColor.lightGray.withAlphaComponent(0.7)
    static let TAB_BAR_BACKGROUND_COLOR = UIColor(red: 0.96, green: 0.96, blue: 0.96, alpha: 1)
    
    //input text fields
    static let TEXT_FIELD_BORDER_COLOR: UIColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.08)
    
    //faint text color
    static let LIGHT_TEXT_COLOR: UIColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
    
    //control button
    static let BUTTON_COLOR_DEFAULT: UIColor = UIColor(red: 0, green: 0.47, blue: 1, alpha: 1)
    static let BUTTON_COLOR_DANGER: UIColor = UIColor(red: 1, green: 0, blue: 0, alpha: 1)
    
    //home page categories
    //static let BUTTON_COLOR_CATEGORIES: UIColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
    static let BUTTON_COLOR_CATEGORIES: UIColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.1)
    
    //small font for messages
    static let SMALL_FONT: CGFloat = 12
    
    //very small font for messages
    static let VERY_SMALL_FONT: CGFloat = 10
}
