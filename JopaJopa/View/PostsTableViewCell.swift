//
//  PostsTableViewCell.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 14/8/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit

class PostsTableViewCell: UITableViewCell {
    
    // MARK: Properties
    let containerViewHeight = CGFloat(225)
    let cellImageViewHeight = CGFloat(125)
    
    //static let selectCellImaveViewHeight = CGFloat(30)
    
    let containerView: UIView = {
        let uiView = UIView()
        uiView.layer.borderWidth = 1.0
        uiView.backgroundColor = UIColor(red: 0.98, green: 0.98, blue: 0.98, alpha: 1)
        //uiView.backgroundColor = ViewConstants.LIGHT_TEXT_COLOR
        uiView.layer.borderColor = UIColor.white.cgColor
        uiView.layer.cornerRadius = 10.0
        uiView.translatesAutoresizingMaskIntoConstraints = false
        return uiView
    }()
    
    let cellImageView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "defaultImage"))
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 5.0
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        return imageView
    }()
    
    let imgViewLocation: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "locationPin"))
        imageView.tintColor = UIColor.blue
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let lblPostLocation: UILabel = {
        let lbl = UILabel()
        lbl.text = "2 places"
        lbl.font = UIFont.preferredFont(forTextStyle: .caption1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let lblPostCategory: UILabel = {
        let lbl = UILabel()
        lbl.text = "Health | Beauty"
        lbl.font = UIFont.preferredFont(forTextStyle: .caption1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let imgViewPostCategory: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "category"))
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let postTitle : UILabel = {
        let lbl = UILabel()
        lbl.text = "Happy Hour"
        lbl.font = UIFont.preferredFont(forTextStyle: .caption1).bold()
        //lbl.adjustsFontForContentSizeCategory = true
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let postText: UILabel = {
        let lbl = UILabel()
        lbl.text = "The world is my oyster and the"
        //lbl.layer.borderWidth = 1.0

       lbl.text = "The world is my oyster and we are all pearls that live in it unit we get pulled out and we don't have anything anymore to do about - come on and lets go away to the hidden mountain"
        lbl.font = UIFont.preferredFont(forTextStyle: .caption2)
        lbl.numberOfLines = 3
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let btnPublishUnpublish: UIButton = {
        let btn = UIButton()
        btn.setTitle("Publish", for: .normal)
        btn.backgroundColor = ViewConstants.TAB_BAR_BACKGROUND_COLOR
        btn.layer.borderColor = ViewConstants.LIGHT_TEXT_COLOR.cgColor
        btn.layer.borderWidth = 1.0
        btn.layer.cornerRadius = 5.0
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitleColor(UIColor.black, for: .normal)
        btn.titleLabel?.font = UIFont.preferredFont(forTextStyle: .subheadline)
        return btn
    }()
    
    let lblTimeRemaining: UILabel = {
        let lbl = UILabel()
        lbl.text = "Time Remaining: 00:00:00"
        //lbl.text = ""
        lbl.font = UIFont.preferredFont(forTextStyle: .caption2)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    
    // MARK: Constructor
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
        //add views
        self.contentView.addSubview(containerView)
        
        self.containerView.addSubview(cellImageView)
        self.containerView.addSubview(lblPostLocation)
        self.containerView.addSubview(imgViewLocation)
        self.containerView.addSubview(lblPostCategory)
        self.containerView.addSubview(imgViewPostCategory)
        
        self.containerView.addSubview(postTitle)
        self.containerView.addSubview(postText)
        
        self.containerView.addSubview(btnPublishUnpublish)
        self.containerView.addSubview(lblTimeRemaining)
        
        //set up ui layout
        setupUILayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    // MARK: UI Setup
    
    func setupUILayout() {
        
        containerView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant:10).isActive = true
        containerView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10).isActive = true
        containerView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -10).isActive = true
        containerView.heightAnchor.constraint(equalToConstant: self.containerViewHeight).isActive = true
        containerView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -10).isActive = true
        
        
        cellImageView.topAnchor.constraint(equalTo:self.containerView.topAnchor, constant: 20).isActive = true
        cellImageView.leadingAnchor.constraint(equalTo:self.containerView.leadingAnchor, constant:20).isActive = true
//        cellImageView.widthAnchor.constraint(equalTo: containerView.heightAnchor, multiplier: 0.5).isActive = true
        cellImageView.widthAnchor.constraint(equalToConstant: self.cellImageViewHeight).isActive = true
        cellImageView.heightAnchor.constraint(equalTo:cellImageView.widthAnchor).isActive = true
     
       
       
        
        imgViewLocation.topAnchor.constraint(equalTo: cellImageView.topAnchor, constant:0).isActive = true
        imgViewLocation.leadingAnchor.constraint(equalTo: cellImageView.trailingAnchor, constant:20).isActive = true
        imgViewLocation.widthAnchor.constraint(equalToConstant: 15).isActive = true
        imgViewLocation.heightAnchor.constraint(equalTo: imgViewLocation.widthAnchor).isActive = true
        
        lblPostLocation.topAnchor.constraint(equalTo: imgViewLocation.topAnchor, constant:0).isActive = true
        lblPostLocation.leadingAnchor.constraint(equalTo: imgViewLocation.trailingAnchor, constant:10).isActive = true
//        lblPostLocation.trailingAnchor.constraint(equalTo: containerView.centerXAnchor, constant: -2.5).isActive = true
//        lblPostLocation.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        imgViewPostCategory.topAnchor.constraint(equalTo: imgViewLocation.bottomAnchor, constant:10).isActive = true
        imgViewPostCategory.leadingAnchor.constraint(equalTo: cellImageView.trailingAnchor, constant:20).isActive = true
        imgViewPostCategory.widthAnchor.constraint(equalToConstant: 15).isActive = true
        imgViewPostCategory.heightAnchor.constraint(equalTo: imgViewPostCategory.widthAnchor).isActive = true
        
        lblPostCategory.topAnchor.constraint(equalTo: imgViewPostCategory.topAnchor, constant:0).isActive = true
        lblPostCategory.leadingAnchor.constraint(equalTo: imgViewPostCategory.trailingAnchor, constant: 10).isActive = true
//        lblPostCategory.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10).isActive = true
//        lblPostCategory.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        postTitle.topAnchor.constraint(equalTo: imgViewPostCategory.bottomAnchor, constant: 10).isActive = true
        postTitle.leadingAnchor.constraint(equalTo: cellImageView.trailingAnchor, constant: 20).isActive = true
        postTitle.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10).isActive = true
        
        postText.topAnchor.constraint(equalTo: postTitle.bottomAnchor, constant: 5).isActive = true
        postText.leadingAnchor.constraint(equalTo: cellImageView.trailingAnchor, constant: 20).isActive = true
        postText.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20).isActive = true
        //postText.bottomAnchor.constraint(equalTo: cellImageView.bottomAnchor).isActive = true
        
        btnPublishUnpublish.topAnchor.constraint(equalTo: cellImageView.bottomAnchor, constant:10).isActive = true
        btnPublishUnpublish.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        btnPublishUnpublish.heightAnchor.constraint(equalToConstant: 40).isActive = true
        btnPublishUnpublish.widthAnchor.constraint(equalToConstant: cellImageViewHeight).isActive = true
        
        lblTimeRemaining.topAnchor.constraint(equalTo: btnPublishUnpublish.bottomAnchor, constant:5).isActive = true
        lblTimeRemaining.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        
        
    }
    
}

