//
//  NormalButton.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 7/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit

class NormalButton: UIButton {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        themeButton()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        themeButton()
    }
    
    /*
    * Adds bottom border and auto layout
    */
    private func themeButton(){
        self.contentEdgeInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 1
        self.layer.borderColor = ViewConstants.BACKGROUND_COLOR.cgColor
        self.layer.backgroundColor = ViewConstants.BUTTON_COLOR_DEFAULT.cgColor
        self.translatesAutoresizingMaskIntoConstraints = false
    }

}
