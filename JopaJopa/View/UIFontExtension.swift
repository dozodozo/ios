//
//  UIFontExtension.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 27/8/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import Foundation
import UIKit


extension UIFont {
    func withTraits(traits:UIFontDescriptorSymbolicTraits) -> UIFont {
        let descriptor = fontDescriptor.withSymbolicTraits(traits)
        return UIFont(descriptor: descriptor!, size: 0) //size 0 means keep the size as it is
    }
    
    func bold() -> UIFont {
        return withTraits(traits: .traitBold)
    }
    
    func italic() -> UIFont {
        return withTraits(traits: .traitItalic)
    }
}
