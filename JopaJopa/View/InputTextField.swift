//
//  InputTextField.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 7/6/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit

class InputTextField: UITextField {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    
    
    let padding = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5);
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        themeTextFieldButton()
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    /*
    * For auto layout
    */
    private func themeTextFieldButton(){
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    /*
    * Add bottom border to text field
    */
    
    func addBottomBorder(){
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = ViewConstants.TEXT_FIELD_BORDER_COLOR.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.borderStyle = UITextBorderStyle.none
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    
}
