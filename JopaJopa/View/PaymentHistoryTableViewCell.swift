//
//  PaymentHistoryTableViewCell.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 13/9/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit

//
//  PaymentHistoryViewCell.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 14/8/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit

class PaymentHistoryTableViewCell: UITableViewCell {
    
    
    //static let selectCellImaveViewHeight = CGFloat(30)
    
    let containerView: UIView = {
        let uiView = UIView()
        //uiView.layer.borderWidth = 0.5
        //uiView.backgroundColor = UIColor(red: 0.98, green: 0.98, blue: 0.98, alpha: 1)
        //uiView.backgroundColor = ViewConstants.LIGHT_TEXT_COLOR
        //uiView.layer.borderColor = ViewConstants.LIGHT_TEXT_COLOR.cgColor
        uiView.translatesAutoresizingMaskIntoConstraints = false
        return uiView
    }()
    
    
    let chargeAmount: UILabel = {
        let lbl = UILabel()
        lbl.text = "$0"
        lbl.numberOfLines = 0
        lbl.textAlignment = NSTextAlignment.right
        lbl.font = UIFont.preferredFont(forTextStyle: .headline).bold()
        lbl.textColor = ViewConstants.BUTTON_COLOR_DEFAULT
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let chargeDescription: UILabel = {
        let lbl = UILabel()
        lbl.text = "Description"
        lbl.font = UIFont.preferredFont(forTextStyle: .subheadline).bold()
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let chargeTransactionDate: UILabel = {
        let lbl = UILabel()
        lbl.text = "Date"
        lbl.font = UIFont.preferredFont(forTextStyle: .footnote)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    var chargeID : UILabel = {
        let lbl = UILabel()
        lbl.text = "chargeID"
        lbl.font = UIFont.preferredFont(forTextStyle: .footnote)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
   
    
    // MARK: Constructor
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
        //add views
        self.contentView.addSubview(containerView)
        
        
        self.containerView.addSubview(chargeAmount)
        self.containerView.addSubview(chargeDescription)
        self.containerView.addSubview(chargeTransactionDate)
        self.containerView.addSubview(chargeID)
        
        
        //set up ui layout
        setupUILayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    // MARK: UI Setup
    
    func setupUILayout() {
   
        containerView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor).isActive = true
        containerView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant:20).isActive = true
        containerView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant:-20).isActive = true
        
        chargeDescription.topAnchor.constraint(equalTo:self.containerView.topAnchor).isActive = true
        chargeDescription.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor, constant: 10).isActive = true
        chargeDescription.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor, constant: -80).isActive = true
        
        chargeAmount.topAnchor.constraint(equalTo:self.containerView.topAnchor, constant: 0).isActive = true
        chargeAmount.leadingAnchor.constraint(equalTo: self.chargeDescription.trailingAnchor, constant: 0).isActive = true
        chargeAmount.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor, constant: -10).isActive = true
        
        chargeTransactionDate.topAnchor.constraint(equalTo:self.chargeDescription.bottomAnchor, constant: 0).isActive = true
        chargeTransactionDate.leadingAnchor.constraint(equalTo: self.chargeDescription.leadingAnchor).isActive = true
        chargeTransactionDate.trailingAnchor.constraint(equalTo: self.chargeAmount.leadingAnchor).isActive = true
        chargeTransactionDate.bottomAnchor.constraint(equalTo: chargeAmount.bottomAnchor).isActive = true
        
        
        chargeID.topAnchor.constraint(equalTo:self.chargeTransactionDate.bottomAnchor, constant: 0).isActive = true
        chargeID.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor, constant: 10).isActive = true
        chargeID.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor).isActive = true
        chargeID.bottomAnchor.constraint(equalTo: self.containerView.bottomAnchor).isActive = true
        
        
        
        
        
        
        
        
    }
    
}


