//
//  PostsTableViewCell.swift
//  JopaJopa
//
//  Created by Kinzang Chhogyal on 14/8/18.
//  Copyright © 2018 Kinzang Chhogyal. All rights reserved.
//

import UIKit

class PublishedPostsTableViewCell: UITableViewCell {
    
    // MARK: Properties
    
    let placeImageViewHeight = CGFloat(40)
    let cornerRadius = 10
    
    //static let selectCellImaveViewHeight = CGFloat(30)
    
    let containerView: UIView = {
        let uiView = UIView()
        uiView.layer.borderWidth = 0.5
        //uiView.backgroundColor = UIColor(red: 0.98, green: 0.98, blue: 0.98, alpha: 1)
        //uiView.backgroundColor = ViewConstants.LIGHT_TEXT_COLOR
        uiView.layer.borderColor = ViewConstants.LIGHT_TEXT_COLOR.cgColor
        uiView.translatesAutoresizingMaskIntoConstraints = false
        return uiView
    }()
    
    let placeImageView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "darlingStreetBlur"))
        imageView.contentMode = .scaleAspectFill
        imageView.layer.borderWidth = 0.5
        imageView.layer.borderColor = UIColor.red.cgColor
        imageView.layer.cornerRadius = 20.0
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let placeName: UILabel = {
        let lbl = UILabel()
        lbl.text = "The Irresitible Chef"
        lbl.font = UIFont.preferredFont(forTextStyle: .footnote).bold()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let placeAddress: UILabel = {
        let lbl = UILabel()
        lbl.text = "Keio University Hiyoshi Campus, 4 Chome-1-1 Hiyoshi, Kōhoku-ku, Yokohama, Kanagawa Prefecture, Japan"
        lbl.font = UIFont.preferredFont(forTextStyle: .caption1)
        lbl.numberOfLines = 2
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let postImageView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "defaultImage"))
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.layer.shouldRasterize = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        return imageView
    }()
    
    var postTitle : UILabel = {
        let lbl = UILabel()
        lbl.text = "HAPPY HOUR IN THE HILL AND STARS"
        //lbl.font = UIFont.boldSystemFont(ofSize: 12)
        lbl.font = UIFont.preferredFont(forTextStyle: .subheadline).bold()
        //lbl.adjustsFontForContentSizeCategory = true
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let postText: UITextView = {
        let txtView = UITextView()
        txtView.text = "The world is my oyster and the"
        //lbl.layer.borderWidth = 1.0
        txtView.text = "The world is my oyster and we are all pearls that live in it unit we get pulled out and we don't have anything anymore to do about - come on and lets go away to the hidden mountain"
        txtView.font = UIFont.preferredFont(forTextStyle: .subheadline)
        txtView.dataDetectorTypes = .link
        txtView.isEditable = false
        txtView.isScrollEnabled = false
        txtView.contentInset.top = -5
        txtView.translatesAutoresizingMaskIntoConstraints = false
        return txtView
    }()
    
    let mapIconImageView: UIImageView = {
        let tintableImage = UIImage(named: "locationPin")?.withRenderingMode(.alwaysTemplate)

        let imageView = UIImageView()
        imageView.image = tintableImage
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        return imageView
    }()
    
    let distanceFromUser: UILabel = {
        let lbl = UILabel()
        lbl.text = "500m"
        lbl.textColor = UIColor.lightGray
        lbl.textAlignment = NSTextAlignment.right
        lbl.font = UIFont.preferredFont(forTextStyle: .footnote)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let timePosted: UILabel = {
        let lbl = UILabel()
        lbl.text = "4m ago"
        lbl.textColor = UIColor.lightGray
        lbl.font = UIFont.preferredFont(forTextStyle: .footnote)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    
    // MARK: Constructor
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
        //add views
        self.contentView.addSubview(containerView)
        
        
        self.containerView.addSubview(postImageView)
        self.containerView.addSubview(placeImageView)
        self.containerView.addSubview(placeName)
        self.containerView.addSubview(placeAddress)
        
        self.containerView.addSubview(postTitle)
        self.containerView.addSubview(postText)
       
        self.containerView.addSubview(timePosted)
        self.containerView.addSubview(distanceFromUser)
        self.containerView.addSubview(mapIconImageView)
        
        
        //set up ui layout
        setupUILayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    // MARK: UI Setup
    
    func setupUILayout() {
        
        let tableMargin = self.contentView.frame.width/40
        let placeImageSize = self.contentView.frame.width/10
        
        containerView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant:tableMargin).isActive = true
        containerView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: tableMargin).isActive = true
        containerView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -tableMargin).isActive = true
        containerView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -tableMargin*4).isActive = true
        
        postImageView.topAnchor.constraint(equalTo:self.containerView.topAnchor, constant: 0).isActive = true
        postImageView.centerXAnchor.constraint(equalTo: self.containerView.centerXAnchor).isActive = true
        postImageView.widthAnchor.constraint(equalTo: self.containerView.widthAnchor).isActive = true
        postImageView.heightAnchor.constraint(equalTo: postImageView.widthAnchor).isActive = true
        //postImageView.bottomAnchor.constraint(equalTo: self.containerView.bottomAnchor).isActive = true
        
        placeImageView.topAnchor.constraint(equalTo:self.postImageView.bottomAnchor, constant: 10).isActive = true
        placeImageView.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor, constant: 10).isActive = true
        placeImageView.widthAnchor.constraint(equalToConstant: placeImageViewHeight).isActive = true
        placeImageView.heightAnchor.constraint(equalTo: placeImageView.widthAnchor).isActive = true
        
        placeName.topAnchor.constraint(equalTo: placeImageView.topAnchor, constant: 0).isActive = true
        placeName.leadingAnchor.constraint(equalTo: placeImageView.trailingAnchor, constant: 10).isActive = true
        placeName.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor, constant: -10).isActive = true
        
        placeAddress.topAnchor.constraint(equalTo: placeName.bottomAnchor, constant: 0).isActive = true
        placeAddress.leadingAnchor.constraint(equalTo: placeImageView.trailingAnchor, constant: 10).isActive = true
        placeAddress.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor, constant: -10).isActive = true
        
        postTitle.topAnchor.constraint(equalTo: placeImageView.bottomAnchor, constant: 20).isActive = true
        postTitle.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor, constant:10).isActive = true
        postTitle.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor, constant: -10).isActive = true

        postText.topAnchor.constraint(equalTo: postTitle.bottomAnchor, constant: 0).isActive = true
        postText.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor, constant:5).isActive = true
        postText.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor, constant: -5).isActive = true
        
//        postText.topAnchor.constraint(equalTo: postTitle.bottomAnchor, constant: 5).isActive = true
//        postText.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor, constant:10).isActive = true
//        postText.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor, constant: -10).isActive = true
       
        
        mapIconImageView.topAnchor.constraint(equalTo: postText.bottomAnchor, constant: 10).isActive = true
        mapIconImageView.leadingAnchor.constraint(equalTo: self.containerView.trailingAnchor, constant:-30).isActive = true
        mapIconImageView.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor, constant: -10).isActive = true
        mapIconImageView.heightAnchor.constraint(equalTo: mapIconImageView.widthAnchor).isActive = true
        mapIconImageView.bottomAnchor.constraint(equalTo: self.containerView.bottomAnchor, constant: -10).isActive = true
        
        distanceFromUser.topAnchor.constraint(equalTo: postText.bottomAnchor, constant: 10).isActive = true
        distanceFromUser.leadingAnchor.constraint(equalTo: self.mapIconImageView.leadingAnchor, constant:-50).isActive = true
        distanceFromUser.trailingAnchor.constraint(equalTo: self.mapIconImageView.leadingAnchor, constant: -5).isActive = true
        distanceFromUser.bottomAnchor.constraint(equalTo: self.containerView.bottomAnchor, constant: -10).isActive = true
        
        timePosted.topAnchor.constraint(equalTo: postText.bottomAnchor, constant: 10).isActive = true
        timePosted.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor, constant:10).isActive = true
        timePosted.widthAnchor.constraint(equalToConstant: 100).isActive = true
        timePosted.bottomAnchor.constraint(equalTo: self.containerView.bottomAnchor, constant: -10).isActive = true
        
    }
    
}

